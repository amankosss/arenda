
//  Arenda
//  Created by Альберт on 26.09.15.

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)changeViewController:(int)mode;

@end

