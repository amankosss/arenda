
#import "MKStoreManager.h"

@implementation MKStoreManager

@synthesize product_7days, product_14days, product_30days;

@synthesize availableProducts;
@synthesize resultString;
@synthesize resultDescription;
@synthesize storeObserver;
@synthesize delegate;

static MKStoreManager * _sharedStoreManager;


+ (MKStoreManager *) sharedManager {
    
    @synchronized(self) {
        
        if (_sharedStoreManager == nil) {
            [[self alloc] init];
            
            _sharedStoreManager.product_7days = @"app.arenda.7days";
            _sharedStoreManager.product_14days = @"app.arenda.14days";
            _sharedStoreManager.product_30days = @"app.arenda.30days";

            _sharedStoreManager.storeObserver = [[MKStoreObserver alloc] init];
            [[SKPaymentQueue defaultQueue] addTransactionObserver:_sharedStoreManager.storeObserver];
        }
        
    }
    
    return _sharedStoreManager;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (_sharedStoreManager == nil) {
            _sharedStoreManager = [super allocWithZone:zone];
            return _sharedStoreManager;
        }
    }
    return nil;
}


- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (void)getAvailableProducts {
    
    SKProductsRequest * request = [[SKProductsRequest alloc] initWithProductIdentifiers:
                                  [NSSet setWithObjects: product_7days, product_14days, product_30days, nil]];
    request.delegate = self;
    [request start];
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    availableProducts = response.products;
}

- (void)purchaseProduct:(NSString *)productID {
    
    // 1. Проверяем можно ли совершать покупки в данный момент
    if (![SKPaymentQueue canMakePayments]) {
        
        resultString = @"NOT ALLOWED";
        resultDescription = @"";
        
        // cообщаем главному окну о невозможности совершать покупки
        [[NSNotificationCenter defaultCenter] postNotificationName:@"shopResults" object:@"paymentError"];
        return;
    }
    
    // 2. Пробуем совершить покупку по выбранному ProductID
    for (SKProduct * product in availableProducts) {
        if ([product.productIdentifier isEqualToString:productID]) {
            SKPayment * payment = [SKPayment paymentWithProduct:product];
            [[SKPaymentQueue defaultQueue] addPayment:payment];
            return;
        }
    }
    
    [self paymentFailed:@"Товар не существует" error:999];
}

- (void)paymentCanceled {
    resultString = @"CANCELED";
    resultDescription = @"";
    
    // cообщаем главному окну, что пользователь отменил покупку
    [[NSNotificationCenter defaultCenter] postNotificationName:@"shopResults" object:@"paymentError"];
}

- (void)paymentFailed:(NSString *)description error:(NSInteger)errorCode {
    resultString = @"FAILED";
    resultDescription = [NSString stringWithFormat:@"%@ (%d)", description, errorCode];
    
    // cообщаем главному окну, что произошла ошибка при покупке
    [[NSNotificationCenter defaultCenter] postNotificationName:@"shopResults" object:@"paymentError"];
}

- (void)paymentComplete:(NSString *)productID {
    // cообщаем главному окну, что покупка успешно выполнена
    [[NSNotificationCenter defaultCenter] postNotificationName:@"shopResults" object:@"paymentSuccess"];
}

@end
