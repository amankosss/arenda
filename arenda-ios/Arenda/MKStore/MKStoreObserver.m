
#import "MKStoreObserver.h"
#import "MKStoreManager.h"

@implementation MKStoreObserver

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *) transactions {
    
    for (SKPaymentTransaction * transaction in transactions) {
        
        switch (transaction.transactionState) {
                
           case SKPaymentTransactionStatePurchasing:
                break;
                
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
				
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;

            default:
                break;
        }
        
    }

}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    
    if (transaction.error.code == SKErrorPaymentCancelled) {
        // если отменил пользователь
        [[MKStoreManager sharedManager] paymentCanceled];
    } else {
        // если произошла какая-то ошибка
        [[MKStoreManager sharedManager] paymentFailed:transaction.error.localizedDescription
                                                error:transaction.error.code];
    }

    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];	
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    
    [[MKStoreManager sharedManager] paymentComplete: transaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];	
}

@end
