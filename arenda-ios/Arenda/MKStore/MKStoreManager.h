
#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "MKStoreObserver.h"

@protocol MKStoreKitDelegate <NSObject>
@end

@interface MKStoreManager : NSObject <SKProductsRequestDelegate> {

    NSString * product_7days;
    NSString * product_14days;
    NSString * product_30days;
    
    NSString * resultString;
    NSString * resultDescription;
    
    NSArray * availableProducts;
    
    MKStoreObserver * storeObserver;
    id <MKStoreKitDelegate> delegate;
    
}

@property (nonatomic, retain) NSString * product_7days;
@property (nonatomic, retain) NSString * product_14days;
@property (nonatomic, retain) NSString * product_30days;

@property (nonatomic, retain) NSString * resultString;
@property (nonatomic, retain) NSString * resultDescription;

@property (nonatomic, retain) NSArray * availableProducts;

@property (nonatomic, retain) id <MKStoreKitDelegate> delegate;
@property (nonatomic, retain) MKStoreObserver * storeObserver;


- (void)getAvailableProducts;
- (void)purchaseProduct:(NSString *)productID;

- (void)paymentCanceled;
- (void)paymentFailed:(NSString *)description error:(NSInteger)errorCode;
- (void)paymentComplete:(NSString *)productID;

+ (MKStoreManager *) sharedManager;

@end
