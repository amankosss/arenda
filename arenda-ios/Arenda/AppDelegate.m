
#import "AppDelegate.h"
#import "ArendaSingleton.h"
#import <AVFoundation/AVFoundation.h>

@interface AppDelegate ()
@end

@implementation AppDelegate

// метод, который меняет главное (корневое) окно на экране
- (void)changeViewController:(int)mode {
    
    NSString * rootVC;
    
    switch (mode) {
            
        case 0:
            rootVC = @"wnd_welcome";
            break;
            
        case 1:
            rootVC = @"wnd_main";
            break;
            
        case 2:
            rootVC = @"wnd_realtor";
            break;
    }
    
    UIStoryboard * storyboard = self.window.rootViewController.storyboard;
    
    self.window.rootViewController = [storyboard instantiateViewControllerWithIdentifier:rootVC];
    [self.window makeKeyAndVisible];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // выделяем отдельную сессию под воспроизведение звуков
    // чтобы в случае звука, не выключалась фоновая в системе музыка
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
    
    // решение бага (замирание) с показом клавиатуры в первый раз
    UITextField *lagFreeField = [[UITextField alloc] init];
    [self.window addSubview:lagFreeField];
    [lagFreeField becomeFirstResponder];
    [lagFreeField resignFirstResponder];
    [lagFreeField removeFromSuperview];
    
    [ArendaSingleton arenda];

    // какое окно сейчас нам нужно показать?
    // 0 - окно Welcome, 1 - окно соискателя, 2 - окно риелтора
    int currentMode = [[ArendaSingleton arenda] getCurrentMode];
    [self changeViewController:currentMode];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {

}

- (void)applicationDidEnterBackground:(UIApplication *)application {

}

- (void)applicationWillEnterForeground:(UIApplication *)application {

}

// удаляем бейджик возле иконки при активации приложения
- (void)applicationDidBecomeActive:(UIApplication *)application {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)applicationWillTerminate:(UIApplication *)application {

}

// отлавливаем событие получения Device Token
- (void)application:(UIApplication *)application
    didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    // отправляем его на сервер
    [[ArendaSingleton arenda] saveDeviceTokenOnServer:[NSString stringWithFormat:@"%@", deviceToken]];
}

@end
