
#import <UIKit/UIKit.h>
#import "ArendaSingleton.h"

@interface WND_Welcome : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel * label_title;
@property (weak, nonatomic) IBOutlet UILabel * label_description;
@property (weak, nonatomic) IBOutlet UILabel * label_registration;
@property (weak, nonatomic) IBOutlet UILabel * label_plus7;
@property (weak, nonatomic) IBOutlet UILabel * label_userAgreement;

@property (weak, nonatomic) IBOutlet UIImageView * img_stepIcon;
@property (weak, nonatomic) IBOutlet UIImageView * img_blackLine;

@property (weak, nonatomic) IBOutlet UIButton * btn_close;
@property (weak, nonatomic) IBOutlet UIButton * btn_next;
@property (weak, nonatomic) IBOutlet UIButton * btn_user;
@property (weak, nonatomic) IBOutlet UIButton * btn_realtor;
@property (weak, nonatomic) IBOutlet UIButton * btn_continue;
@property (weak, nonatomic) IBOutlet UIButton * btn_userAgreement;
@property (weak, nonatomic) IBOutlet UIButton * btn_resendCode;

@property (weak, nonatomic) IBOutlet UIScrollView * body_registration;
@property (weak, nonatomic) IBOutlet UITextField * textfield_phone;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView * loader;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView * loader_userRegistration;

- (IBAction)btn_close:(id)sender;

- (IBAction)btn_next:(id)sender;
- (IBAction)btn_user:(id)sender;
- (IBAction)btn_realtor:(id)sender;

- (IBAction)btn_continue:(id)sender;
- (IBAction)btn_userAgreement:(id)sender;
- (IBAction)btn_resendCode:(id)sender;


@end

