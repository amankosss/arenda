
#import <UIKit/UIKit.h>
#import "ArendaSingleton.h"

@interface WND_Realtor: UIViewController <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UIActionSheetDelegate>

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;

@property (weak, nonatomic) IBOutlet UIImageView * img_blackBackground;
@property (weak, nonatomic) IBOutlet UIScrollView * body_payment;

@property (weak, nonatomic) IBOutlet UITableView * table_users;

@property (weak, nonatomic) IBOutlet UILabel * label_usersOnline;
@property (weak, nonatomic) IBOutlet UILabel * label_yourRegion;

@property (weak, nonatomic) IBOutlet UILabel * label_payment14DaysHint;
@property (weak, nonatomic) IBOutlet UILabel * label_payment30DaysHint;
@property (weak, nonatomic) IBOutlet UILabel * label_extendAnAccess;

@property (weak, nonatomic) IBOutlet UIButton * btn_pay_7days;
@property (weak, nonatomic) IBOutlet UIButton * btn_pay_14days;
@property (weak, nonatomic) IBOutlet UIButton * btn_pay_30days;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView * loader;

- (IBAction)btn_settings:(id)sender;
- (IBAction)btn_contract:(id)sender;
- (IBAction)btn_call:(id)sender;
- (IBAction)btn_info:(id)sender;

- (IBAction)btn_pay_7days:(id)sender;
- (IBAction)btn_pay_14days:(id)sender;
- (IBAction)btn_pay_30days:(id)sender;
- (IBAction)btn_closePayment:(id)sender;

@end
