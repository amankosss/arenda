
#import <MapKit/MapKit.h>

#define PHP_FILE_REG                                    @"v2_reg.php"
#define PHP_FILE_USER                                   @"v2_user.php"
#define PHP_FILE_SETTINGS                               @"v2_settings.php"
#define PHP_FILE_REALTOR                                @"v2_realtor.php"
#define PHP_FILE_PAYMENT                                @"v2_payment_ios.php"

#define TASK_NEW_CODE                                   @"New Code"
#define TASK_CHECK_CODE                                 @"Check Code"
#define TASK_REG_TENANT                                 @"Reg Tenant"
#define TASK_SUBMIT_SEARCH                              @"Submit Search"
#define TASK_CANCEL_SEARCH                              @"Cancel Search"

#define ECHO_TENANT_EXISTS                              @"tenantExists"
#define ECHO_REG_TENANT_SUCCESS                         @"regTenantSuccess"
#define ECHO_CODE_SENT                                  @"codeSent"
#define ECHO_REG_SUCCESS                                @"regSuccess"
#define ECHO_ERROR_CODE                                 @"errorCode"
#define ECHO_WRONG_PHONE                                @"wrongPhone"
#define ECHO_ERROR_PHONE                                @"errorPhone"
#define ECHO_PLEASE_WAIT                                @"pleaseWait"
#define ECHO_DAY_LIMIT                                  @"dayLimit"
#define ECHO_ALREADY_CHANGED                            @"alreadyChanged"
#define ECHO_USERS                                      @"users"
#define ECHO_PAYMENT_ERROR                              @"paymentError"
#define ECHO_PAYMENT_SUCCESS                            @"paymentSuccess"
#define ECHO_SUBMIT_SUCCESS                             @"submitSuccess"
#define ECHO_CANCEL_SUCCESS                             @"cancelSuccess"
#define ECHO_UPDATE_APP                                 @"updateApp"
#define ECHO_ERROR_ACCOUNT                              @"errorAccount"
#define ECHO_ERROR_COMMENT                              @"errorComment"
#define ECHO_CONTACT_US_SUCCESS                         @"contactusSuccess"

#define ALERT_TASK_CHECK_NUMBER                         @"Check Number"
#define ALERT_TASK_RESEND_CODE                          @"Resend Code"
#define ALERT_TASK_SUBMITTING_SEARCH                    @"Submitting Search"
#define ALERT_TASK_CANCELLING_SEARCH                    @"Cancelling Search"
#define ALERT_TASK_UPDATE_APP                           @"Update App"
#define ALERT_TASK_PUSH_NOTIFICATION_HINT               @"Push Notifications"
#define ALERT_TASK_REALTOR_NEWS                         @"Realtor News"
#define ALERT_TASK_CONTRACT                             @"Contract"
#define ALERT_TASK_TEST_PERIOD_EXPIRED                  @"Test Period Expired"
#define ALERT_TASK_PURCHASE_TIMEOUT                     @"Purchase Timeout"
#define ALERT_TASK_PURCHASE_NOT_VALIDATED_OR_FAILED     @"Purchase Not Validated Or Failed"
#define ALERT_TASK_CHANGE_MODE                          @"Change Mode"
#define ALERT_TASK_DISABLE_PUSH_NOTIFICATIONS           @"Disable Push Notifications"

#define URL_AGREEMENT                                   @"agreement"
#define URL_ARENDA_APPSTORE                             @"appstore"
#define URL_ARENDA_CONTRACT_FILE                        @"contract.doc"
#define URL_ARENDA_HOMEPAGE                             @""
#define URL_FACEBOOK                                    @"facebook"
#define URL_TWITTER                                     @"twitter"
#define URL_VKONTAKTE                                   @"vkontakte"

#define SEGUE_MAP_TO_SETTINGS                           @"map2settings"
#define SEGUE_REALTOR_TO_SETTINGS                       @"realtor2settings"
#define SEGUE_REALTOR_TO_CONTACT_US                     @"realtor2contactus"
#define SEGUE_SETTINGS_TO_WELCOME                       @"settings2welcome"
#define SEGUE_SETTINGS_TO_CONTACT_US                    @"settings2contactus"

#define MODE_TENANT                                     1
#define MODE_REALTOR                                    2

#define TEXT_FIELD_CODE                                 6
#define TEXT_FIELD_PHONE                                10

@interface ArendaSingleton : NSObject <CLLocationManagerDelegate, UIActionSheetDelegate>

+ (ArendaSingleton *) arenda;

- (CLLocationCoordinate2D)getMyLocation;
- (BOOL)checkInternet;
- (void)activateLocation;
- (void)activatePushNotifications;
- (void)saveDeviceTokenOnServer:(NSString *)token;
- (void)updateUserInfo;
- (BOOL)isPushNotificationsEnabled;

- (int)loadIntFromUserDefaults:(NSString *)keyName;
- (void)saveIntToUserDefaults:(int)value forKey:(NSString *)keyName;

- (BOOL)loadBooleanFromUserDefaults:(NSString *)keyName;
- (void)saveBooleanToUserDefaults:(BOOL)value forKey:(NSString *)keyName;

- (NSString *)loadStringFromUserDefaults:(NSString *)keyName;
- (void)saveStringToUserDefaults:(NSString *)value forKey:(NSString *)keyName;

- (NSArray *)loadPhonesArray;

- (NSString *)generateRooms:(int)rooms;
- (NSString *)generateFlatCost:(int)cost prefix:(int)_prefix;
- (NSString *) getTime:(NSString *)userUnix;



- (void)roundCorners:(UIView *)view radius:(CGFloat)value;
- (void)animateView:(UIView *)view time:(NSTimeInterval)seconds;

- (NSString *)getMyUserID;
- (NSString *)getMyPhoneNumber;
- (NSString *)getMyID;
- (CGSize)getScreenSizes;
- (int)getCurrentMode;
- (NSString *)getAppWebsite;
- (NSString *)getAppVersion;
- (NSString *)getOSVersion;
- (NSString *)getDeviceModel;
- (void)openURL:(NSString *)link;

- (NSInteger)random:(NSInteger)min maxNum:(NSInteger)max;

- (void)POST:(NSString *)body phpFile:(NSString *)_phpFile;

@property (nonatomic, retain) NSString * resultString;
@property (nonatomic, readwrite) NSString * currentWindow;

@property (nonatomic, readwrite) NSString * rateAppOrContactUs;


@end