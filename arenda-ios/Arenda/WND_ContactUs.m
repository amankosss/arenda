
#import "WND_ContactUs.h"

@interface WND_ContactUs () {

    NSTimer * timerTimeout;                             // таймер таймаута
}

@end

@implementation WND_ContactUs

@synthesize label_title, textview_field, btn_send, loader;

#define FIELD_PLACEHOLDER @"Если у Вас есть какие-либо замечания, предложения или пожелания"

// показать сообщение
- (void)showMessage:(NSString *)_messageTitle message:(NSString *)_message firstButton:(NSString *)_firstButton secondButton:(NSString *)_secondButton {
    
    [[[UIAlertView alloc]
      initWithTitle:_messageTitle
      message:_message
      delegate:self
      cancelButtonTitle:_secondButton
      otherButtonTitles:_firstButton, nil] show];
}

// обработчик ответов от сервера
- (void)checkServerResult {

    // разбиваем полученный ответ на массив
    NSArray * body = [[[ArendaSingleton arenda] resultString] componentsSeparatedByString:@"*"];

    // если это успешный ответ о подтверждении поиска жилья
    if ([[body objectAtIndex:0] isEqualToString:ECHO_CONTACT_US_SUCCESS]) {
        
        [timerTimeout invalidate];
        [self returnElementsFirstPosition:YES];
        [btn_send setTitle:@"Отправлено!" forState:UIControlStateDisabled];
    }
    
}

- (void)returnElementsFirstPosition:(BOOL)withText {
    
    [loader stopAnimating];
    [btn_send setHidden:NO];
    [textview_field setEditable:YES];
    
    if (withText) {
        [btn_send setEnabled:NO];
        [textview_field setTextColor:[UIColor lightGrayColor]];
        [textview_field setText:FIELD_PLACEHOLDER];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkServerResult) name:
     @"contactUsResults" object:nil];
    
    textview_field.delegate = self;
    
    [ArendaSingleton arenda];
    
    [label_title setText:[ArendaSingleton arenda].rateAppOrContactUs];
    [[ArendaSingleton arenda] roundCorners:textview_field radius:10];
    [[ArendaSingleton arenda] roundCorners:btn_send radius:20];
    
    // получаем высоту экрана
    CGSize screenSizes = [[ArendaSingleton arenda] getScreenSizes];
    // если приложение запущено на iPhone 4
    [textview_field setFrame:CGRectMake(20, 112, 280, 76)];
    [btn_send setFrame:CGRectMake(60, 210, 200, 42)];
    
    // если приложение запущено на iPhone 5
    if (screenSizes.height > 481) {
        [textview_field setFrame:CGRectMake(20, 112, 280, 146)];
        [btn_send setFrame:CGRectMake(60, 280, 200, 42)];
    }
    
    // если приложение запущено на iPhone 6
    if (screenSizes.height > 569) {
        [textview_field setFrame:CGRectMake(screenSizes.width / 2 - 160, 112, 320, 246)];
        [btn_send setFrame:CGRectMake(screenSizes.width / 2 - 100, 380, 200, 42)];
    }
    
    // если приложение запущено на iPhone 6 plus
    if (screenSizes.height > 668) {
        [textview_field setFrame:CGRectMake(screenSizes.width / 2 - 180, 112, 360, 296)];
        [btn_send setFrame:CGRectMake(screenSizes.width / 2 - 100, 430, 200, 42)];
    }
    
    loader.center = CGPointMake(loader.center.x, btn_send.center.y + 2);
}

// событие при показе данного окна
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    // сообщаем синглтону, что ответы от сервера нужно присылать в наше окно
    [ArendaSingleton arenda].currentWindow = @"contactUsResults";
}

// если клавиатура видна на экране, то скрываем по нажатию на любое место экрана
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [textview_field resignFirstResponder];
}

// ограничитель ввода текста отзыва до 254 символов
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)s {
    
    // получаем текущее кол-во символов
    NSInteger textLength = [textView.text length] - range.length + [s length];
    
    BOOL allowEnter = YES;
    
    if (textLength > 254) allowEnter = NO;
    
    if ([s isEqualToString:@"'"] |
        [s isEqualToString:@"\""] |
        [s isEqualToString:@"+"] |
        [s isEqualToString:@"*"] |
        [s isEqualToString:@"|"]) allowEnter = NO;
    
    // если пользователь попытался перейти на новую строку (нажать Enter), что запрещено
    if ([s isEqualToString:@"\n"]) {
        [textview_field resignFirstResponder];
        allowEnter = NO;
    }
    
    return allowEnter;
}

// при начале набора отзыва
- (void)textViewDidBeginEditing:(UITextField *)textField {
    
    [btn_send setTitle:@"Отправить" forState:UIControlStateDisabled];
    
    // если в поле ввода сейчас подсказка
    if ([[textview_field text] isEqualToString:FIELD_PLACEHOLDER]) {
        [textview_field setTextColor:[UIColor darkGrayColor]];
        [textview_field setText:@""];
    }
}

// при завершении набора отзыва
- (void)textViewDidEndEditing:(UITextField *)textField {
    
    // если пользователь ничего не ввел
    if ([[textview_field text] isEqualToString:@""]) {
        [textview_field setTextColor:[UIColor lightGrayColor]];
        [textview_field setText:FIELD_PLACEHOLDER];
    }
}

// делаем enabled/disabed кнопку "Отправить",
// а также управляем плавной анимацией перехода на новую строку
- (void)textViewDidChange:(UITextView *)textView {
    
    // если в поле ввода текст отсутствует, делаем кнопку Disabled
    btn_send.enabled = textView.text.length > 0 ? YES : NO;

    CGRect line = [textView caretRectForPosition: textView.selectedTextRange.start];
    CGFloat overflow = line.origin.y + line.size.height - (textView.contentOffset.y + textView.bounds.size.height - textView.contentInset.bottom - textView.contentInset.top);
    
    if (overflow > 0) {
        CGPoint offset = textView.contentOffset;
        offset.y += overflow + 6;
        [UIView animateWithDuration:.2 animations:^{[textView setContentOffset:offset];}];
    }
}

// пользователь нажимает "Отправить"
- (IBAction)btn_send:(id)sender {
    
    // проверяем наличие интернета
    if (![[ArendaSingleton arenda] checkInternet]) {
        
        [self showMessage:@"Информация"
                  message:@"В данный момент отсутствует соединение с Интернетом."
              firstButton:@"OK" secondButton:nil];
        return;
    }
    
    NSString * message = [textview_field text];
    
    // если в поле ввода сейчас подсказка-placeholder
    if ([message isEqualToString:FIELD_PLACEHOLDER]) return;
    
    // (проверка на всякий случай) если текст отсутствует, хотя такое невозможно (disabled)
    if (message.length == 0) return;
    
    [textview_field resignFirstResponder];
    [textview_field setEditable:NO];
    [btn_send setHidden:YES];
    
    NSString * params = [NSString stringWithFormat:
                         @"action=settings&task=contactus&userid=%@&phone=%@&id=%@&stars=%d&text=%@&mode=%d",
                         [[ArendaSingleton arenda] getMyUserID],
                         [[ArendaSingleton arenda] getMyPhoneNumber],
                         [[ArendaSingleton arenda] getMyID],
                         [[ArendaSingleton arenda] loadIntFromUserDefaults:@"rateApp"],
                         message,
                         [[ArendaSingleton arenda] getCurrentMode]];
    
    [loader startAnimating];
    
    timerTimeout = [NSTimer scheduledTimerWithTimeInterval:10
                                                    target:self
                                                  selector:@selector(taskTimeout)
                                                  userInfo:nil
                                                   repeats:NO];
    
    [[ArendaSingleton arenda] POST:params phpFile:PHP_FILE_SETTINGS];
}

// сработал таймаут
- (void)taskTimeout {
    
    [timerTimeout invalidate];
    [self returnElementsFirstPosition:NO];
    
    [self showMessage:@"Проблема с Интернетом"
              message:@"Пожалуйста, повторите попытку."
          firstButton:@"OK" secondButton:nil];
}

// при закрытии окна
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    
    // на всякий случай выключаем таймер, чтобы не произошел крэш
    [timerTimeout invalidate];
}

// пользователь хочет закрыть окно обратной связи
- (IBAction)btn_close:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
