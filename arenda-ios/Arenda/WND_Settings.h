
#import <UIKit/UIKit.h>
#import "ArendaSingleton.h"

@interface WND_Settings: UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView * body_accountConfig;
@property (weak, nonatomic) IBOutlet UIScrollView * body_feedback;

@property (weak, nonatomic) IBOutlet UIScrollView * body_about;

@property (weak, nonatomic) IBOutlet UIButton * btn_push;
@property (weak, nonatomic) IBOutlet UIButton * btn_changeMode;
@property (weak, nonatomic) IBOutlet UIButton * btn_changePhone;
@property (weak, nonatomic) IBOutlet UIButton * btn_contactUs;

@property (weak, nonatomic) IBOutlet UIButton * btn_rate1star;
@property (weak, nonatomic) IBOutlet UIButton * btn_rate2stars;
@property (weak, nonatomic) IBOutlet UIButton * btn_rate3stars;
@property (weak, nonatomic) IBOutlet UIButton * btn_rate4stars;
@property (weak, nonatomic) IBOutlet UIButton * btn_rate5stars;

- (IBAction)btn_close:(id)sender;
- (IBAction)btn_push:(id)sender;

- (IBAction)btn_changeMode:(id)sender;
- (IBAction)btn_changePhone:(id)sender;

- (IBAction)btn_contactUs:(id)sender;

- (IBAction)btn_rate1star:(id)sender;
- (IBAction)btn_rate2stars:(id)sender;
- (IBAction)btn_rate3stars:(id)sender;
- (IBAction)btn_rate4stars:(id)sender;
- (IBAction)btn_rate5stars:(id)sender;

- (IBAction)btn_facebook:(id)sender;
- (IBAction)btn_twitter:(id)sender;
- (IBAction)btn_vkontakte:(id)sender;

- (IBAction)btn_website:(id)sender;

@end
