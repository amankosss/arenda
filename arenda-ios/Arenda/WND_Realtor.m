
#import "WND_Realtor.h"
#import "SQLiteAccess.h"
#import "MKStore/MKStoreManager.h"
#import <AudioToolbox/AudioToolbox.h>

@interface WND_Realtor () {
    
    NSString * alertMessageTask;                // какая задача перед диалоговым сообщением?
    NSString * actionSheetSelectedUserPhone;
    
    NSString * currentPurchaseProductID;
    int currentPurchaseTry;
    
    NSMutableArray * usersArray;                // массив группы диалогов
    
    BOOL flagErrorPhone;                        // флаг - сообщение о бане на экране сейчас
    BOOL flagUpdateApp;                         // флаг - сообщение обновитесь на экране сейчас
    BOOL flagCheckInternet;                     // флаг - сообщение нет интернета на экране сейчас
    BOOL flagNoLocation;                        // флаг - сообщение нет местоположения на экране сейчас
    BOOL flagNews;                              // флаг - новость сейчас на экране
    
    BOOL userInfoUpdated;                       // информация о риелторе обновилась при старте окна?
    BOOL deviceTokenRequested;                  // был ли запрос токена уже?
    BOOL callsIsDisabled;                       // запрещены ли звонки?
    BOOL isBanned;                              // заблокирован ли мой номер телефона?
    
    NSTimer * timerUsers;                       // таймер запроса новых соискателей
    NSTimer * timerPurchaseTimeout;             // таймер таймаута покупок
    
    int windowSession;                          // сессия данного окна
}

@end

@implementation WND_Realtor

@synthesize table_users, label_usersOnline, label_yourRegion, loader, img_blackBackground, body_payment, btn_pay_7days, btn_pay_14days, btn_pay_30days, label_payment14DaysHint, label_payment30DaysHint, label_extendAnAccess;

// показать сообщение
- (void)showMessage:(NSString *)alert title:(NSString *)_messageTitle message:(NSString *)_message firstButton:(NSString *)_firstButton secondButton:(NSString *)_secondButton {
    
    alertMessageTask = alert;
    
    [[[UIAlertView alloc]
      initWithTitle:_messageTitle
      message:_message
      delegate:self
      cancelButtonTitle:_secondButton
      otherButtonTitles:_firstButton, nil] show];
}

// обработка нажатий кнопок в окнах сообщений
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    // если это подсказка разрешить Push уведомления
    if ([alertMessageTask isEqualToString:ALERT_TASK_PUSH_NOTIFICATION_HINT]) {
        [[ArendaSingleton arenda] saveBooleanToUserDefaults:YES forKey:@"realtorPushRequested"];
        [[ArendaSingleton arenda] activatePushNotifications];
    }
    
    // если пользователь закрыл новость дня
    if ([alertMessageTask isEqualToString:ALERT_TASK_REALTOR_NEWS])
        [self performSelector:@selector(showPushRequestOrRegisterPush) withObject:nil afterDelay:1.0];
    
    // если это обновление приложения
    if ([alertMessageTask isEqualToString:ALERT_TASK_UPDATE_APP] && buttonIndex == 1)
        [[ArendaSingleton arenda] openURL:URL_ARENDA_APPSTORE];
    
    // если это переход по ссылке договора аренды жилья
    if ([alertMessageTask isEqualToString:ALERT_TASK_CONTRACT] && buttonIndex == 1)
        [[ArendaSingleton arenda] openURL:URL_ARENDA_CONTRACT_FILE];
    
    // если это сообщение об истечении тестового периода
    if ([alertMessageTask isEqualToString:ALERT_TASK_TEST_PERIOD_EXPIRED])
        [self openShop:NO];
    
    // произошел таймаут покупки товара
    if ([alertMessageTask isEqualToString:ALERT_TASK_PURCHASE_TIMEOUT]) {
        currentPurchaseTry ++;
        [self postForPurchase];
    }
    
    // если подтвердить покупку не удалось и пользователь требует помощи
    if ([alertMessageTask isEqualToString:ALERT_TASK_PURCHASE_NOT_VALIDATED_OR_FAILED] && buttonIndex == 1) {
        
        [ArendaSingleton arenda].rateAppOrContactUs = @"Обратная связь";
        
        [self performSegueWithIdentifier:SEGUE_REALTOR_TO_CONTACT_US
                                  sender:nil];
    }
    
    flagErrorPhone      = NO;
    flagUpdateApp       = NO;
    flagCheckInternet   = NO;
    flagNoLocation      = NO;
    flagNews            = NO;
    
    alertMessageTask    = @"";
}

// показать сообщение вы заблокированы
- (void)showMessageYouAreBanned {
    
    [self showMessage:nil
                title:[NSString stringWithFormat:@"+7%@", [[ArendaSingleton arenda] getMyUserID]]
              message:@"Ваш номер телефона недействителен."
          firstButton:@"OK" secondButton:nil];
}

// обработчик ответов от сервера
- (void)checkServerResult {
    
    // если текущая сессия отличается (из-за смены режимов)
    if ([[ArendaSingleton arenda] loadIntFromUserDefaults:@"windowSession"] != windowSession)
        return;
    
    isBanned = NO;

    // разбиваем полученный ответ на массив
    NSArray * body = [[[ArendaSingleton arenda] resultString] componentsSeparatedByString:@"*"];
    
    // если требуется обновить приложение
    if ([[body objectAtIndex:0] isEqualToString:ECHO_UPDATE_APP]) {
        
        // если это сообщение уже на экране (на всякий случай проверка)
        if (flagUpdateApp) return;
        flagUpdateApp = YES;
        
        [self stopTimer];

        [self showMessage:ALERT_TASK_UPDATE_APP
                    title:[NSString stringWithFormat:@"Доступна версия %@", [body objectAtIndex:1]]
                  message:@"Пожалуйста, сначала обновите приложение."
              firstButton:@"Обновить"
             secondButton:@"Отмена"];
        
        return;
    }
    
    // если номер телефона недействителен (возможно бан)
    if ([[body objectAtIndex:0] isEqualToString:ECHO_ERROR_PHONE]) {
        
        isBanned = YES;
        [self stopTimer];
        
        // если это сообщение уже на экране (на всякий случай проверка)
        if (flagErrorPhone) return;
        flagErrorPhone = YES;
        
        [self showMessageYouAreBanned];
        return;
    }
    
    // если это список соискателей
    if ([[body objectAtIndex:0] isEqualToString:ECHO_USERS]) {
        
        [label_usersOnline setText:[NSString stringWithFormat:@"Соискателей в городе: %d", [usersArray count]]];
        [label_usersOnline setHidden:NO];
        [label_yourRegion setHidden:NO];
        
        [loader stopAnimating];
        
        NSArray * configArray = [[body objectAtIndex:1] componentsSeparatedByString:@"|"];
        
        // показать новость, если она есть или зарегистрироваться в сервисе Push уведомлений
        [self showNewsOrPushRequest:[configArray objectAtIndex:0]];
        
        // разрешены ли звонки?
        callsIsDisabled = ![[configArray objectAtIndex:1] boolValue];
        [[ArendaSingleton arenda] saveBooleanToUserDefaults:callsIsDisabled forKey:@"callsIsDisabled"];
        
        if ([body count] == 2) return;
        
        int savedLastActionTime = 0;
        int newUsers = 0;
        
        NSArray * subArray;
        
        for (int i = 2; i < [body count]; i++) {
            
            subArray = [[body objectAtIndex:i] componentsSeparatedByString:@"|"];
            if (subArray.count < 10) return;
            
            NSString * userPhone        = [subArray objectAtIndex:0];
            NSString * userSearching    = [subArray objectAtIndex:1];
            NSString * userRegion       = [subArray objectAtIndex:4];
            NSString * userRentTime     = [subArray objectAtIndex:6];
            NSString * userComment      = [subArray objectAtIndex:9];
            
            int userActionTime          = [[subArray objectAtIndex:2] intValue];
            int userSearchTime          = [[subArray objectAtIndex:3] intValue];
            int userRooms               = [[subArray objectAtIndex:5] intValue];
            int userFlatCost            = [[subArray objectAtIndex:7] intValue];
            int userRealtorCost         = [[subArray objectAtIndex:8] intValue];
            
            // cохраняем каретку
            savedLastActionTime = userActionTime;
            
            // если это изменение статуса поиска на "Поиск отменен"
            if ([userSearching isEqualToString:@"CANCELED"]) {
                
                [SQLiteAccess updateWithSQL:[NSString stringWithFormat:@"update USERS set SEARCHING = 'CANCELED' where PHONE = '%@'", userPhone]];
            } else {
                
                newUsers++;
            
                // если же это новый поиск от соискателя,
                // то вначале удаляем текущую запись в базе
                [SQLiteAccess updateWithSQL:
                [NSString stringWithFormat:@"delete from USERS where PHONE = '%@'", userPhone]];

                // и добавляем новую
                [SQLiteAccess updateWithSQL:[NSString stringWithFormat:@"insert into USERS (PHONE, SEARCHING, ACTIONTIME, SEARCHTIME, REGION, ROOMS, RENTTIME, FLATCOST, REALTORCOST, COMMENT, COLOR, NEWUSER) values ('%@', '%@', %d, %d, '%@', %d, '%@', %d, %d, '%@', 'WHITE', 1)", userPhone, userSearching, userActionTime, userSearchTime, userRegion, userRooms, userRentTime, userFlatCost, userRealtorCost, userComment]];
            }
        }
        
        if (newUsers > 0) AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
        
        [[ArendaSingleton arenda] saveIntToUserDefaults: savedLastActionTime forKey:@"usersActionTime"];
        [self refreshUsers];
        
        return;
    }
    
    // во время покупки произошла ошибка, возможно джейлбрейк
    if ([[body objectAtIndex:0] isEqualToString:ECHO_PAYMENT_ERROR]) {
        
        [timerPurchaseTimeout invalidate];
        
        [self enableShopButtons:YES];
        
        [self showMessage:nil
                    title:@"Ошибка"
                  message:@"Убедитесь, что на Вашем телефоне не сделан джейлбрейк."
              firstButton:@"OK" secondButton:nil];
        return;
    }
    
    // успешна оплата
    if ([[body objectAtIndex:0] isEqualToString:ECHO_PAYMENT_SUCCESS]) {
        
        [timerPurchaseTimeout invalidate];

        label_extendAnAccess.text = @"спасибо за оплату!";
        
        callsIsDisabled = NO;
        [[ArendaSingleton arenda] saveBooleanToUserDefaults:callsIsDisabled forKey:@"callsIsDisabled"];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkServerResult)
                                                 name:@"realtorResults"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkShopResult:)
                                                 name:@"shopResults"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appWillEnterForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appWillEnterBackground:)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    
    // устанавливаем делегат и первые настройки таблицы сообщений
    table_users.delegate    = self;
    table_users.dataSource  = self;
    
    // обработчик долгого удерживания пальца над сообщением
    UILongPressGestureRecognizer * lp = [[UILongPressGestureRecognizer alloc]
                                         initWithTarget:self
                                         action:@selector(onTableLongPressed:)];
    lp.minimumPressDuration = 0.3;
    lp.delegate             = self;
    [table_users addGestureRecognizer:lp];
    
    [MKStoreManager sharedManager];
    [[MKStoreManager sharedManager] getAvailableProducts];
    
    [ArendaSingleton arenda];
    
    callsIsDisabled = [[ArendaSingleton arenda] loadBooleanFromUserDefaults:@"callsIsDisabled"];
    
    windowSession = [[ArendaSingleton arenda]random:100000 maxNum:999999];
    [[ArendaSingleton arenda]saveIntToUserDefaults:windowSession forKey:@"windowSession"];
    
    [[ArendaSingleton arenda] activateLocation];
    
    [[ArendaSingleton arenda] roundCorners:body_payment radius:10];
    [[ArendaSingleton arenda] roundCorners:btn_pay_7days radius:20];
    [[ArendaSingleton arenda] roundCorners:btn_pay_14days radius:20];
    [[ArendaSingleton arenda] roundCorners:btn_pay_30days radius:20];
    
    // показываем список соискателей, сохраненных ранее в базе данных
    [self refreshUsers];
}

// событие при разворачивании приложения
- (void)appWillEnterForeground:(NSNotification *)notification {

    // если текущая сессия отличается (из-за смены режимов)
    if ([[ArendaSingleton arenda] loadIntFromUserDefaults:@"windowSession"] != windowSession)
        return;

    [self startTimerAndRequest];
}

// событие в момент сворачивания приложения (уходим в бекграунд)
- (void)appWillEnterBackground:(NSNotification *)notification {

    [self stopTimer];
    
    // если текущая сессия отличается (из-за смены режимов)
    if ([[ArendaSingleton arenda] loadIntFromUserDefaults:@"windowSession"] != windowSession)
        return;

    // убираем у всех новых ячеек надпись "новый"
    [SQLiteAccess updateWithSQL:@"update USERS set NEWUSER = '0' where NEWUSER = '1'"];
    [self refreshUsers];
}

// событие при показе данного окна
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];

    // если текущая сессия отличается (из-за смены режимов)
    if ([[ArendaSingleton arenda] loadIntFromUserDefaults:@"windowSession"] != windowSession)
        return;

    // сообщаем синглтону, что ответы от сервера нужно присылать в наше окно
    [ArendaSingleton arenda].currentWindow = @"realtorResults";
    
    [self startTimerAndRequest];
}

// событие при скрытии окна
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];

    [self stopTimer];
}

// метод запускает таймер запроса новых соискателей
- (void)startUsersTimer {
    
    if (![timerUsers isValid])
        timerUsers = [NSTimer scheduledTimerWithTimeInterval:10
                                                      target:self
                                                    selector:@selector(requestNewUsersByTimer)
                                                    userInfo:nil
                                                     repeats:YES];
}

- (void)startTimerAndRequest {
    
    // запускаем таймер запроса новых соискателей
    [self startUsersTimer];
 
    // проверяем наличие интернета
    if (![[ArendaSingleton arenda] checkInternet] && !flagCheckInternet) {
        
        flagCheckInternet = YES;
        
        [loader startAnimating];
        [label_usersOnline setHidden:YES];
        [label_yourRegion setHidden:YES];
        
        [self showMessage:nil
                    title:@"Информация"
                  message:@"В данный момент отсутствует соединение с Интернетом."
              firstButton:@"OK" secondButton:nil];
        
        return;
    }
    
    // если местоположение определить не удается
    if ([[ArendaSingleton arenda] getMyLocation].latitude == 0.000000 && !flagNoLocation) {
        
        flagNoLocation = YES;
        
        [loader startAnimating];
        [label_usersOnline setHidden:YES];
        [label_yourRegion setHidden:YES];
        
        NSString * messageText = [NSString stringWithFormat:@"Пожалуйста, разрешите Arenda в Настройках > %@ > Службы геолокации.", [[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 ? @"Конфиденциальность" : @"Приватность"];
        
        [self showMessage:nil
                    title:@"Невозможно определить Ваше местоположение"
                  message:messageText
              firstButton:@"OK" secondButton:nil];
        
        return;
        
    }
    
    userInfoUpdated = YES;  // сообщаем, что информацию о риелторе мы сейчас обновим
    [[ArendaSingleton arenda] updateUserInfo];
    
    [self getSelectedRegion];
    [self requestNewUsers];
    
}

- (void)stopTimer {
    [timerUsers invalidate];
}

- (void)requestNewUsersByTimer {
    
    if (![[ArendaSingleton arenda] checkInternet] |
        ([[ArendaSingleton arenda] getMyLocation].latitude == 0.000000)) {
        
        [loader startAnimating];
        [label_usersOnline setHidden:YES];
        [label_yourRegion setHidden:YES];
        
        return;
    }
    
    [loader stopAnimating];
    [label_usersOnline setHidden:NO];
    [label_yourRegion setHidden:NO];
    
    // если при показе окна информацию о риелторе обновить не удалось
    if (!userInfoUpdated) {
        userInfoUpdated = YES;
        [[ArendaSingleton arenda] updateUserInfo];
    }
    
    [self getSelectedRegion];
    [self requestNewUsers];
}

// сделать запрос новых соискателей
- (void)requestNewUsers {
    
    NSString * params = [NSString stringWithFormat:
                         @"action=loadusers&userid=%@&phone=%@&id=%@&actiontime=%d",
                         [[ArendaSingleton arenda] getMyUserID],
                         [[ArendaSingleton arenda] getMyPhoneNumber],
                         [[ArendaSingleton arenda] getMyID],
                         [[ArendaSingleton arenda] loadIntFromUserDefaults:@"usersActionTime"]];
    
    [[ArendaSingleton arenda] POST:params phpFile:PHP_FILE_REALTOR];
}

// нужно ли показать подсказку на разрешение Push уведомлений?
// или сразу зарегистрироваться (получить Device Token)
- (void)showPushRequestOrRegisterPush {
    
    if (deviceTokenRequested) return;
  
    // если данная подсказка уже показывалась ранее, то регистрируемся сразу
    if ([[ArendaSingleton arenda] loadBooleanFromUserDefaults:@"realtorPushRequested"]) {
        deviceTokenRequested = YES;
        [[ArendaSingleton arenda] activatePushNotifications];
        return;
    }
    
    if (flagNews) return;
    deviceTokenRequested = YES;
    
    // если нет, то вначале показываем подсказку
    [self showMessage:ALERT_TASK_PUSH_NOTIFICATION_HINT
                title:@"Пожалуйста, разрешите Push уведомления"
              message:@"Чтобы моментально получать информацию о соискателях."
          firstButton:@"OK" secondButton:nil];
    
}

// показать новость для риелторов
- (void)showNewsOrPushRequest:(NSString *)news {

    // если новостей никаких нет или нет новых
    if ([news isEqualToString:@"-"] | [[[ArendaSingleton arenda] loadStringFromUserDefaults:@"realtorNews"] isEqualToString:news]) {
        [self performSelector:@selector(showPushRequestOrRegisterPush)
                   withObject:nil
                   afterDelay:1.0];
        return;
    }
    
    if (flagNews) return;
    flagNews = YES;
    
    [[ArendaSingleton arenda] saveStringToUserDefaults:news forKey:@"realtorNews"];
    
    [self showMessage:ALERT_TASK_REALTOR_NEWS
                title:@"Новость дня"
              message:news
          firstButton:@"OK" secondButton:nil];
}

// метод получения заголовка "Ваш город: Уфа"
- (void)getSelectedRegion {
    
    CLLocation * selectedRegion = [[CLLocation alloc]
                initWithLatitude:[[ArendaSingleton arenda] getMyLocation].latitude
                       longitude:[[ArendaSingleton arenda] getMyLocation].longitude];
    
    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
    
    [geoCoder reverseGeocodeLocation:selectedRegion
                   completionHandler:^(NSArray * placemarks, NSError * error) {
                       
                       NSString * selectedRegion = @"ваш город не определен";
                       
                       if (error == nil && [placemarks count] > 0) {
                           
                           CLPlacemark * placemark = [placemarks lastObject];
                           
                           if (placemark.locality)
                               selectedRegion = [NSString stringWithFormat:@"Ваш город: %@", placemark.locality];
                       }
                       
                       label_yourRegion.text = selectedRegion;
    }];
}

// риелтор хочет показать окно настроек приложения
- (IBAction)btn_contract:(id)sender {
    
    if (!img_blackBackground.hidden) return;
    
    [self showMessage:ALERT_TASK_CONTRACT
                title:@"Собственнику или начинающему риелтору"
              message:@"Мы подготовили для Вас шаблон договора аренды жилья, который обязательно потребуется, когда будете заключать сделку с соискателем.\n\nСсылка для скачивания:\narenda-app.ru/contract.doc"
          firstButton:@"Посмотреть"
         secondButton:@"OK"];
}

- (IBAction)btn_settings:(id)sender {

    if (!img_blackBackground.hidden) return;
    
    [self performSegueWithIdentifier:SEGUE_REALTOR_TO_SETTINGS
                              sender:nil];
}

// риелтор хочет позвонить соискателю
- (IBAction)btn_call:(id)sender {
    
    if (!img_blackBackground.hidden) return;
    
    // если подсказка про выделение цветом еще не показывалась
    if (![[ArendaSingleton arenda] loadBooleanFromUserDefaults:@"realtorUsersColorsHint"]) {
        [[ArendaSingleton arenda] saveBooleanToUserDefaults:YES forKey:@"realtorUsersColorsHint"];
        
        [self showMessage:nil
                    title:@"Подсказка"
                  message:@"Если Вы хотите выделить ячейку соискателя другим цветом, то нажмите и удерживайте ее."
              firstButton:@"OK" secondButton:nil];
        
        return;
    }
    
    // проверка вдруг мы заблокированы
    if (isBanned) {
        [self showMessageYouAreBanned];
        return;
    }
    
    // если звонки запрещены
    if (callsIsDisabled) {
        [self openShop:YES];
        return;
    }
    
    UIButton * btn = (UIButton *)sender;
    NSString * tel = [btn.restorationIdentifier substringFromIndex:3];
    
    [[UIApplication sharedApplication] openURL:
    [NSURL URLWithString:[NSString stringWithFormat:@"tel:+7%@", tel]]];
}

// риелтор хочет посмотреть комментарий к поиску жилья
- (IBAction)btn_info:(id)sender {
    
    if (!img_blackBackground.hidden) return;
    
    UIButton * btn = (UIButton *)sender;
    NSString * info = [btn.restorationIdentifier substringFromIndex:4];
    
    [self showMessage:nil
                title:@"Информация"
              message:info
          firstButton:@"OK" secondButton:nil];
}

// функция вывода на экран нужного кол-ва ячеек
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    //  если соискателей никаких нет, то показываем Empty сообщение
    if ([usersArray count] == 0) {
        [table_users setScrollEnabled:NO];
        return 1;
    }
    
    // если есть, выводим нужное кол-во ячеек
    [table_users setScrollEnabled:YES];
    return [usersArray count];
}

// устанавливаем высоту ячеек
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([usersArray count] == 0)
        return ([[ArendaSingleton arenda] getScreenSizes].height) / 2;
    
    NSDictionary * indx  = [usersArray objectAtIndex:indexPath.row];
    
    if ([[indx objectForKey:@"RENTTIME"] isEqualToString:@"MONTH"]) return 250;
    return 228;
}

#define rgbColor(r, g, b) [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0];

// Отображение списка соискателей в таблице
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // если никаких соискателей нет, то выводим Empty сообщение
    if ([usersArray count] == 0)
        return [tableView dequeueReusableCellWithIdentifier:@"cell_empty"];
    
    UITableViewCell * cell      = [tableView dequeueReusableCellWithIdentifier:@"cell_users"];
    
    NSDictionary * indx         = [usersArray objectAtIndex:indexPath.row];
    
    UIImageView * imgWhiteBg    = (UIImageView *) [cell viewWithTag:100];
    UIImageView * imgInfoIcon   = (UIImageView *) [cell viewWithTag:101];
    
    UILabel * labelSearchTime   = (UILabel *) [cell viewWithTag:102];
    UILabel * labelRooms        = (UILabel *) [cell viewWithTag:103];
    UILabel * labelRentTime     = (UILabel *) [cell viewWithTag:104];
    UILabel * labelRegion       = (UILabel *) [cell viewWithTag:105];
    UILabel * labelFlatCost     = (UILabel *) [cell viewWithTag:106];
    UILabel * labelRealtorCost  = (UILabel *) [cell viewWithTag:107];
    UILabel * labelIndex        = (UILabel *) [cell viewWithTag:108];
    
    UIButton * buttonCall       = (UIButton *) [cell viewWithTag:109];
    UIButton * buttonInfo       = (UIButton *) [cell viewWithTag:110];
    
    UILabel * labelNew          = (UILabel *) [cell viewWithTag:111];
    
    labelNew.hidden             = ![[indx objectForKey:@"NEWUSER"] isEqualToString:@"1"];
    
    labelIndex.text             = [NSString stringWithFormat:@"%d", indexPath.row + 1];
    
    labelRooms.text             = [indx objectForKey:@"ROOMS"];
    labelRentTime.text          = [indx objectForKey:@"RENTTIME"];
    labelRegion.text            = [indx objectForKey:@"REGION"];
    labelFlatCost.text          = [indx objectForKey:@"FLATCOST"];
    labelRealtorCost.text       = [indx objectForKey:@"REALTORCOST"];
    
    BOOL longRentTime           = [[indx objectForKey:@"RENTTIME"] isEqualToString:@"MONTH"];
    
    // заголовок: сегодня в 21:00
    labelSearchTime.text        = [[ArendaSingleton arenda] getTime:[indx objectForKey:@"SEARCHTIME"]];
    
    // заголовок: Ищут 1-комнатную квартиру
    labelRooms.text             = [NSString stringWithFormat:
                                   @"Ищут %@.",
                                   [[ArendaSingleton arenda] generateRooms:[[indx objectForKey:@"ROOMS"] intValue]]];
    
    // заголовок: На длительный срок, в районе:
    labelRentTime.text          = [NSString stringWithFormat:
                                   @"%@, в районе:",
                                   longRentTime == YES ? @"На длительный срок" : @"Посуточно"];
    
    // заголовок: Не дороже: 14000 тг./мес.
    labelFlatCost.text          = [[ArendaSingleton arenda] generateFlatCost:[[indx objectForKey:@"FLATCOST"] intValue]
                                                                      prefix:!longRentTime];
    
    // заголовок: Риелтору не более 1000 тг.
    labelRealtorCost.text       = [NSString stringWithFormat:
                                   @"Риелтору не более: %d тг.",
                                   [[indx objectForKey:@"REALTORCOST"] intValue]];
    
    // не показывать заголовок "Риелтору не более 1000 руб.", если посуточно
    [labelRealtorCost setHidden:!longRentTime];

    // показываем кнопку Info, если есть комментарий
    NSString * userComment = [indx objectForKey:@"COMMENT"];
    [buttonInfo setHidden:[userComment isEqualToString:@"-"] ? YES : NO];
    
    [imgInfoIcon setHidden:buttonInfo.hidden];
    [buttonInfo setRestorationIdentifier:
    [NSString stringWithFormat:@"info%@", userComment]];
    
    // присваиваем к кнопке Позвонить - номер телефона соискателя
    [buttonCall setRestorationIdentifier:
     [NSString stringWithFormat:@"tel%@", [indx objectForKey:@"PHONE"]]];
    
    // если соискатель отменил поиск жилья
    if ([[indx objectForKey:@"SEARCHING"] isEqualToString:@"CANCELED"]) {
        
        [buttonCall setTitle:@"Поиск отменен"
                    forState:UIControlStateNormal];
        
        [buttonCall setBackgroundImage:[UIImage imageNamed:@"bg_red.png"]
                              forState:UIControlStateNormal];
        
        [buttonCall setEnabled:NO];
        
    } else {
        
        // если он ищет в данный момент жилье
        [buttonCall setTitle:@"Позвонить"
                    forState:UIControlStateNormal];
        
        [buttonCall setBackgroundImage:[UIImage imageNamed:@"bg_green.png"]
                              forState:UIControlStateNormal];
        
        [buttonCall setEnabled:YES];
    }
    
    // поднимаем вверх кнопку Позвонить и белую ячейку, если комиссия риелтору не указана
    labelSearchTime.center  = CGPointMake(labelNew.hidden ? labelNew.center.x - 50 : labelNew.center.x - 120,
                                         labelSearchTime.center.y);
    
    labelIndex.center       = CGPointMake(labelIndex.center.x,
                                          longRentTime == YES ? 225 : 203);
    
    buttonCall.center       = CGPointMake(buttonCall.center.x,
                                          longRentTime == YES ? 198 : 176);
    
    imgWhiteBg.frame        = CGRectMake(imgWhiteBg.frame.origin.x,
                                  imgWhiteBg.frame.origin.y,
                                  imgWhiteBg.frame.size.width,
                                  longRentTime == YES ? 230 : 208);
    
    // окрашиваем цвет ячейки соискателя, если риелтор это сделал
    NSString * userColor = [indx objectForKey:@"COLOR"];
    
    if ([userColor isEqualToString:@"WHITE"])
        imgWhiteBg.backgroundColor = rgbColor(255, 255, 255);
    
    if ([userColor isEqualToString:@"RED"])
        imgWhiteBg.backgroundColor = rgbColor(235, 186, 186);
    
    if ([userColor isEqualToString:@"GREEN"])
        imgWhiteBg.backgroundColor = rgbColor(181, 233, 159);
    
    if ([userColor isEqualToString:@"BLUE"])
        imgWhiteBg.backgroundColor = rgbColor(169, 208, 228);
    
    if ([userColor isEqualToString:@"YELLOW"])
        imgWhiteBg.backgroundColor = rgbColor(248, 255, 178);

    // округляем цвет ячейки, надписи "новый" и кнопки "Позвонить"
    [[ArendaSingleton arenda] roundCorners:imgWhiteBg   radius:10];
    [[ArendaSingleton arenda] roundCorners:buttonCall   radius:20];
    [[ArendaSingleton arenda] roundCorners:labelNew     radius:4];
    
    return cell;
}

// на какой цвет поменять ячейку соискателя
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString * colorName = @"";
    
    if (buttonIndex == 0) colorName = @"RED";
    if (buttonIndex == 1) colorName = @"GREEN";
    if (buttonIndex == 2) colorName = @"BLUE";
    if (buttonIndex == 3) colorName = @"YELLOW";
    if (buttonIndex == 4) colorName = @"WHITE";

    if (colorName.length == 0) return;
    
    [SQLiteAccess updateWithSQL: [NSString stringWithFormat:
                                  @"update USERS set COLOR = '%@' where PHONE = '%@'",
                                  colorName,
                                  actionSheetSelectedUserPhone]];
    
    [self refreshUsers];
}

// обрабатываем долгое удерживание ячейки, чтобы показать доп опции
- (void)onTableLongPressed:(UILongPressGestureRecognizer *)gestureRecognizer {
    
    if (!img_blackBackground.hidden) return;
    
    // получаем указатель на выделенную ячейку
    CGPoint p = [gestureRecognizer locationInView:table_users];
    NSIndexPath * indexPath = [table_users indexPathForRowAtPoint:p];
    
    if (indexPath == nil) return;
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan) return;
    if ([usersArray count] == 0) return;
    
    NSDictionary * indx  = [usersArray objectAtIndex:indexPath.row];
    
    actionSheetSelectedUserPhone = [indx objectForKey:@"PHONE"];
    
    UIActionSheet * actionMenu = [[UIActionSheet alloc]
                                  initWithTitle:@"Вы можете выделить соискателя другим цветом, если это необходимо."
                                  delegate:self
                                  cancelButtonTitle:@"Отмена"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"Красный", @"Зеленый", @"Синий", @"Желтый", @"Белый", nil];
    
    [actionMenu showInView:self.view];
}

// доступен ли свайп справа налево чтобы показать кнопку УДАЛИТЬ соискателя
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (!img_blackBackground.hidden || [usersArray count] == 0) return NO;

    return YES;
}

// функция свайпа налево, чтобы нажать Удалить сообщение
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (!img_blackBackground.hidden) return;
    
    // удаление ли это? (проверка на всякий случай)
    if (editingStyle != UITableViewCellEditingStyleDelete) return;
    
    // если хотя бы есть хоть одна группа диалога (проверка на всякий случай)
    if ([usersArray count] == 0) return;
    
    NSDictionary * indx  = [usersArray objectAtIndex:indexPath.row];
    
    [SQLiteAccess updateWithSQL:[NSString stringWithFormat:
                                 @"delete from USERS where PHONE = '%@'",
                                 [indx objectForKey:@"PHONE"]]];
    
    [self refreshUsers];
}

// обновить список соискателей на экране
- (void)refreshUsers {
    
    // загружаем соискателей в массив данных
    usersArray = [NSMutableArray arrayWithArray:
                  [SQLiteAccess selectManyRowsWithSQL:@"select * from USERS order by ACTIONTIME desc"]];
    
    [label_usersOnline setText:[NSString stringWithFormat:
                                 @"Соискателей в городе: %lu",
                                 (unsigned long)[usersArray count]]];
    
    // и делаем обновление таблицы
    [table_users reloadData];
}

// открыть магазин товаров для риелтора
- (void)openShop:(BOOL)withHint {
    
    // если нужно показать подсказку про окончание тестового периода
    if (withHint && ![[ArendaSingleton arenda] loadBooleanFromUserDefaults:@"realtorTestPeriodExpired"]) {
        
        [[ArendaSingleton arenda] saveBooleanToUserDefaults:YES forKey:@"realtorTestPeriodExpired"];
        
        [self showMessage:ALERT_TASK_TEST_PERIOD_EXPIRED
                    title:@"Информация"
                  message:@"Тестовый период закончился. Чтобы совершать звонки, Вам необходимо продлить доступ."
              firstButton:@"OK" secondButton:nil];
        return;
    }
    
    // проверяем были получены товары
    if ([MKStoreManager sharedManager].availableProducts.count == 0) {
        
        [self showMessage:nil
                    title:@"Информация"
                  message:@"Пожалуйста, подождите несколько секунд или попробуйте перезапустить приложение."
              firstButton:@"OK" secondButton:nil];
        return;
    }
    
    // обновляем цены на кнопках
    [self refreshPrices];
    
    if ([label_extendAnAccess.text isEqualToString:@"спасибо за оплату!"])
        [self enableShopButtons:YES];
    
    // и показываем окно магазина
    img_blackBackground.hidden = NO;
    [[ArendaSingleton arenda] animateView:body_payment time:0.2];
    body_payment.hidden = NO;
    
    // параллельно считаем сколько раз мы открывали окно магазина
    int shopOpens = [[ArendaSingleton arenda] loadIntFromUserDefaults:@"shopOpens"];
    shopOpens ++;
    [[ArendaSingleton arenda] saveIntToUserDefaults:shopOpens forKey:@"shopOpens"];
    
    // и передаем информацию на сервер об этом
    NSString * params = [NSString stringWithFormat:@"action=purchase&type=%@&typedesc=%d&userid=%@&id=%@",
                         @"SHOP",
                         shopOpens,
                         [[ArendaSingleton arenda] getMyUserID],
                         [[ArendaSingleton arenda] getMyID]];
    
    [[ArendaSingleton arenda] POST:params phpFile:PHP_FILE_PAYMENT];
}

// обновить цены на кнопках магазина
- (void)refreshPrices {
    
    float dayPrice      = 0;
    
    int days7Price      = 0;
    int days14Price     = 0;
    int days30Price     = 0;
    
    // раз товары с сервера Apple получены - пробуем открыть магазин
    for (SKProduct * product in [[MKStoreManager sharedManager] availableProducts]) {
        
        if ([product.productIdentifier isEqualToString:[[MKStoreManager sharedManager] product_7days]])
            days7Price = [product.price intValue];
        
        if ([product.productIdentifier isEqualToString:[[MKStoreManager sharedManager] product_14days]])
            days14Price = [product.price intValue];
        
        if ([product.productIdentifier isEqualToString:[[MKStoreManager sharedManager] product_30days]])
            days30Price = [product.price intValue];
    }
    
    [btn_pay_7days setTitle:[NSString stringWithFormat:@"7 дней = %d тг.", days7Price]
                  forState:UIControlStateNormal];
    
    [btn_pay_14days setTitle:[NSString stringWithFormat:@"14 дней = %d тг.", days14Price]
                    forState:UIControlStateNormal];
    
    [btn_pay_30days setTitle:[NSString stringWithFormat:@"30 дней = %d тг.", days30Price]
                    forState:UIControlStateNormal];
    
    dayPrice = (float)days7Price / 7;
    
    [label_payment14DaysHint setText:[NSString stringWithFormat:@"Выгоднее на %0.f тг.", dayPrice * 14 - days14Price]];
    
    [label_payment30DaysHint setText:[NSString stringWithFormat:@"Выгоднее на %0.f тг.", dayPrice * 30 - days30Price]];
}

// закрыть окно магазина
- (IBAction)btn_closePayment:(id)sender {
    
    img_blackBackground.hidden = YES;
    body_payment.hidden = YES;
    
    // запускаем таймер запроса новых соискателей
    [self startUsersTimer];
}

- (IBAction)btn_pay_7days:(id)sender {
    [self startPurchase:[[MKStoreManager sharedManager] product_7days]];
}

- (IBAction)btn_pay_14days:(id)sender {
    [self startPurchase:[[MKStoreManager sharedManager] product_14days]];
}

- (IBAction)btn_pay_30days:(id)sender {
    [self startPurchase:[[MKStoreManager sharedManager] product_30days]];
}

- (void)enableShopButtons:(BOOL)yes {
    
    btn_pay_7days.enabled   = yes;
    btn_pay_14days.enabled  = yes;
    btn_pay_30days.enabled  = yes;
    
    label_extendAnAccess.text = yes ? @"продлите доступ!" : @"подождите ...";
}

- (void)startPurchase:(NSString *)product {
    
    // проверяем наличие интернета
    if (![[ArendaSingleton arenda] checkInternet]) {
        
        flagCheckInternet = YES;

        [self showMessage:nil
                    title:@"Информация"
                  message:@"В данный момент отсутствует соединение с Интернетом."
              firstButton:@"OK" secondButton:nil];
        return;
    }
    
    [self enableShopButtons:NO];
    
    // передаем в очередь на покупку
    currentPurchaseProductID = product;
    [[MKStoreManager sharedManager] purchaseProduct:product];
}

// обработчик ответов от магазина
- (void)checkShopResult:(NSNotification *)data {
    
    // если текущая сессия отличается (из-за смены режимов)
    if ([[ArendaSingleton arenda] loadIntFromUserDefaults:@"windowSession"] != windowSession)
        return;
    
    // Если при покупке произошла ошибка
    if ([[data object] isEqualToString:@"paymentError"]) {
        
        [self enableShopButtons:YES];
        
        // передаем информацию на сервер об этом
        NSString * params = [NSString stringWithFormat:
                             @"action=purchase&type=%@&typedesc=%@&product=%@&userid=%@&id=%@",
                             [MKStoreManager sharedManager].resultString,
                             [MKStoreManager sharedManager].resultDescription,
                             currentPurchaseProductID,
                             [[ArendaSingleton arenda] getMyUserID],
                             [[ArendaSingleton arenda] getMyID]];
        
        [[ArendaSingleton arenda] POST:params phpFile:PHP_FILE_PAYMENT];
        
        // если запрещено совершать покупки
        if ([[[MKStoreManager sharedManager] resultString] isEqualToString:@"NOT ALLOWED"]) {
            [self showMessage:nil
                        title:@"Информация"
                      message:@"Убедитесь, что Встроенные покупки разрешены в Настройках > Основные > Ограничения."
                  firstButton:@"OK" secondButton:nil];
        }
        
        // если произошла ошибка во время покупки
        if ([[[MKStoreManager sharedManager] resultString] isEqualToString:@"FAILED"]) {
            [self showMessage:ALERT_TASK_PURCHASE_NOT_VALIDATED_OR_FAILED
                        title:@"Информация"
                      message:@"Ошибка! Пожалуйста, повторите попытку или свяжитесь с нами."
                  firstButton:@"Обратная связь" secondButton:@"OK"];
        }

        return;
    }
    
    // Если покупка успешно выполнена
    if ([[data object] isEqualToString:@"paymentSuccess"]) {
        
        currentPurchaseTry = 1; // первая попытка сохранить данные о купленном товаре
        [self postForPurchase];
    }
    
}

// проверка купленной только что покупки на сервере + дальнейшее сохранение информации об этом
- (void)postForPurchase {
    
    label_extendAnAccess.text = @"проверяем ...";
    
    // запускаем таймер таймаута
    if (![timerPurchaseTimeout isValid])
          timerPurchaseTimeout = [NSTimer scheduledTimerWithTimeInterval:12
                                                          target:self
                                                        selector:@selector(taskPurchaseTimeout)
                                                        userInfo:nil
                                                         repeats:NO];
    
    // выполняем запрос на подтверждение покупки
    NSString * params = [NSString stringWithFormat:
                         @"action=purchase&type=%@&typedesc=%d&product=%@&userid=%@&id=%@&receipt=%@",
                         @"BUY",
                         currentPurchaseTry,
                         currentPurchaseProductID,
                         [[ArendaSingleton arenda] getMyUserID],
                         [[ArendaSingleton arenda] getMyID],
                         [self encodeReceiptsToBase64]];
    
    [[ArendaSingleton arenda] POST:params phpFile:PHP_FILE_PAYMENT];
}

// кодируем купленные покупки (сохраненные в аккаунте) для проверки на сервере
- (NSString *)encodeReceiptsToBase64 {
    
    NSURL * receiptURL  = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData * receipt    = [NSData dataWithContentsOfURL:receiptURL];
    
    // если покупок нет
    if (!receipt) return @"error";
    
    // если покупки есть
    return [[receipt base64EncodedStringWithOptions:0] stringByReplacingOccurrencesOfString:@"+" withString:@">P<"];
}

// Если вдруг сработал таймаут покупки или смены статуса
- (void)taskPurchaseTimeout {
    
    [timerPurchaseTimeout invalidate];
    
    if (currentPurchaseTry == 3) {
        
        [self enableShopButtons:YES];
        
        [self showMessage:ALERT_TASK_PURCHASE_NOT_VALIDATED_OR_FAILED
                    title:@"Информация"
                  message:@"К сожалению, Ваш платеж не подтвержден. Пожалуйста, свяжитесь с нами."
              firstButton:@"Обратная связь" secondButton:@"OK"];
        return;
    }
    
    // если это была попытка купить товар, то выводим сообщение и отправляем POST запрос заново
    [self showMessage:ALERT_TASK_PURCHASE_TIMEOUT
                title:@"Сервер недоступен"
              message:@"Невозможно сохранить информацию об оплате. Пожалуйста, проверьте соединение с Интернетом."
          firstButton:@"OK" secondButton:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
