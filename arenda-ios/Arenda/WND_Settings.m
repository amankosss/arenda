
#import "WND_Settings.h"
#import "AppDelegate.h"

@interface WND_Settings () {
    
    NSString * alertMessageTask;
}

@end

@implementation WND_Settings

@synthesize body_accountConfig, body_feedback, body_about, btn_push,  btn_changeMode, btn_changePhone, btn_contactUs, btn_rate1star, btn_rate2stars, btn_rate3stars, btn_rate4stars, btn_rate5stars;

#define BTN_RATE_HOVER          @"btn_rate_hover.png"
#define BTN_RATE                @"btn_rate.png"

#define BTN_PUSH_HOVER          @"btn_push_hover.png"
#define BTN_PUSH                @"btn_push.png"

#define TITLE_THANKS_FOR_RATE   @"Спасибо за оценку"
#define TITLE_FEEDBACK          @"Обратная связь"

#define TITLE_MODE_REALTOR      @"Перейти в режим риелтора"
#define TITLE_MODE_TENANT       @"Режим соискателя жилья"

// показать сообщение
- (void)showMessage:(NSString *)alert title:(NSString *)_messageTitle message:(NSString *)_message firstButton:(NSString *)_firstButton secondButton:(NSString *)_secondButton {
    
    alertMessageTask = alert;
    
    [[[UIAlertView alloc]
      initWithTitle:_messageTitle
      message:_message
      delegate:self
      cancelButtonTitle:_secondButton
      otherButtonTitles:_firstButton, nil] show];
}

// обработка нажатий кнопок в окнах сообщений
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    // если это отключение Push уведомлений
    if ([alertMessageTask isEqualToString:ALERT_TASK_DISABLE_PUSH_NOTIFICATIONS] && buttonIndex == 1) {
        
        [btn_push setImage:[UIImage imageNamed:BTN_PUSH_HOVER]
                  forState:UIControlStateNormal];
        
        [[ArendaSingleton arenda] saveBooleanToUserDefaults:YES forKey:@"disablePushNotifications"];
    }
    
    // если это смена режима
    if ([alertMessageTask isEqualToString:ALERT_TASK_CHANGE_MODE] && buttonIndex == 1) {
        
        
        int newMode = [[ArendaSingleton arenda] getCurrentMode] == MODE_TENANT ? MODE_REALTOR : MODE_TENANT;
        [[ArendaSingleton arenda] saveIntToUserDefaults:newMode forKey:@"currentMode"];
        
        [(AppDelegate *)[[UIApplication sharedApplication] delegate] changeViewController:newMode];
        return;
    }
    
    alertMessageTask = @"";
}

// обработчик ответов от сервера
//- (void)checkServerResult {
//    
//    // разбиваем полученный ответ на массив
//    NSArray * body = [[[ArendaSingleton arenda] resultString] componentsSeparatedByString:@"*"];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkServerResult) name:
//     @"settingsResults" object:nil];
    
    [ArendaSingleton arenda];
    
    [[ArendaSingleton arenda] roundCorners:body_accountConfig   radius:10];
    [[ArendaSingleton arenda] roundCorners:body_feedback        radius:10];
    
    [[ArendaSingleton arenda] roundCorners:btn_changeMode       radius:20];
    [[ArendaSingleton arenda] roundCorners:btn_changePhone      radius:20];
    [[ArendaSingleton arenda] roundCorners:btn_contactUs        radius:20];
    
    // окрашиваем рейтинг в нужное кол-во звездочек
    [self updateStars:[[ArendaSingleton arenda] loadIntFromUserDefaults:@"rateApp"]];
    
    // если мы в режиме риелтора, то показать кнопку смены номера и выключения Push уведомлений
    int currentMode         = [[ArendaSingleton arenda] getCurrentMode];
    btn_push.hidden         = currentMode == MODE_TENANT ? YES : NO;
    btn_changePhone.hidden  = currentMode == MODE_TENANT ? YES : NO;

    [btn_push setImage:[UIImage imageNamed:[[ArendaSingleton arenda] isPushNotificationsEnabled]
                            ? BTN_PUSH
                            : BTN_PUSH_HOVER]
              forState:UIControlStateNormal];
    
    // в зависимости от выбранного режима, установить нужный текст для кнопки смены режима
    [btn_changeMode setTitle:currentMode == MODE_TENANT
                                ? TITLE_MODE_REALTOR
                                : TITLE_MODE_TENANT
                    forState:UIControlStateNormal];
    
    // в зависимости от выбранного режима, изменить высоту содержимого "Настройки аккаунта"
    body_accountConfig.frame = CGRectMake(body_accountConfig.frame.origin.x,
                                  body_accountConfig.frame.origin.y,
                                  body_accountConfig.frame.size.width,
                                  currentMode == MODE_TENANT
                                        ? body_accountConfig.frame.size.height - 50
                                        : body_accountConfig.frame.size.height);
    
    // в зависимости от выбранного режима, изменить верхний отступ содержимого "О приложении"
    body_about.frame = CGRectMake(body_about.frame.origin.x,
                                  currentMode == MODE_TENANT
                                        ? body_about.frame.origin.y - 50
                                        : body_about.frame.origin.y,
                                  body_about.frame.size.width,
                                  body_about.frame.size.height);
}

// событие при показе данного окна
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    // сообщаем синглтону, что ответы от сервера нужно присылать в наше окно
    [ArendaSingleton arenda].currentWindow = @"settingsResults";
}

// окрашиваем нужное кол-во звездочек, взависимости от поставленной оценки
- (void)updateStars:(int)count {
    
    [btn_rate1star setImage:[UIImage imageNamed: count > 0
                                ? BTN_RATE_HOVER
                                : BTN_RATE]
                   forState:UIControlStateNormal];
    
    [btn_rate2stars setImage:[UIImage imageNamed: count > 1
                                ? BTN_RATE_HOVER
                                : BTN_RATE]
                    forState:UIControlStateNormal];
    
    [btn_rate3stars setImage:[UIImage imageNamed: count > 2
                                ? BTN_RATE_HOVER
                                : BTN_RATE]
                    forState:UIControlStateNormal];
    
    [btn_rate4stars setImage:[UIImage imageNamed: count > 3
                                ? BTN_RATE_HOVER
                                : BTN_RATE]
                    forState:UIControlStateNormal];
    
    [btn_rate5stars setImage:[UIImage imageNamed: count > 4
                                ? BTN_RATE_HOVER
                                : BTN_RATE]
                    forState:UIControlStateNormal];
}

// пользователь хочет закрыть окно настроек
- (IBAction)btn_close:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

// пользователь включает/отключает Push уведомления
- (IBAction)btn_push:(id)sender {
    
    // проверяем наличие интернета
    if (![self checkIntenet]) return;
    
    // если Push уведомления в данный момент включены
    if ([[ArendaSingleton arenda] isPushNotificationsEnabled]) {
        
        [self showMessage:ALERT_TASK_DISABLE_PUSH_NOTIFICATIONS
                    title:@"Отключить Push уведомления?"
                  message:@"В таком случае Вас не будут беспокоить соискатели жилья."
              firstButton:@"Да" secondButton:@"Нет"];
        return;
    }

    // если же выключены
    [btn_push setImage:[UIImage imageNamed:BTN_PUSH]
              forState:UIControlStateNormal];
    
    [[ArendaSingleton arenda] saveBooleanToUserDefaults:NO forKey:@"disablePushNotifications"];
}

// пользователь хочет сменить режим (соискатель или риелтор)
- (IBAction)btn_changeMode:(id)sender {
    
    // проверяем наличие интернета
    if (![self checkIntenet]) return;
    
    // проверяем, если ранее не было регистрации в качестве риелтора, то открываем регистрацию
    if ([[ArendaSingleton arenda] getMyUserID].length != 10) {
        
        [self performSegueWithIdentifier:SEGUE_SETTINGS_TO_WELCOME
                                  sender:nil];
        return;
    }
    
    [self showMessage:ALERT_TASK_CHANGE_MODE
                title:@"Вы действительно хотите сменить режим?"
              message:nil
          firstButton:@"Да" secondButton:@"Нет"];
}

// пользователь хочет сменить номер телефона
- (IBAction)btn_changePhone:(id)sender {
    
    // проверяем наличие интернета
    if (![self checkIntenet]) return;
    
    // показываем окно смены (4 шаг окна Welcome)
    [self performSegueWithIdentifier:SEGUE_SETTINGS_TO_WELCOME
                              sender:nil];
}

// пользователь хочет оценить приложение
- (void)rateApp:(int)stars selectedStar:(UIButton *)starButton {
    
    // если пользователь ставит туже оценку, что и раньше
    if ([[ArendaSingleton arenda] loadIntFromUserDefaults:@"rateApp"] == stars) return;
    
    // проверяем наличие интернета
    if (![self checkIntenet]) return;
    
    // сохраняем оценку и обновляем звездочки на экране
    [[ArendaSingleton arenda] saveIntToUserDefaults:stars forKey:@"rateApp"];
    [self updateStars:stars];
    
    // если пользователь поставил 4 или 5, то переходим в App Store
    if (stars > 3)
        [[ArendaSingleton arenda] openURL:URL_ARENDA_APPSTORE];
    else
        // если от 1 до 3, то показываем окно обратной связи с задержкой,
        // чтобы увидеть обновление звездочек на экране
        [self performSelector:@selector(showContactUs) withObject:nil afterDelay:0.1];
}

// показать окно обратной связи
- (void)showContactUs {
    [self contactUs:YES];
}

- (IBAction)btn_rate1star:(id)sender {
    
    [self rateApp:1 selectedStar:btn_rate1star];
}

- (IBAction)btn_rate2stars:(id)sender {

    [self rateApp:2 selectedStar:btn_rate1star];
}

- (IBAction)btn_rate3stars:(id)sender {
    
    [self rateApp:3 selectedStar:btn_rate1star];
}

- (IBAction)btn_rate4stars:(id)sender {
    
    [self rateApp:4 selectedStar:btn_rate1star];
}

- (IBAction)btn_rate5stars:(id)sender {
    
    [self rateApp:5 selectedStar:btn_rate1star];
}

- (void)contactUs:(BOOL)rateApp {
    
    [ArendaSingleton arenda].rateAppOrContactUs = rateApp
    ? TITLE_THANKS_FOR_RATE
    : TITLE_FEEDBACK;
    
    [self performSegueWithIdentifier:SEGUE_SETTINGS_TO_CONTACT_US
                              sender:nil];
}

// пользователь хочет связаться с нами
- (IBAction)btn_contactUs:(id)sender {
    
    // проверяем наличие интернета
    if (![self checkIntenet]) return;
    
    [self contactUs:NO];
}

// показать сообщение, что нет интернета, если его нет
- (BOOL)checkIntenet {
    
    if ([[ArendaSingleton arenda] checkInternet]) return YES;

    [self showMessage:nil
                title:@"Информация"
              message:@"В данный момент отсутствует соединение с Интернетом."
          firstButton:@"OK" secondButton:nil];
    
    return NO;
}

// пользователь нажимает "Мы в Facebook"
- (IBAction)btn_facebook:(id)sender {
    [[ArendaSingleton arenda] openURL:URL_FACEBOOK];
}

// пользователь нажимает "Мы в Twitter"
- (IBAction)btn_twitter:(id)sender {
    [[ArendaSingleton arenda] openURL:URL_TWITTER];
}

// пользователь нажимает "Мы ВКонтакте"
- (IBAction)btn_vkontakte:(id)sender {
    [[ArendaSingleton arenda] openURL:URL_VKONTAKTE];
}

// пользователь нажимает на ссылку "www.arenda-app.ru"
- (IBAction)btn_website:(id)sender {
    [[ArendaSingleton arenda] openURL:URL_ARENDA_HOMEPAGE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
