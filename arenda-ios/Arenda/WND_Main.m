
#import "WND_Main.h"
#import "WND_Welcome.h"
#import "ArendaSingleton.h"

@interface WND_Main () {

    BOOL keyboardIsVisible;         // клавиатура сейчас на экране?
    
    NSString * alertMessageTask;    // какая задача перед диалоговым сообщением?
    
    NSString * pushBodyParams;      // параметры для пуш уведомлений
    
    NSTimer * timerTimeout;         // таймер таймаута
    NSString * timeoutTask;         // какая задача перед таймаутом?
    
    BOOL flagErrorPhone;            // флаг - сообщение о бане на экране сейчас
    BOOL flagUpdateApp;             // флаг - сообщение обновитесь на экране сейчас
    BOOL flagErrorComment;          // флаг - сообщение о стоп-словах на экране сейчас
    BOOL flagCheckInternet;         // флаг - сообщение нет интернета на экране сейчас
    BOOL flagNoLocation;            // флаг - сообщение нет местоположения на экране сейчас
    
    int windowSession;              // сессия данного окна
    int originalSearchSettingsY;
    int minusKeyboardHeight;
    
    float searchSettingsRegionLatitude;
    float searchSettingsRegionLongitude;
}

@end

@implementation WND_Main

@synthesize btn_startSearch, btn_cancelSearch, body_startSearch, body_cancelSearch, img_pointer, img_blackBackground, body_searchSettings, btn_searchOK, slider_flatMaxCost, segment_rentTime, label_maxCost, slider_realtorMaxCost, label_maxPercent, textfield_comment, textfield_yourPhone, btn_searchCancel, segment_rooms, label_selectedRegion, loader_searchSettings, loader_cancelSearch, mapView;

// формируем строку параметров для сервера во время потдверждения поиска жилья
- (NSString *)getSubmittedParameters:(NSString *)task onlyAuth:(BOOL)onlyAuth {

    NSString * authPhone        = [[ArendaSingleton arenda] getMyPhoneNumber];
    NSString * authID           = [[ArendaSingleton arenda] getMyID];
    
    // если нам нужно передать только данные авторизации (для отмены поиска жилья)
    if (onlyAuth)
        return [NSString stringWithFormat:
                @"action=arenda&task=%@&userid=%@&phone=%@&id=%@",
                task,
                [[ArendaSingleton arenda] getMyUserID],
                authPhone,
                authID];

    NSString * selectedRegion   = [label_selectedRegion text];
    
    int selectedRooms           = [segment_rooms selectedSegmentIndex];
    int selectedRentTime        = [segment_rentTime selectedSegmentIndex];
    
    int selectedFlatMaxCost     = [slider_flatMaxCost value];
    selectedFlatMaxCost         *= segment_rentTime.selectedSegmentIndex == 0 ? 2000 : 500;
    
    int selectedRealtorMaxCost  = 0;
    
    if (segment_rentTime.selectedSegmentIndex == 0) {
        selectedRealtorMaxCost  = [slider_realtorMaxCost value];
        selectedRealtorMaxCost  *= 500;
    }
    
    NSString * enteredComment   = [textfield_comment text];
    
    pushBodyParams = [NSString stringWithFormat:
                      @"action=arenda&task=pushes&userid=%@&phone=%@&id=%@&rooms=%d&renttime=%d&cost=%d&regionlat=%f&regionlng=%f",
                      [[ArendaSingleton arenda] getMyUserID],
                      authPhone,
                      authID,
                      selectedRooms,
                      selectedRentTime,
                      selectedFlatMaxCost,
                      searchSettingsRegionLatitude,
                      searchSettingsRegionLongitude];

    return [NSString stringWithFormat:
            @"action=arenda&task=%@&userid=%@&phone=%@&id=%@&region=%@&rooms=%d&renttime=%d&cost=%d&realtor=%d&comment=%@&regionlat=%f&regionlng=%f",
            task,
            [[ArendaSingleton arenda] getMyUserID],
            authPhone,
            authID,
            selectedRegion,
            selectedRooms,
            selectedRentTime,
            selectedFlatMaxCost,
            selectedRealtorMaxCost,
            enteredComment,
            searchSettingsRegionLatitude,
            searchSettingsRegionLongitude];
}

// показать сообщение
- (void)showMessage:(NSString *)alert title:(NSString *)_messageTitle message:(NSString *)_message firstButton:(NSString *)_firstButton secondButton:(NSString *)_secondButton {
    
    alertMessageTask = alert;
    
    [[[UIAlertView alloc]
      initWithTitle:_messageTitle
      message:_message
      delegate:self
      cancelButtonTitle:_secondButton
      otherButtonTitles:_firstButton, nil] show];
}

// обработка нажатий кнопок в окнах сообщений
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    // если это подтверждение поиска жилья
    if ([alertMessageTask isEqualToString:ALERT_TASK_SUBMITTING_SEARCH] && buttonIndex == 1) {
        
        [[ArendaSingleton arenda] saveBooleanToUserDefaults:YES forKey:@"alreadySearched"];
            
        [self disableSearchSettings];
        [loader_searchSettings startAnimating];
            
        [self startTimeout:TASK_SUBMIT_SEARCH interval:10.0];
        [[ArendaSingleton arenda] POST:[self getSubmittedParameters:@"submit" onlyAuth:NO]
                               phpFile:PHP_FILE_USER];
    }
    
    // если это отмена подтверждения поиска жилья
    if ([alertMessageTask isEqualToString:ALERT_TASK_CANCELLING_SEARCH] && buttonIndex == 1) {
        
        btn_cancelSearch.hidden = YES;
        [loader_cancelSearch startAnimating];
        
        [self startTimeout:TASK_CANCEL_SEARCH interval:10.0];
        [[ArendaSingleton arenda] POST:[self getSubmittedParameters:@"cancel" onlyAuth:YES]
                               phpFile:PHP_FILE_USER];
    }
    
    // если это обновление приложения
    if ([alertMessageTask isEqualToString:ALERT_TASK_UPDATE_APP] && buttonIndex == 1)
        [[ArendaSingleton arenda] openURL:URL_ARENDA_APPSTORE];
    
    flagErrorPhone      = NO;
    flagUpdateApp       = NO;
    flagErrorComment    = NO;
    flagCheckInternet   = NO;
    flagNoLocation      = NO;
    
    alertMessageTask    = @"";
}

// обработчик ответов от сервера
- (void)checkServerResult {
    
    // если текущая сессия отличается (из-за смены режимов)
    if ([[ArendaSingleton arenda] loadIntFromUserDefaults:@"windowSession"] != windowSession)
        return;

    // разбиваем полученный ответ на массив
    NSArray * body = [[[ArendaSingleton arenda] resultString] componentsSeparatedByString:@"*"];
    
    // если это успешный ответ о подтверждении поиска жилья
    if ([[body objectAtIndex:0] isEqualToString:ECHO_SUBMIT_SUCCESS]) {
        
        [timerTimeout invalidate];
        [loader_searchSettings stopAnimating];
        
        [self enableSearchSettings];
        [self hideSearchSettings];
        
        [[ArendaSingleton arenda] animateView:body_cancelSearch time:0.2];
        body_cancelSearch.hidden = NO;
        
        [[ArendaSingleton arenda] saveStringToUserDefaults:label_selectedRegion.text forKey:@"selectedRegion"];
        
        // если мы до этого не показывали подсказку про Push уведомления
        // [ПОДСКАЗКА УБРАНА] [self isNeedToShowPushNotificationsRequest];

        [[ArendaSingleton arenda] POST:pushBodyParams phpFile:PHP_FILE_USER];
        
        return;
    }
    
    // если это отмена поиска жилья
    if ([[body objectAtIndex:0] isEqualToString:ECHO_CANCEL_SUCCESS]) {
        
        [timerTimeout invalidate];
        [loader_cancelSearch stopAnimating];
        
        btn_cancelSearch.hidden  = NO;
        body_cancelSearch.hidden = YES;
        
        [self showNormalSearchState];
        
        [[ArendaSingleton arenda] saveStringToUserDefaults:@"" forKey:@"selectedRegion"];
        
        return;
    }
    
    // если требуется обновить приложение
    if ([[body objectAtIndex:0] isEqualToString:ECHO_UPDATE_APP]) {
        
        // если это сообщение уже на экране (на всякий случай проверка)
        if (flagUpdateApp) return;
        flagUpdateApp = YES;
        
        [self stopTask];
        [self showMessage:ALERT_TASK_UPDATE_APP
                    title:[NSString stringWithFormat:@"Доступна версия %@", [body objectAtIndex:1]]
                  message:@"Пожалуйста, сначала обновите приложение."
              firstButton:@"Обновить"
             secondButton:@"Отмена"];
        
        return;
    }
    
    // если номер телефона недействителен (возможно бан)
    if ([[body objectAtIndex:0] isEqualToString:ECHO_ERROR_ACCOUNT]) {
        
        // если это сообщение уже на экране (на всякий случай проверка)
        if (flagErrorPhone) return;
        flagErrorPhone = YES;
        
        [self stopTask];
        [self showMessage:nil
                    title:@"Информация"
                  message:@"Ваша учетная запись недействительна."
              firstButton:@"OK" secondButton:nil];
        
        return;
    }
    
    // если комментарий соискателя содержит стоп-слова
    if ([[body objectAtIndex:0] isEqualToString:ECHO_ERROR_COMMENT]) {
        
        // если это сообщение уже на экране (на всякий случай проверка)
        if (flagErrorComment) return;
        flagErrorComment = YES;
        
        [self stopTask];
        [self showMessage:nil
                    title:@"Информация"
                  message:@"Ваш комментарий содержит запрещенные слова."
              firstButton:@"OK" secondButton:nil];
        
        return;
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkServerResult)
                                                 name:@"mainResults"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appWillEnterForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [ArendaSingleton arenda];
    
    windowSession = [[ArendaSingleton arenda]random:100000 maxNum:999999];
    [[ArendaSingleton arenda]saveIntToUserDefaults:windowSession forKey:@"windowSession"];
    
    [[ArendaSingleton arenda] activateLocation];

    mapView.delegate                = self;
    textfield_yourPhone.delegate    = self;
    textfield_comment.delegate      = self;
    
    [[ArendaSingleton arenda] roundCorners:btn_startSearch      radius:20];
    [[ArendaSingleton arenda] roundCorners:btn_searchCancel     radius:20];
    [[ArendaSingleton arenda] roundCorners:btn_searchOK         radius:20];
    [[ArendaSingleton arenda] roundCorners:btn_cancelSearch     radius:20];
    
    [[ArendaSingleton arenda] roundCorners:body_startSearch     radius:10];
    [[ArendaSingleton arenda] roundCorners:body_searchSettings  radius:10];
    [[ArendaSingleton arenda] roundCorners:body_cancelSearch    radius:10];
    
    // проверяем, нет ли поиска жилья в данный момент
    NSString * selectedRegion = [[ArendaSingleton arenda] loadStringFromUserDefaults:@"selectedRegion"];
    
    if (selectedRegion.length) {
        
        img_pointer.hidden = YES;
        [[ArendaSingleton arenda] animateView:body_cancelSearch time:0.2];
        body_cancelSearch.hidden = NO;
        
        [label_selectedRegion setText:selectedRegion];
    }

    [self goToMyLocation];
    
    // если мы до этого уже показывали подсказку про Push уведомления, то просто обновляем токен
    // [ПОДСКАЗКА УБРАНА] if ([[ArendaSingleton arenda] loadBooleanFromUserDefaults:@"pushRequested"])
    // [ПОДСКАЗКА УБРАНА]    [[ArendaSingleton arenda] activatePushNotifications];
    
    // получаем высоту экрана
    CGSize screenSizes = [[ArendaSingleton arenda] getScreenSizes];
    
    minusKeyboardHeight = 0;
    
    // если приложение запущено на iPhone 4
    if (screenSizes.height < 500)
        body_searchSettings.center = CGPointMake(body_searchSettings.center.x,
                                                 body_searchSettings.center.y + 6);
    
    // если приложение запущено на iPhone 5
    if (screenSizes.height > 481) minusKeyboardHeight = 50;
    
    // если приложение запущено на iPhone 6
    if (screenSizes.height > 569) minusKeyboardHeight = 120;
    
    // если приложение запущено на iPhone 6 plus
    if (screenSizes.height > 668) minusKeyboardHeight = 170;
    
    // отступ сверху у настроек поиска квартиры (по умолчанию)
    originalSearchSettingsY = body_searchSettings.frame.origin.y;
        
}

// событие при разворачивании приложения
- (void)appWillEnterForeground:(NSNotification *)notification {
    
    // если текущая сессия отличается (из-за смены режимов)
    if ([[ArendaSingleton arenda] loadIntFromUserDefaults:@"windowSession"] != windowSession)
        return;

    [[ArendaSingleton arenda] updateUserInfo];
}

// событие при показе данного окна
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    // если текущая сессия отличается (из-за смены режимов)
    if ([[ArendaSingleton arenda] loadIntFromUserDefaults:@"windowSession"] != windowSession)
        return;

    // сообщаем синглтону, что ответы от сервера нужно присылать в наше окно
    [ArendaSingleton arenda].currentWindow = @"mainResults";
    
    [[ArendaSingleton arenda] updateUserInfo];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

// событие при скрытии данного окна
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    
    // если текущая сессия отличается (из-за смены режимов)
    if ([[ArendaSingleton arenda] loadIntFromUserDefaults:@"windowSession"] != windowSession)
        return;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

// Устанавливаем максимальную длину ползунка "Не дороже" взависимости от кол-ва комнат [значение умножается на 100]
- (void)refreshMaxValue {
    int sliderMaxValue = 40;
    if (segment_rooms.selectedSegmentIndex == 1) sliderMaxValue = 80;
    if (segment_rooms.selectedSegmentIndex == 2) sliderMaxValue = 120;
    if (segment_rooms.selectedSegmentIndex == 3) sliderMaxValue = 160;
    if (segment_rooms.selectedSegmentIndex == 4) sliderMaxValue = 200;
    
    if (segment_rentTime.selectedSegmentIndex == 1) sliderMaxValue = 60;
    
    [slider_flatMaxCost setMaximumValue:sliderMaxValue];
}

// Метод обновления надписи "Не дороже: 14000 тг./мес."
- (void)refreshMaxCost {
    int sliderValue = [slider_flatMaxCost value];
    
    // если в данный момент пользователь выбирает "На длительный срок"
    if (segment_rentTime.selectedSegmentIndex == 0) {
        
        [slider_realtorMaxCost setMaximumValue:sliderValue];
        
        int sliderRealtorValue  = [slider_realtorMaxCost value];
        sliderRealtorValue      *= 500;
        
        [label_maxPercent setText:[NSString stringWithFormat:
                                   @"%% риелтору не более: %d тг.",
                                   sliderRealtorValue]];
    }

    sliderValue                 *= segment_rentTime.selectedSegmentIndex == 0 ? 2000 : 500;
    
    [label_maxCost setText:[[ArendaSingleton arenda] generateFlatCost:
                            sliderValue prefix:segment_rentTime.selectedSegmentIndex]];
}

// Пользователь меняет период проживания между "На длительный срок" и "Посуточно"
- (IBAction)segment_rentTime_changed:(id)sender {
    [label_maxPercent setEnabled:segment_rentTime.selectedSegmentIndex == 0];
    [slider_realtorMaxCost setEnabled:segment_rentTime.selectedSegmentIndex == 0];
    
    [self refreshMaxValue];
    [self refreshMaxCost];
}

// Пользователь меняет кол-во необходимых комнат
- (IBAction)segment_rooms_changed:(id)sender {
    [self refreshMaxValue];
    [self refreshMaxCost];
}

// Пользователь двигает ползунок "Не дороже: 14000 тг./мес."
- (IBAction)slider_flatMaxCost:(id)sender {
    [self refreshMaxCost];
}

// Пользователь двигает ползунок "% риелтору не более"
- (IBAction)slider_realtorMaxCost:(id)sender {
    [self refreshMaxCost];
}

// событие при показе клавиатуры на экране
- (void)keyboardWillShow:(NSNotification *)notification {
    
    // получаем высоту клавитуры
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // двигаем окно вверх с анимацией
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f    = body_searchSettings.frame;
        f.origin.y  = - (keyboardSize.height - minusKeyboardHeight);
        body_searchSettings.frame = f;
    }];
    
    keyboardIsVisible = YES;
}

// событие при скрытии клавиатуры с экрана
- (void)keyboardWillHide:(NSNotification *)notification {

    // двигаем окно вниз с анимацией
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f    = body_searchSettings.frame;
        f.origin.y  = originalSearchSettingsY;
        body_searchSettings.frame = f;
    }];
    
    keyboardIsVisible = NO;
}

// пользователь нажимает кнопку "Готово" на клавиатуре
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return NO;
}

// ограничиваем ввод комментария до 254 символов
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    // если это ввод в поле номера телефона
    if (textField.tag == 1) {
        if (newLength > TEXT_FIELD_PHONE) return NO;
        return YES;
    }
    
    // если это ввод в поле для комментария
    if ([string isEqualToString:@"'"] |
        [string isEqualToString:@"\""] |
        [string isEqualToString:@"+"] |
        [string isEqualToString:@":"] |
        [string isEqualToString:@"/"] |
        [string isEqualToString:@"\\"] |
        [string isEqualToString:@"*"] |
        [string isEqualToString:@"|"]) return NO;
    
    return (newLength > 254) ? NO : YES;
}

// [ПОДСКАЗКА УБРАНА] нужно ли показать подсказку на разрешение Push уведомлений?
- (void)isNeedToShowPushNotificationsRequest {
    
    // если данная подсказка уже показывалась ранее
    if ([[ArendaSingleton arenda] loadBooleanFromUserDefaults:@"pushRequested"]) return;
    
    // если данная подсказка уже показывалась в окне риелтора ранее
    if ([[ArendaSingleton arenda] loadBooleanFromUserDefaults:@"realtorPushRequested"]) {
        [[ArendaSingleton arenda] saveBooleanToUserDefaults:YES forKey:@"pushRequested"];
        return;
    }
        
    [[ArendaSingleton arenda] saveBooleanToUserDefaults:YES forKey:@"pushRequested"];
        
    [self showMessage:ALERT_TASK_PUSH_NOTIFICATION_HINT
                title:@"Пожалуйста, разрешите Push уведомления"
                message:@"Чтобы моментально получать важную информацию."
            firstButton:@"OK" secondButton:nil];

}

// Пользователь указал регион на карте и нажимает "Начать поиск"
- (IBAction)btn_startSearch:(id)sender {
    
    // проверяем наличие интернета
    if (![self checkIntenet]) return;
    
    // проверяем мое местоположение, активно ли?
    // [ПРОВЕРКА УБРАНА] if (![self isMyLocationEnabled]) return;
    
    [textfield_yourPhone setText:[[ArendaSingleton arenda] getMyPhoneNumber]];
    
    body_startSearch.hidden     = YES;
    img_pointer.hidden          = YES;
    
    img_blackBackground.hidden  = NO;
    
    [[ArendaSingleton arenda] animateView:body_searchSettings time:0.2];
    body_searchSettings.hidden  = NO;
}

// Пользователь указал необходимые параметры и условия и нажимает "OK"
- (IBAction)btn_searchOK:(id)sender {
    
    // вначале проверяем, на экране ли клавиатура (если что скрываем ее)
    if (keyboardIsVisible) {
        [textfield_yourPhone resignFirstResponder];
        [textfield_comment resignFirstResponder];
        return;
    }
    
    // затем проверяем наличие интернета
    if (![self checkIntenet]) return;
    
    // проверяем, что пользователь указал комиссию не меньше 10% от аренды
    if (segment_rentTime.selectedSegmentIndex == 0) {
        
        int checkFlatCost       = [slider_flatMaxCost value];
        int checkRealtorCost    = [slider_realtorMaxCost value];
        
        checkFlatCost           = checkFlatCost * 1000;
        checkRealtorCost        = checkRealtorCost * 500;
        
        if ((float)checkRealtorCost / (float)checkFlatCost < 0.1) {
            [self showMessage:nil
                        title:@"Информация"
                      message:@"Комиссия риелтору должна быть хотя бы 10% от аренды, иначе наврятли кто-то захочет сдать Вам жилье."
                  firstButton:@"OK" secondButton:nil];
            return;
        }
    }
    
    NSString * searchMyNumber = [textfield_yourPhone text];
    
    // Проверяем длину номера телефона на 10 цифр
    if (searchMyNumber.length != 10) {
        [self showMessage:nil
                    title:@"Информация"
                  message:@"Номер телефона должен состоять из 10 цифр."
              firstButton:@"OK" secondButton:nil];
        return;
    }
    
    // [Проверку убрали] Проверяем, в правильном ли формате указан номер
//    if (![[searchMyNumber substringToIndex:1] isEqualToString:@"9"]) {
//        [self showMessage:nil
//                    title:@"Информация"
//                  message:@"Номер телефона должен начинаться с цифры 9."
//              firstButton:@"OK" secondButton:nil];
//        return;
//    }
    
    [[ArendaSingleton arenda] saveStringToUserDefaults:searchMyNumber forKey:@"authPhone"];
    
    BOOL alreadySearched = [[ArendaSingleton arenda] loadBooleanFromUserDefaults:@"alreadySearched"];
    
    // ниже формируем строку красивого сообщения "Вы хотите найти квартиру ..."
    NSString * messageText = alreadySearched == NO ? @"Обращаем Ваше внимание, что нажимая \"Да\", Вам незамедлительно начнут поступать звонки.\n\n" : @"";
    
    NSString * roomsText = [[ArendaSingleton arenda] generateRooms:segment_rooms.selectedSegmentIndex];

    NSString * rentTimeText = segment_rentTime.selectedSegmentIndex == 0 ? @"на длительный срок" : @"посуточно";
    
    NSString * maxPercentText = segment_rentTime.selectedSegmentIndex == 0 ? [NSString stringWithFormat:@" с %@", [label_maxPercent text]] : @".";
    
    messageText = [[NSString stringWithFormat:
                    @"%@Вы хотите найти %@ в районе \"%@\", %@. %@%@",
                    messageText,
                    roomsText,
                    [label_selectedRegion text],
                    rentTimeText,
                    [label_maxCost text],
                    maxPercentText] stringByReplacingOccurrencesOfString:@":" withString:@""];
    
    messageText = [NSString stringWithFormat:
                   @"%@\n\nВаш телефон: +7%@",
                   messageText,
                   [[ArendaSingleton arenda] getMyPhoneNumber]];
    
    // красивая строка сформирована, теперь показываем сообщение
    [self showMessage:ALERT_TASK_SUBMITTING_SEARCH
                title:@"Всё правильно?"
              message:messageText
          firstButton:@"Да"
         secondButton:@"Нет"];
}

// Пользователь не хочет указывать параметры поиска и нажимает "Отмена"
- (IBAction)btn_searchCancel:(id)sender {
    [self hideSearchSettings];
    [self showNormalSearchState];
    
    // если мы до этого не показывали подсказку про Push уведомления
    // [ПОДСКАЗКА УБРАНА] [self isNeedToShowPushNotificationsRequest];
}

// Пользователь нашел квартиру и теперь нажимает "Отменить поиск"
- (IBAction)btn_cancelSearch:(id)sender {
    
    // проверяем наличие интернета
    if (![self checkIntenet]) return;
    
    // проверяем мое местоположение, активно ли?
    // [ПРОВЕРКА УБРАНА] if (![self isMyLocationEnabled]) return;
    
    [self showMessage:ALERT_TASK_CANCELLING_SEARCH
                title:@"Вы уже нашли жильё?"
              message:@"Нажмите \"Да\", чтобы Вас больше не беспокоили звонками."
          firstButton:@"Да"
         secondButton:@"Нет"];
}

- (void)disableSearchSettings {
    
    btn_searchOK.hidden             = YES;
    btn_searchCancel.hidden         = YES;
    
    segment_rooms.enabled           = NO;
    segment_rentTime.enabled        = NO;
    
    slider_flatMaxCost.enabled      = NO;
    slider_realtorMaxCost.enabled   = NO;
    
    textfield_yourPhone.enabled     = NO;
    textfield_comment.enabled       = NO;
}

- (void)enableSearchSettings {
    
    btn_searchOK.hidden             = NO;
    btn_searchCancel.hidden         = NO;
    
    segment_rooms.enabled           = YES;
    segment_rentTime.enabled        = YES;
    
    slider_flatMaxCost.enabled      = YES;
    slider_realtorMaxCost.enabled   = YES;
    
    textfield_yourPhone.enabled     = YES;
    textfield_comment.enabled       = YES;
}

- (void)hideSearchSettings {
    [textfield_yourPhone resignFirstResponder];
    [textfield_comment resignFirstResponder];
    
    img_blackBackground.hidden = YES;
    body_searchSettings.hidden = YES;
}

- (void)showNormalSearchState {
    
    img_pointer.hidden = NO;
    [[ArendaSingleton arenda] animateView:body_startSearch time:0.2];
    body_startSearch.hidden = NO;
}

// запускаем таймер таймаута
- (void)startTimeout:(NSString *)task interval:(int)sec {
    
    timeoutTask = task;
    
    timerTimeout = [NSTimer scheduledTimerWithTimeInterval:sec
                                                    target:self
                                                  selector:@selector(taskTimeout)
                                                  userInfo:nil
                                                   repeats:NO];
}

// остановить таймер, все действия и показать ошибку
- (void)stopTask {
    
    [timerTimeout invalidate];
    
    // если это было подтверждение поиска жилья
    if ([timeoutTask isEqualToString:TASK_SUBMIT_SEARCH]) {
        
        [self enableSearchSettings];
        [loader_searchSettings stopAnimating];
    }
    
    // если это была отмена поиска жилья
    if ([timeoutTask isEqualToString:TASK_CANCEL_SEARCH]) {
        
        btn_cancelSearch.hidden = NO;
        [loader_cancelSearch stopAnimating];
    }
    
}

// сработал таймаут
- (void)taskTimeout {
    
    [self stopTask];
    
    [self showMessage:nil
                title:@"Проблема с Интернетом"
              message:@"Пожалуйста, повторите попытку."
          firstButton:@"OK" secondButton:nil];
}

// пользователь хочет показать окно настроек приложения
- (IBAction)btn_settings:(id)sender {
    
    if (!img_blackBackground.hidden) return;
    
    [self performSegueWithIdentifier:SEGUE_MAP_TO_SETTINGS
                              sender:nil];
}

// пользователь нажимает "Где я?"
- (IBAction)btn_myLocation:(id)sender {
    [self goToMyLocation];
}

// и выполняет метод перехода к текущему местоположению
- (void)goToMyLocation {
    
    if (!img_blackBackground.hidden) return;
    
    // проверяем мое местоположение, активно ли?
    if (![self isMyLocationEnabled]) return;
    
    // получаем мои текущие координаты и делаем переход к ним
    CLLocationCoordinate2D goToLocation;
    
    goToLocation.latitude   = [[ArendaSingleton arenda] getMyLocation].latitude;
    goToLocation.longitude  = [[ArendaSingleton arenda] getMyLocation].longitude;
    
    MKCoordinateRegion viewRegion       = MKCoordinateRegionMakeWithDistance(goToLocation, 3000, 3000);
    MKCoordinateRegion adjustedRegion   = [self.mapView regionThatFits:viewRegion];
    
    [self.mapView setRegion:adjustedRegion animated:YES];
}

// проверяем мое местоположение, активно ли?
- (BOOL)isMyLocationEnabled {
    
    // если местоположение определить невозможно
    if ([[ArendaSingleton arenda] getMyLocation].latitude == 0.000000) {
        
        NSString * messageText = [NSString stringWithFormat:
                                  @"Пожалуйста, разрешите Arenda в Настройках > %@ > Службы геолокации.",
                                  [[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0
                                  ? @"Конфиденциальность" : @"Приватность"];
        
        if (flagNoLocation) return NO;
        flagNoLocation = YES;
        
        [self showMessage:nil
                    title:@"Невозможно определить Ваше местоположение"
                  message:messageText
              firstButton:@"OK" secondButton:nil];
        
        return NO;
    }
    
    return YES;
}

// метод получения заголовка региона поиска жилья, например "Уфа, Проспект октября"
- (void)getSelectedRegion {
    
    CLLocationCoordinate2D pointerCoordinates = [mapView convertPoint:CGPointMake(img_pointer.frame.origin.x + 50, img_pointer.frame.origin.y + 100) toCoordinateFromView:mapView];
    
    CLLocation * selectedRegion = [[CLLocation alloc]  initWithLatitude:pointerCoordinates.latitude longitude:pointerCoordinates.longitude];
    
    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
    
    [geoCoder reverseGeocodeLocation:selectedRegion
                   completionHandler:^(NSArray * placemarks, NSError * error) {

                       NSString * selectedRegion = @"подвиньте или увеличьте карту";
                       body_startSearch.hidden = YES;
                       
                       if (error == nil && [placemarks count] > 0) {
                           
                           CLPlacemark * placemark = [placemarks lastObject];
                           
                           if (placemark.locality && placemark.thoroughfare) {
  
                               [[ArendaSingleton arenda] animateView:body_startSearch time:0.2];
                               body_startSearch.hidden = NO;
                               
                               selectedRegion = [NSString stringWithFormat:@"%@, %@", placemark.locality, placemark.thoroughfare];
                               
                               searchSettingsRegionLatitude = pointerCoordinates.latitude;
                               searchSettingsRegionLongitude = pointerCoordinates.longitude;
                           }
                           
                       }
                       
                       label_selectedRegion.text = selectedRegion;
    }];
}

// изменение масштаба и сдвиг региона просмотра карты
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    
    // если в данный момент уже поиск осуществляется
    if (img_pointer.hidden) return;
        
    // проверяем наличие интернета
    if (![self checkIntenet]) return;
        
    [[ArendaSingleton arenda] animateView:label_selectedRegion time:0.2];
    label_selectedRegion.hidden = NO;
        
    label_selectedRegion.text = @"...";
    [self getSelectedRegion];

}

// перед изменением масштаба и сдвига региона просмотра карты
- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated {
    
    if (img_pointer.hidden) return;
    
    searchSettingsRegionLatitude  = 0.000000;
    searchSettingsRegionLongitude = 0.000000;
    
    [[ArendaSingleton arenda] animateView:body_startSearch      time:0.2];
    [[ArendaSingleton arenda] animateView:label_selectedRegion  time:0.2];
    
    body_startSearch.hidden     = YES;
    label_selectedRegion.hidden = YES;
}

// показать сообщение, что нет интернета, если его нет
- (BOOL)checkIntenet {
    
    if ([[ArendaSingleton arenda] checkInternet]) return YES;
    
    if (flagCheckInternet) return NO;
    flagCheckInternet = YES;
    
    [self showMessage:nil
                title:@"Информация"
              message:@"В данный момент отсутствует соединение с Интернетом."
          firstButton:@"OK" secondButton:nil];
    
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
