
#import <UIKit/UIKit.h>
#import "ArendaSingleton.h"

@interface WND_ContactUs: UIViewController <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel * label_title;
@property (weak, nonatomic) IBOutlet UITextView * textview_field;
@property (weak, nonatomic) IBOutlet UIButton * btn_send;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView * loader;

- (IBAction)btn_close:(id)sender;
- (IBAction)btn_send:(id)sender;

@end
