
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface WND_Main : UIViewController <MKMapViewDelegate, UITextFieldDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView * body_startSearch;
@property (weak, nonatomic) IBOutlet UIScrollView * body_cancelSearch;
@property (weak, nonatomic) IBOutlet UIScrollView * body_searchSettings;

@property (weak, nonatomic) IBOutlet UIButton * btn_startSearch;
@property (weak, nonatomic) IBOutlet UIButton * btn_cancelSearch;
@property (weak, nonatomic) IBOutlet UIButton * btn_searchOK;
@property (weak, nonatomic) IBOutlet UIButton * btn_searchCancel;

@property (weak, nonatomic) IBOutlet UIImageView * img_pointer;
@property (weak, nonatomic) IBOutlet UIImageView * img_blackBackground;

@property (weak, nonatomic) IBOutlet UISlider * slider_flatMaxCost;
@property (weak, nonatomic) IBOutlet UISlider * slider_realtorMaxCost;

@property (weak, nonatomic) IBOutlet UISegmentedControl * segment_rooms;
@property (weak, nonatomic) IBOutlet UISegmentedControl * segment_rentTime;

@property (weak, nonatomic) IBOutlet UILabel * label_selectedRegion;
@property (weak, nonatomic) IBOutlet UILabel * label_maxCost;
@property (weak, nonatomic) IBOutlet UILabel * label_maxPercent;

@property (weak, nonatomic) IBOutlet UITextField * textfield_comment;
@property (weak, nonatomic) IBOutlet UITextField * textfield_yourPhone;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView * loader_searchSettings;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader_cancelSearch;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

- (IBAction)btn_startSearch:(id)sender;
- (IBAction)btn_cancelSearch:(id)sender;
- (IBAction)btn_searchOK:(id)sender;
- (IBAction)btn_searchCancel:(id)sender;

- (IBAction)btn_settings:(id)sender;
- (IBAction)btn_myLocation:(id)sender;

- (IBAction)segment_rentTime_changed:(id)sender;
- (IBAction)segment_rooms_changed:(id)sender;

- (IBAction)slider_flatMaxCost:(id)sender;
- (IBAction)slider_realtorMaxCost:(id)sender;

@end
