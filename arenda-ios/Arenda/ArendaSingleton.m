
#import "Reachability.h"
#import "ArendaSingleton.h"
#import <sys/utsname.h>

@implementation ArendaSingleton

@synthesize resultString, currentWindow, rateAppOrContactUs;

    NSMutableData * receivedData;                   // серверная обработка данных

    NSUserDefaults * userDefaults;                  // мои сохранения
    CLLocationManager * locationManager;            // мои текущие координаты на карте

static ArendaSingleton * _arenda;

+ (ArendaSingleton *) arenda {
    
    @synchronized(self) {
        if (_arenda == nil) {
            [[self alloc] init];
            userDefaults = [NSUserDefaults standardUserDefaults];
        }
    }
    
    return _arenda;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (_arenda == nil) {
            _arenda = [super allocWithZone:zone];
            return _arenda;
        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

// получить ссылку на сайт
- (NSString *)getAppWebsite {
    return @"http://arenda.890m.com";
}

// получить номер версии приложения
- (NSString *)getAppVersion {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

// генератор случайных чисел
- (NSInteger)random:(NSInteger)min maxNum:(NSInteger)max {
    return min + arc4random() % (max - min + 1);
}

// получить номер версии iOS
- (NSString *)getOSVersion {
    return [[UIDevice currentDevice] systemVersion];
}

- (NSString *)getDeviceNameByModel:(NSString *)model {
    if ([model isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([model isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([model isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([model isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([model isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([model isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([model isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([model isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM CDMA)";
    if ([model isEqualToString:@"iPhone5,3"])    return @"iPhone 5C (GSM)";
    if ([model isEqualToString:@"iPhone5,4"])    return @"iPhone 5C (GSM CDMA)";
    if ([model isEqualToString:@"iPhone6,1"])    return @"iPhone 5S (GSM)";
    if ([model isEqualToString:@"iPhone6,2"])    return @"iPhone 5S (GSM CDMA)";
    if ([model isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([model isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([model isEqualToString:@"iPhone8,1"])    return @"iPhone 6S";
    if ([model isEqualToString:@"iPhone8,2"])    return @"iPhone 6S Plus";
    if ([model isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([model isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([model isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([model isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([model isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([model isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([model isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([model isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([model isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([model isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
    if ([model isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([model isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([model isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM CDMA)";
    if ([model isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([model isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM CDMA)";
    if ([model isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([model isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([model isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([model isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM CDMA)";
    if ([model isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
    if ([model isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([model isEqualToString:@"iPad4,3"])      return @"iPad Air";
    if ([model isEqualToString:@"iPad4,4"])      return @"iPad Mini 2G (WiFi)";
    if ([model isEqualToString:@"iPad4,5"])      return @"iPad Mini 2G (Cellular)";
    if ([model isEqualToString:@"iPad4,6"])      return @"iPad Mini 2G";
    if ([model isEqualToString:@"iPad4,7"])      return @"iPad Mini 3 (WiFi)";
    if ([model isEqualToString:@"iPad4,8"])      return @"iPad Mini 3 (Cellular)";
    if ([model isEqualToString:@"iPad4,9"])      return @"iPad Mini 3 (China)";
    if ([model isEqualToString:@"iPad5,3"])      return @"iPad Air 2 (WiFi)";
    if ([model isEqualToString:@"iPad5,4"])      return @"iPad Air 2 (Cellular)";
    if ([model isEqualToString:@"AppleTV2,1"])   return @"Apple TV 2G";
    if ([model isEqualToString:@"AppleTV3,1"])   return @"Apple TV 3";
    if ([model isEqualToString:@"AppleTV3,2"])   return @"Apple TV 3 (2013)";
    if ([model isEqualToString:@"i386"])         return @"Simulator";
    if ([model isEqualToString:@"x86_64"])       return @"Simulator";
    return model;
}

// получить название устройства
- (NSString *)getDeviceModel {
    struct utsname systemInfo;
    uname(&systemInfo);
    return [self getDeviceNameByModel:[NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding]];
}

// открыть страницу в браузере
- (void)openURL:(NSString *)link {
    
    NSString * website = [self getAppWebsite];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", website, link]]];
}

// получить мой UserID
- (NSString *)getMyUserID {
    
    NSString * authUserID = [self loadStringFromUserDefaults:@"authUserID"];
    if (authUserID.length == 0) authUserID = @"";
    
    return authUserID;
}

// получить номер моего телефона
- (NSString *)getMyPhoneNumber {
    
    NSString * authPhone = [self loadStringFromUserDefaults:@"authPhone"];
    if (authPhone.length == 0) authPhone = @"";
    
   return authPhone;
}

// получить мой ID в системе
- (NSString *)getMyID {
    
    NSString * authID = [self loadStringFromUserDefaults:@"authID"];
    if (authID.length == 0) authID = @"";
    
    return authID;
}

// получить размеры экрана
- (CGSize)getScreenSizes {
    return [[UIScreen mainScreen] bounds].size;
}

// включены ли Push уведомления
- (BOOL)isPushNotificationsEnabled {
    return ![[ArendaSingleton arenda] loadBooleanFromUserDefaults:@"disablePushNotifications"];
}

// получить текущий режим
- (int)getCurrentMode {
    return [[ArendaSingleton arenda] loadIntFromUserDefaults:@"currentMode"];
}

- (int)loadIntFromUserDefaults:(NSString *)keyName {
    return [userDefaults integerForKey:keyName];
}

- (void)saveIntToUserDefaults:(int)value forKey:(NSString *)keyName {
    [userDefaults setInteger:value forKey:keyName];
    [userDefaults synchronize];
}

- (BOOL)loadBooleanFromUserDefaults:(NSString *)keyName {
    return [userDefaults boolForKey:keyName];
}

- (void)saveBooleanToUserDefaults:(BOOL)value forKey:(NSString *)keyName {
    [userDefaults setBool:value forKey:keyName];
    [userDefaults synchronize];
}

- (NSString *)loadStringFromUserDefaults:(NSString *)keyName {
    return [userDefaults objectForKey:keyName];
}

- (void)saveStringToUserDefaults:(NSString *)value forKey:(NSString *)keyName {
    [userDefaults setObject:value forKey:keyName];
    [userDefaults synchronize];
}

- (NSArray *)loadPhonesArray {
    return [userDefaults objectForKey:@"phonesArray"];
}

- (NSString *)generateRooms:(int)rooms {
    
    NSString * roomsText = @"комнату";
    if (rooms == 1) roomsText = @"1-комнатную квартиру";
    if (rooms == 2) roomsText = @"2-комнатную квартиру";
    if (rooms == 3) roomsText = @"3-комнатную квартиру";
    if (rooms == 4) roomsText = @"4+комнатную квартиру";
    
    return roomsText;
}

- (NSString *)generateFlatCost:(int)cost prefix:(int)_prefix {
    
    NSString * rentTime = _prefix == 0 ? @"мес." : @"сутки";
    return [NSString stringWithFormat:@"Не дороже: %d тг./%@", cost, rentTime];
}

// получение даты из UNIX времени юзера
- (NSString *) getTime:(NSString *)userUnix {

    // получение значения GMT+ или GTM-
    NSDateFormatter * gmtFormat = [[NSDateFormatter alloc] init];
    [gmtFormat setDateFormat:@"Z"];
    
    NSString * gmtTime          = [gmtFormat stringFromDate:[NSDate date]];
    int gmtUnix                 = [[gmtTime substringToIndex:3] intValue];
    gmtUnix                     = gmtUnix * 60 * 60;
    
    // получение окончательного Unix-времени с учетом GMT
    int unix                    = [userUnix intValue] + gmtUnix;
    
    // преобразования Unix-времени в полную дату
    NSDate * date               = [NSDate dateWithTimeIntervalSince1970:unix];
    NSDateFormatter * msgTime   = [[NSDateFormatter alloc] init];
    NSDateFormatter * nowTime   = [[NSDateFormatter alloc]init];
    [msgTime setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [msgTime setDateFormat:@"H:mm"];
    
    NSString * resultTime       = [msgTime stringFromDate:date];

    [msgTime setDateFormat:@"d MMM"];
    [nowTime setDateFormat:@"d MMM"];
    
    NSString * resultDate       = [msgTime stringFromDate:date];
    
    NSString * todayDate        = [nowTime stringFromDate:[NSDate date]];
    NSString * yesterdayDate    = [nowTime stringFromDate:
    [NSDate dateWithTimeIntervalSince1970:[[NSDate date] timeIntervalSince1970] - 86400]];
    
    resultDate = [resultDate stringByReplacingOccurrencesOfString:todayDate withString:@"сегодня"];
    resultDate = [resultDate stringByReplacingOccurrencesOfString:yesterdayDate withString:@"вчера"];

    return [NSString stringWithFormat:
            @"%@ %@ %@",
            resultDate,
            @"в",
            resultTime];
}

// округляем углы элементов
- (void)roundCorners:(UIView *)view radius:(CGFloat)value {
    CALayer * cl = [view layer];
    [cl setMasksToBounds:YES];
    [cl setCornerRadius:value];
}

// добавление анимации Fade эффекта
- (void)animateView:(UIView *)view time:(NSTimeInterval)seconds {
    [UIView transitionWithView:view
                      duration:seconds
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:NULL
                    completion:NULL];
}

// инициализация получения координат пользователя
- (void)activateLocation {

    locationManager = [[CLLocationManager alloc] init];
    
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        [locationManager requestWhenInUseAuthorization];
    
    locationManager.delegate        = self;
    locationManager.distanceFilter  = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    
    [locationManager startUpdatingLocation];
}

// регистрация приложения в сервисе Push уведомлений для получения Device Token
- (void)activatePushNotifications {
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]){
        // если это iOS 8
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings
settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
    } else {
        // если iOS 7
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
}

// сохранение Device Token на сервере
- (void)saveDeviceTokenOnServer:(NSString *)token {
    
    if (![self checkInternet]) return;
    
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    NSString * params = [NSString stringWithFormat:
                         @"action=settings&task=updatetoken&userid=%@&phone=%@&id=%@&token=%@",
                         [self getMyUserID],
                         [self getMyPhoneNumber],
                         [self getMyID],
                         token];
    
    [self POST:params phpFile:PHP_FILE_SETTINGS];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    [locationManager stopUpdatingLocation];
}

// получить текущие координаты
- (CLLocationCoordinate2D)getMyLocation {
    return locationManager.location.coordinate;
}

// проверка интернет соединения
- (BOOL)checkInternet {

    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    SCNetworkReachabilityRef r = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault,
                                                                        (const struct sockaddr*)&zeroAddress);
    
    if (r != NULL) {
        
        SCNetworkReachabilityFlags flags;
        
        if (SCNetworkReachabilityGetFlags(r, &flags)) {
            if ((flags & kSCNetworkReachabilityFlagsReachable) == 0) {return NO;}
            if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0) {return YES;}
            
            if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
                 (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0)) {
                if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0) {return YES;}
            }
            
            if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN) {return YES;}
        }
        
    }
    
    return NO;
}

// обновлении информации о пользователе на сервере
- (void)updateUserInfo {
    
    if (![self checkInternet]) return;
    
    BOOL notify = [self isPushNotificationsEnabled];
    
    // если сейчас режим соискателя, то push уведомления всегда включены
    if ([self getCurrentMode] == 1) notify = YES;
    
    [self POST:[NSString stringWithFormat:
                @"action=settings&task=updateinfo&userid=%@&phone=%@&id=%@&stars=%d&mode=%d&notify=%d",
                [self getMyUserID],
                [self getMyPhoneNumber],
                [self getMyID],
                [self loadIntFromUserDefaults:@"rateApp"],
                [self getCurrentMode],
                notify
                ] phpFile:PHP_FILE_SETTINGS];
}

// отправка данных на сервер
- (void)POST:(NSString *)body phpFile:(NSString *)_phpFile {

    body = [NSString stringWithFormat:
            @"%@&app=%@&os=%@&version=%@&device=%@&latitude=%f&longitude=%f",
            body,
            [self getAppVersion],
            @"iOS",
            [self getOSVersion],
            [self getDeviceModel],
            [self getMyLocation].latitude,
            [self getMyLocation].longitude];
    
    NSString * url = [NSString stringWithFormat:@"%@/%@", [self getAppWebsite], _phpFile];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:15.0];
    
    request.HTTPMethod = @"POST";
    request.HTTPBody = [body dataUsingEncoding:NSUTF8StringEncoding];
    NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (connection) receivedData = [[NSMutableData data] init];
}

// ставим каретку в начало, если ответ получен
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [receivedData setLength:0];
}

// записываем получаемые данные
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [receivedData appendData:data];
}

// обработка ошибки при получении данных
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"Error - %@", error);
}

// обработка ответа
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    // получаем содержимое ответа и проверяем не равна ли длина пустоте
    resultString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@", resultString);
    if (resultString.length == 0) return;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:currentWindow object:nil];
}

@end