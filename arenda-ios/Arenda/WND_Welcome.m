
#import "WND_Welcome.h"
#import "AppDelegate.h"

@interface WND_Welcome () {
    
    NSString * authUserID,                  // телефон пользователя при сохранении
             * authPhone,                   // телефон пользователя при вводе
             * authID;                      // случайный id
    
    BOOL phoneChanging,                     // если это изменение номера телефона
         codeSubmitting;                    // если это подтверждение кодом
    
    int welcomeStep,                        // текущий шаг приветствия
        selectedMode;                       // какой режим (риелтор или соискатель) выбран
    
    NSString * alertMessageTask;            // какая задача перед диалоговым сообщением?

    NSTimer * timerTimeout;                 // таймер таймаута
    NSString * timeoutTask;                 // какая задача перед таймаутом?
    
    NSString * getCodeString;               // строка параметров (при запросе кода)
}

@end

@implementation WND_Welcome

@synthesize body_registration, label_plus7, label_registration, label_title, label_description, label_userAgreement, img_stepIcon, img_blackLine,  btn_next, btn_continue, btn_user, btn_realtor, btn_close, textfield_phone, btn_userAgreement, btn_resendCode, loader, loader_userRegistration;


- (void)textFieldDidChange:(UITextField *)textField {
    btn_continue.enabled = textField.text.length > 0 ? YES : NO;
}

// показать сообщение
- (void)showMessage:(NSString *)alert title:(NSString *)_messageTitle message:(NSString *)_message firstButton:(NSString *)_firstButton secondButton:(NSString *)_secondButton {
    
    alertMessageTask = alert;
    
    [[[UIAlertView alloc]
      initWithTitle:_messageTitle
      message:_message
      delegate:self
      cancelButtonTitle:_secondButton
      otherButtonTitles:_firstButton, nil] show];
}

// обработка нажатий кнопок в окнах сообщений
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    // человек хочет выслать код повторно
    if ([alertMessageTask isEqualToString:ALERT_TASK_RESEND_CODE] && buttonIndex == 1) {
        
        [textfield_phone resignFirstResponder];
        
        timeoutTask = TASK_NEW_CODE;
        [[ArendaSingleton arenda] POST:getCodeString phpFile:PHP_FILE_REG];
    }
    
    // если человек ввел номер телефона и нажал ДА
    if ([alertMessageTask isEqualToString:ALERT_TASK_CHECK_NUMBER] && buttonIndex == 1) {
        
        [textfield_phone resignFirstResponder];
        
        label_plus7.hidden          = YES;
        
        label_userAgreement.hidden  = YES;
        btn_userAgreement.hidden    = YES;
        
        btn_continue.hidden         = YES;
        textfield_phone.hidden      = YES;
        img_blackLine.hidden        = YES;
        
        [[ArendaSingleton arenda] animateView:loader time:0.2];
        [loader startAnimating];
        
        // если это первичная регистрация
        if (!phoneChanging) {
            getCodeString = [NSString stringWithFormat:
                             @"action=newuser&task=newcode&phone=%@",
                             textfield_phone.text];
        } else {
            // если это изменение телефона, то добавляем к запросу информацию о текущем телефоне и ID
            getCodeString = [NSString stringWithFormat:
                             @"action=newuser&task=newcode&phone=%@&oldphone=%@&id=%@",
                             textfield_phone.text,
                             [[ArendaSingleton arenda] getMyUserID],
                             [[ArendaSingleton arenda] getMyID]];
        }
        
        [self startTimeout:30];
        timeoutTask = TASK_NEW_CODE;
        [[ArendaSingleton arenda] POST:getCodeString phpFile:PHP_FILE_REG];
    }
    
    alertMessageTask = @"";
}

// ограничиваем ввод номера телефона до 10 цифр и кода до 6 цифр
- (BOOL)textField:(UITextField *)textField
    shouldChangeCharactersInRange:(NSRange)range
                replacementString:(NSString *)string {
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;

    int maxLength = codeSubmitting ? TEXT_FIELD_CODE : TEXT_FIELD_PHONE;
    return (newLength > maxLength) ? NO : YES;
}

// обработчик ответов от сервера
- (void)checkServerResult {
    
    // разбиваем полученный ответ на массив
    NSArray * body = [[[ArendaSingleton arenda] resultString] componentsSeparatedByString:@"*"];
    
    NSString * command = [body objectAtIndex:0];
    
    // если это успешная регистрация соискателя
    if ([command isEqualToString:ECHO_REG_TENANT_SUCCESS]) {
        
        [timerTimeout invalidate];
        
        // делаем небольшую задержку в 0.5 сек и завершаем процесс регистрации
        [self performSelector:@selector(saveAuthInfoAndStart) withObject:nil afterDelay:0.5];
        return;
    }

    // если такой соискатель уже существует
    if ([command isEqualToString:ECHO_TENANT_EXISTS]) {
        
        [timerTimeout invalidate];
        
        [self returnModeButtons:true];
        
        [self showMessage:nil
                    title:@"Информация"
                  message:@"Произошла ошибка! Пожалуйста, повторите действие."
              firstButton:@"OK" secondButton:nil];
        return;
    }
    
    // если требуется подтвердить номер телефона паролем
    if ([command isEqualToString:ECHO_CODE_SENT]) {
        
        [timerTimeout invalidate];
        
        if (codeSubmitting) {
            [textfield_phone becomeFirstResponder];
            return;
        }
        
        [btn_resendCode setHidden:NO];
    
        [loader stopAnimating];
        
        authPhone = [textfield_phone text];
        
        label_registration.text = [NSString stringWithFormat:@"Код выслан на +7%@", authPhone];
        [img_blackLine setHidden:NO];
        
        [btn_continue setTitle:@"Подтвердить" forState:UIControlStateNormal];
        [btn_continue setHidden:NO];
        [btn_continue setEnabled:NO];

        [textfield_phone setText:@""];
        [textfield_phone setPlaceholder:@"Введите код из 6 цифр"];
        [textfield_phone setTextAlignment:NSTextAlignmentCenter];
        [textfield_phone setFrame:CGRectMake(40, 40, 240, 30)];
        [textfield_phone setHidden:NO];
        [textfield_phone becomeFirstResponder];
        
        codeSubmitting = YES;
        return;
    }
    
    // если это успешная регистрация пользователя
    if ([command isEqualToString:ECHO_REG_SUCCESS]) {
        
        [timerTimeout invalidate];
        [textfield_phone resignFirstResponder];
        
        authUserID = authPhone;
        authID = [textfield_phone text];
        [self performSelector:@selector(saveAuthInfoAndStart) withObject:nil afterDelay:0.5];
        return;
    }
    
    // если код подтверждения неверный
    if ([command isEqualToString:ECHO_ERROR_CODE]) {
        
        [timerTimeout invalidate];
        [self returnEnteringPhoneNumber];
        
        [self showMessage:nil
                    title:@"Информация"
                  message:@"Пожалуйста, введите код, отправленный Вам на телефон."
              firstButton:@"OK" secondButton:nil];
        return;
    }
    
    // если указанный или текущий номер телефона недействителен (бан)
    if ([command isEqualToString:ECHO_WRONG_PHONE] |
        [command isEqualToString:ECHO_ERROR_PHONE] |
        [command isEqualToString:ECHO_PLEASE_WAIT] |
        [command isEqualToString:ECHO_DAY_LIMIT] |
        [command isEqualToString:ECHO_ALREADY_CHANGED]) {
        
        [timerTimeout invalidate];
        [self returnEnteringPhoneNumber];
        
        NSString * messageText = @"Указанный Вами номер телефона заблокирован.";
        
        if ([command isEqualToString:ECHO_ERROR_PHONE])
            messageText = @"Ваш текущий номер телефона недействителен. Он либо использован для авторизации на другом устройстве, либо Вы заблокированы.";
        
        if ([command isEqualToString:ECHO_PLEASE_WAIT])
            messageText = @"Пожалуйста, подождите 1 минуту и повторите действие.";
        
        if ([command isEqualToString:ECHO_DAY_LIMIT])
            messageText = @"Достигнуто ограничение на кол-во запросов кода в течение дня.";
        
        if ([command isEqualToString:ECHO_ALREADY_CHANGED])
            messageText = @"Вы сегодня уже меняли номер телефона.";
        
        [self showMessage:nil
                    title:@"Информация"
                  message:messageText
              firstButton:@"OK" secondButton:nil];

        return;
    }
    
}

// вернуть первоначальное визуальное представление элементов на экране
- (void)returnEnteringPhoneNumber {
    
    [loader stopAnimating];
    
    // если это ввод телефона и при этом не смена телефона, то показываем User Agreement
    if (!codeSubmitting && !phoneChanging) {
        label_userAgreement.hidden = NO;
        btn_userAgreement.hidden   = NO;
    }
    
    // показать надпись "+7" если это ввод телефона
    if (!codeSubmitting) label_plus7.hidden   = NO;
    
    // показать ссылку "Код не пришел" есди это ввод кода
    if (codeSubmitting) btn_resendCode.hidden = NO;

    btn_continue.hidden  = NO;
    img_blackLine.hidden = NO;
    
    [textfield_phone setHidden:NO];
}

// запускаем таймер таймаута
- (void)startTimeout:(int)sec {
    
    timerTimeout = [NSTimer scheduledTimerWithTimeInterval:sec
                                                    target:self
                                                  selector:@selector(taskTimeout)
                                                  userInfo:nil
                                                   repeats:NO];
}

// сработал таймаут
- (void)taskTimeout {
    
    [timerTimeout invalidate];

    // если таймаут сработал при регистрации соискателя
    if ([timeoutTask isEqualToString:TASK_REG_TENANT]) {
        [self returnModeButtons:YES];
    } else {
        // если при регистрации риелтора
        [self returnEnteringPhoneNumber];
    }
    
    [self showMessage:nil
                title:@"Проблема с Интернетом"
              message:@"Пожалуйста, повторите попытку."
          firstButton:@"OK" secondButton:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkServerResult) name:
     @"welcomeResults" object:nil];
    
    [ArendaSingleton arenda];
    
    [[ArendaSingleton arenda] roundCorners:btn_next     radius:20];
    [[ArendaSingleton arenda] roundCorners:btn_continue radius:20];
    [[ArendaSingleton arenda] roundCorners:btn_user     radius:20];
    [[ArendaSingleton arenda] roundCorners:btn_realtor  radius:20];
    
    textfield_phone.delegate = self;
    
    [textfield_phone addTarget:self
                        action:@selector(textFieldDidChange:)
              forControlEvents:UIControlEventEditingChanged];
    
    welcomeStep = [[ArendaSingleton arenda] loadIntFromUserDefaults:@"welcomeStep"];
    
    // если окно приветствия открыто из окна настроек приложения
    if (welcomeStep != 0) {
        
        selectedMode = MODE_REALTOR;
        
        // показываем крестик X для закрытия окна и скрываем кнопку "Далее"
        btn_close.hidden = NO;
        btn_next.hidden  = YES;
        
        if ([[ArendaSingleton arenda] getMyUserID].length == TEXT_FIELD_PHONE) {
            
            // меняем процесс регистрации на смену номера телефона
            phoneChanging = YES;
            
            // скрываем надпись и ссылку User Agreement
            label_userAgreement.hidden = YES;
            btn_userAgreement.hidden   = YES;
            
            // меняем заголовок "Регистрация" на "Смена номера телефона"
            label_registration.text = @"Сменить номер телефона";
            
            [textfield_phone setPlaceholder:[NSString stringWithFormat:
                                             @"Текущий +7%@",
                                            [[ArendaSingleton arenda] getMyUserID]]];
        }
    }
    
    [self updateStepInformation];
    
    // если приложение запущено на iPhone 4
    if ([[ArendaSingleton arenda] getScreenSizes].height < 500) {
        
        label_title.center          = CGPointMake(label_title.center.x,
                                                  label_title.center.y - 35);
        
        label_description.center    = CGPointMake(label_description.center.x,
                                                  label_description.center.y - 40);
        
        img_stepIcon.center         = CGPointMake(img_stepIcon.center.x,
                                                  img_stepIcon.center.y - 40);
        
        btn_next.center             = CGPointMake(btn_next.center.x,
                                                  btn_next.center.y + 15);
        
        btn_user.center             = CGPointMake(btn_user.center.x,
                                                  btn_user.center.y + 15);
        
        btn_realtor.center          = CGPointMake(btn_realtor.center.x,
                                                  btn_realtor.center.y + 15);
        
        // для iPhone 4 необходимо приподнять поле ввода номера телефона
        // так как надпись и ссылка на User Agreement скрывается клавиатурой
        body_registration.center = CGPointMake(body_registration.center.x,
                                               body_registration.center.y - (!phoneChanging ? 30 : 10));
        
        if (phoneChanging)
            btn_resendCode.center = CGPointMake(btn_resendCode.center.x,
                                                btn_resendCode.center.y - 8);
    }
}

// событие при показе данного окна
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    
    // сообщаем синглтону, что ответы от сервера нужно присылать в наше окно
    [ArendaSingleton arenda].currentWindow = @"welcomeResults";
}

// показать или скрыть кнопки выбора режима и loading (при регистрации соискателя)
- (void)returnModeButtons:(BOOL)show {
    
    btn_realtor.hidden = !show;
    btn_user.hidden = !show;
    
    if (show) {
        [loader_userRegistration stopAnimating];
    } else {
        [loader_userRegistration startAnimating];
    }
}

- (void)updateStepInformation {
    
    label_title.hidden          = YES;
    label_description.hidden    = YES;
    img_stepIcon.hidden         = YES;
    
    btn_user.hidden             = YES;
    btn_realtor.hidden          = YES;
    
    if (welcomeStep == 0) {
        label_title.text        = @"Мы произвели революцию в сфере аренды жилья";
        label_description.text  = @"Вам больше не нужно искать объявления на популярных интернет-площадках и в социальных сетях.";
        
        [img_stepIcon setImage:[UIImage imageNamed:@"icon_welcome_step_1.png"]];
    }
    
    if (welcomeStep == 1) {
        label_title.text        = @"Вы просто кидаете клич\nна весь город";
        label_description.text  = @"И сотни риелторов в Вашем городе моментально получат уведомление об этом.";
        
        [img_stepIcon setImage:[UIImage imageNamed:@"icon_welcome_step_2.png"]];
    }
    
    if (welcomeStep == 2) {
        label_title.text        = @"Если Вы сейчас в\nпоисках жилья";
        label_description.text  = @"Вы не только быстро и удобно, но и выгодно найдете жилье, так как сможете торговаться с каждым риелтором.";
        
        [img_stepIcon setImage:[UIImage imageNamed:@"icon_welcome_step_3.png"]];
    }
    
    if (welcomeStep == 3) {
        label_title.text       = @"Если Вы работаете риелтором или только планируете";
        label_description.text = @"Вы моментально узнаете о каждом новом соискателе жилья в Вашем городе и сможете зарабатывать еще больше.";
        
        [img_stepIcon setImage:[UIImage imageNamed:@"icon_welcome_step_4.png"]];
    }
    
    [self performSelector:@selector(showTitleAndDescrition) withObject:nil afterDelay:0.2];
}

- (void)showTitleAndDescrition {
    
    // если нужно показать окно авторизации по телефону (4 шаг)
    if (welcomeStep == 4) {
        
        [[ArendaSingleton arenda] animateView:body_registration time:0.2];
        body_registration.hidden = NO;

        [textfield_phone becomeFirstResponder];
        return;
    }
    
    [[ArendaSingleton arenda] animateView:label_title       time:0.2];
    [[ArendaSingleton arenda] animateView:label_description time:0.2];
    [[ArendaSingleton arenda] animateView:img_stepIcon      time:0.2];
    
    label_title.hidden          = NO;
    label_description.hidden    = NO;
    img_stepIcon.hidden         = NO;
    
    // если пользователь нажал один раз "Далее", то делаем запрос на местоположение
    if (welcomeStep == 1) [[ArendaSingleton arenda] activateLocation];
}

// 1. Пользователь нажимает "Далее" 3 раза
- (IBAction)btn_next:(id)sender {
    
    welcomeStep++;
    [self updateStepInformation];
    
    // при этом если на экране 3 шаг, то нужно показать кнопки "Я риелтор" и "Я ищу жилье"
    if (welcomeStep == 3) {
        btn_next.hidden = YES;
        [self performSelector:@selector(showModeButtons) withObject:nil afterDelay:0.5];
    }
}

// 2. Затем показываются кнопки "Я риелтор" и "Я ищу жилье"
- (void)showModeButtons {
    
    [[ArendaSingleton arenda] animateView:btn_user time:0.2];
    [[ArendaSingleton arenda] animateView:btn_realtor time:0.2];
    
    btn_user.hidden     = NO;
    btn_realtor.hidden  = NO;
}

// 3. Пользователь выбрал режим риелтора
- (IBAction)btn_realtor:(id)sender {
    
    selectedMode = MODE_REALTOR;
    welcomeStep  = 4;
    
    [self updateStepInformation];
}

// 4. Пользователь выбрал режим соискателя
- (IBAction)btn_user:(id)sender {
    
    // проверяем наличие интернета
    if (![[ArendaSingleton arenda] checkInternet]) {
        [self showMessage:nil
                    title:@"Информация"
                  message:@"В данный момент отсутствует соединение с Интернетом."
              firstButton:@"OK" secondButton:nil];
        return;
    }
    
    selectedMode = MODE_TENANT;
    welcomeStep  = 4;
    
    [self returnModeButtons:NO];
    
    // формируем userID и codeID
    NSInteger randPart  = [[ArendaSingleton arenda] random:1000000 maxNum:9999999];
    NSInteger randPart2 = [[ArendaSingleton arenda] random:1000000 maxNum:9999999];
    NSInteger randPart3 = [[ArendaSingleton arenda] random:1000000 maxNum:9999999];
    
    authUserID  = [NSString stringWithFormat:@"%d%d%d", randPart, randPart2, randPart3];
    authID      = [NSString stringWithFormat:@"%d", [[ArendaSingleton arenda] random:100000 maxNum:999999]];
    
    NSString * params = [NSString stringWithFormat:
                         @"action=newuser&task=newtenant&userid=%@&id=%@",
                         authUserID,
                         authID];
 
    [self startTimeout:10];
    timeoutTask = TASK_REG_TENANT;
    [[ArendaSingleton arenda] POST:params phpFile:PHP_FILE_REG];
}

// пользователь нажимает на кнопку "Продолжить/Подтвердить"
- (IBAction)btn_continue:(id)sender {
    
    // +++++++++++++++ если в данный момент подтверждение кода +++++++++++++++
    if (codeSubmitting) {
        
        NSString * regCode = [textfield_phone text];
        
        // 1. проверяем наличие интернета
        if (![[ArendaSingleton arenda] checkInternet]) {
            [self showMessage:nil
                        title:@"Информация"
                      message:@"В данный момент отсутствует соединение с Интернетом."
                  firstButton:@"OK" secondButton:nil];
            return;
        }
        
        // 2. Проверяем длину кода на 6 цифр
        if (regCode.length != TEXT_FIELD_CODE) {
            [self showMessage:nil
                        title:@"Информация"
                      message:@"Код должен состоять из 6 цифр."
                  firstButton:@"OK" secondButton:nil];
            return;
        }
        
        // 3. Если все хорошо, то скрываем все элементы и показываем loading
        textfield_phone.hidden  = YES;
        img_blackLine.hidden    = YES;
        btn_continue.hidden     = YES;
        btn_resendCode.hidden   = YES;
        
        [[ArendaSingleton arenda] animateView:loader time:0.2];
        [loader startAnimating];
        
        NSString * params;
        
        // если это первичная регистрация, то отправляем связку номер-код-режим
        if (!phoneChanging) {
            params = [NSString stringWithFormat:@"action=newuser&task=checkcode&userid=%@&phone=%@&id=%@&mode=%d", [[ArendaSingleton arenda] getMyUserID], authPhone, textfield_phone.text, selectedMode];
        } else {
            
            // если это смена номера, то отправляем связку новый номер - код - старый номер
            params = [NSString stringWithFormat:@"action=newuser&task=checkcode&phone=%@&id=%@&oldphone=%@", authPhone, textfield_phone.text, [[ArendaSingleton arenda] getMyUserID]];
        }
        
        [self startTimeout:10];
        timeoutTask = TASK_CHECK_CODE;
        [[ArendaSingleton arenda] POST:params phpFile:PHP_FILE_REG];
        
        return;
    }
    
    // +++++++++++++++ если человек еще только вводит номер телефона +++++++++++++++
    NSString * regNumber = [textfield_phone text];
    
    // 1. проверяем наличие интернета
    if (![[ArendaSingleton arenda] checkInternet]) {
        [self showMessage:nil
                    title:@"Информация"
                  message:@"В данный момент отсутствует соединение с Интернетом."
              firstButton:@"OK" secondButton:nil];
        return;
    }
    
    // 2. Проверяем длину номера телефона на 10 цифр
    if (regNumber.length != TEXT_FIELD_PHONE) {
        [self showMessage:nil
                    title:@"Информация"
                  message:@"Номер телефона должен состоять из 10 цифр."
              firstButton:@"OK" secondButton:nil];
        return;
    }
    
    // 3. [Проверку убрали] Проверяем, в правильном ли формате указан номер
//    if (![[regNumber substringToIndex:1] isEqualToString:@"9"]) {
//        [self showMessage:nil
//                    title:@"Информация"
//                  message:@"Номер телефона должен начинаться с цифры 9."
//              firstButton:@"OK" secondButton:nil];
//        return;
//    }

    // 4. Проверка на совпадение нового телефона с текущим (если это смена)
    if (phoneChanging && [[[ArendaSingleton arenda] getMyUserID] isEqualToString:regNumber]) {
        [self showMessage:nil
                    title:@"Информация"
                  message:@"Указанный Вами номер телефона используется в данный момент."
              firstButton:@"OK" secondButton:nil];
        return;
    }
    
    NSString * changeComment = phoneChanging ? @"\n\nОбращаем Ваше внимание, что номер можно менять не чаще 1 раза в сутки." : @"";
    
    // 5. Если проверки прошли, просим пользователя проверить номер и нажать ДА
    [self showMessage:ALERT_TASK_CHECK_NUMBER
                title:[NSString stringWithFormat:@"+7%@", [textfield_phone text]]
              message:[NSString stringWithFormat:@"Вы правильно указали номер телефона? На него будет выслан код для подтверждения.%@", changeComment]
          firstButton:@"Да" secondButton:@"Нет"];
}

// пользователь нажимает на кнопку "Код не пришел?"
- (IBAction)btn_resendCode:(id)sender {
    
    // проверяем наличие интернета
    if (![[ArendaSingleton arenda] checkInternet]) {
        [self showMessage:nil
                    title:@"Информация"
                  message:@"В данный момент отсутствует соединение с Интернетом."
              firstButton:@"OK" secondButton:nil];
        return;
    }
    
    [self showMessage:ALERT_TASK_RESEND_CODE
                title:[NSString stringWithFormat:@"+7%@", authPhone]
              message:@"Выслать код повторно?"
          firstButton:@"Да" secondButton:@"Нет"];
}

// метод выполняется после регистрации, он сохраняет данные о регистрации
- (void)saveAuthInfoAndStart {

    // сохраняем наш userID, номер и код (в будущем используется как ID)
    [[ArendaSingleton arenda] saveStringToUserDefaults:authUserID   forKey:@"authUserID"];
    [[ArendaSingleton arenda] saveStringToUserDefaults:authPhone    forKey:@"authPhone"];
    [[ArendaSingleton arenda] saveStringToUserDefaults:authID       forKey:@"authID"];
    
    // сохраняем текущий шаг приветствия и выбранный режим пользователя
    [[ArendaSingleton arenda] saveIntToUserDefaults:welcomeStep     forKey:@"welcomeStep"];
    [[ArendaSingleton arenda] saveIntToUserDefaults:selectedMode    forKey:@"currentMode"];
    
    // если это была первичная регистрация, то заменяем окно на нужное
    if (!phoneChanging) {
        [(AppDelegate *)[[UIApplication sharedApplication] delegate] changeViewController:selectedMode];
        return;
    }
    
    // если это была смена телефона, то просто закрываем окно
    [self dismissViewControllerAnimated:NO completion:nil];
}

// Пользователь нажал на ссылку "Пользовательское соглашение"
- (IBAction)btn_userAgreement:(id)sender {
    [[ArendaSingleton arenda] openURL:URL_AGREEMENT];
}

// Пользователь нажимает на X, чтобы закрыть окно (если это смена номера телефона)
- (IBAction)btn_close:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

// при закрытии окна
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];

    // на всякий случай выключаем таймер, чтобы не произошел крэш
    [timerTimeout invalidate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
