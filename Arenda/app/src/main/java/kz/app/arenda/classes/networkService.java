package kz.app.arenda.classes;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import kz.app.arenda.R;

import kz.app.arenda.singletonArenda;

// КЛАСС ДЛЯ РАБОТЫ С СЕТЬЮ

public class networkService {

    public String TASK_REG_TENANT = "Reg Tenant";
    public String TASK_NEW_CODE = "New Code";
    public String TASK_CHECK_CODE = "Check Code";
    public String TASK_CONTACT_US = "Contact Us";
    public String TASK_SUBMIT_SEARCH = "Submit Search";
    public String TASK_CANCEL_SEARCH = "Cancel Search";
    public String TASK_SEND_PUSHES = "Send Pushes";
    public String TASK_UPDATE_USER_INFO = "Update UserInfo";
    public String TASK_UPDATE_DEVICE_TOKEN = "Update Device Token";
    public String TASK_GET_USERS = "Get Users";
    public String TASK_VALIDATING_PURCHASE = "Validating Purchase";

    public String ECHO_TENANT_EXISTS = "tenantExists";
    public String ECHO_REG_TENANT_SUCCESS = "regTenantSuccess";
    public String ECHO_REG_SUCCESS = "regSuccess";
    public String ECHO_CODE_SENT = "codeSent";
    public String ECHO_ERROR_CODE = "errorCode";
    public String ECHO_WRONG_PHONE = "wrongPhone";
    public String ECHO_ERROR_PHONE = "errorPhone";
    public String ECHO_ERROR_ACCOUNT = "errorAccount";
    public String ECHO_PLEASE_WAIT = "pleaseWait";
    public String ECHO_DAY_LIMIT = "dayLimit";
    public String ECHO_ALREADY_CHANGED = "alreadyChanged";
    public String ECHO_UPDATE_APP = "updateApp";
    public String ECHO_ERROR_COMMENT = "errorComment";
    public String ECHO_SUBMIT_SUCCESS = "submitSuccess";
    public String ECHO_CANCEL_SUCCESS = "cancelSuccess";
    public String ECHO_USERS = "users";
    public String ECHO_PAYMENT_ERROR = "paymentError";
    public String ECHO_PAYMENT_SUCCESS = "paymentSuccess";

    public String PHP_FILE_REG = "v2_reg.php";
    public String PHP_FILE_SETTINGS = "v2_settings.php";
    public String PHP_FILE_USER = "v2_user.php";
    public String PHP_FILE_REALTOR = "v2_realtor.php";
    public String PHP_FILE_PAYMENT = "v2_payment_android.php";
    public String PHP_FILE_CALLS = "v1_calls.php";

    public int TIMEOUT_DISABLED = 0;
    public int TIMEOUT_10_SECONDS = 10;
    public int TIMEOUT_12_SECONDS = 12;

    private String serverURL;
    private Listener mListener = null;
    private Context mContext = null;

    sharedSettings mSettings;

    public interface Listener {
        void onNetworkCallback(String requestTask, String resultString);
    }

    public networkService(Listener listener, Context context) {
        mListener = listener;
        mContext = context;
        serverURL = context.getString(R.string.Link_Website);
        mSettings = new sharedSettings(context);
    }

    // обновление Device Token пользователя на сервере
    public void updateDeviceToken(String tokenString) {

        // проверяем наличие интернета
        if (!singletonArenda.checkInternet(mContext)) return;

        String params = "action=settings" +
                "&task=updatetoken"+
                "&userid=" + mSettings.loadString(mSettings.SETTING_MY_USERID) +
                "&phone=" + mSettings.loadString(mSettings.SETTING_MY_NUMBER) +
                "&id=" + mSettings.loadString(mSettings.SETTING_MY_ID) +
                "&token=" + tokenString;

        postRequest(TASK_UPDATE_DEVICE_TOKEN, PHP_FILE_SETTINGS, params, TIMEOUT_DISABLED);
    }

    // обновление информации о пользователь на сервере
    public void updateUserInfo() {

        // проверяем наличие интернета
        if (!singletonArenda.checkInternet(mContext)) return;

        int currentMode = mSettings.loadInt(mSettings.SETTING_CURRENT_MODE);
        int notify = mSettings.loadInt(mSettings.SETTING_DISABLE_PUSH_NOTIFICATIONS) == 1 ? 0 : 1;

        // если сейчас режим соискателя, то push уведомления всегда включены
        notify = currentMode == 1 ? 1 : notify;

        final String params = "action=settings" +
                "&task=updateinfo" +
                "&userid=" + mSettings.loadString(mSettings.SETTING_MY_USERID) +
                "&phone=" + mSettings.loadString(mSettings.SETTING_MY_NUMBER) +
                "&id=" + mSettings.loadString(mSettings.SETTING_MY_ID) +
                "&stars=" + mSettings.loadInt(mSettings.SETTING_STARS_RATE) +
                "&mode=" + currentMode +
                "&notify=" + notify;


        new Handler().postDelayed(new Runnable() {
            public void run() {
                postRequest(TASK_UPDATE_USER_INFO, PHP_FILE_SETTINGS, params, TIMEOUT_DISABLED);
            }
        }, 1000);
    }

    // выполнить POST запрос
    public void postRequest(final String requestTask, String phpFile, String params, int timeoutSeconds) {
        new post(requestTask, phpFile, timeoutSeconds).execute(addSystemInfo(params));
    }

    // добавляет к запросу дополнительную информацию
    private String addSystemInfo(String params) {
        return params +
                "&app=" + singletonArenda.getAppVersion() +
                "&os=Android" +
                "&version=" + singletonArenda.getAndroidVersion() +
                "&device=" + singletonArenda.getDeviceModel() +
                "&latitude=" + mSettings.loadCoordinate(mSettings.SETTING_COORDS_LATITUDE) +
                "&longitude=" + mSettings.loadCoordinate(mSettings.SETTING_COORDS_LONGITUDE);
    }

    class post extends AsyncTask<String, String, String> {

        private boolean isProgress = false;

        private String requestTask;
        private String phpFile;

        post(String _requestTask, String _phpFile, int _timeoutSeconds) {
            phpFile = _phpFile;
            requestTask = _requestTask;

            if (_timeoutSeconds > 0) {
                new Handler().postDelayed(new Runnable() {
                    public void run() {

                        if (isProgress) {
                            isProgress = false;
                            mListener.onNetworkCallback(requestTask, "TIMEOUT");
                        }

                    }
                }, _timeoutSeconds * 1000);
            }
        }

        @Override
        protected void onPreExecute() {
            isProgress = true;
        }

        @Override
        protected String doInBackground(String... uri) {

            String resultString = "";
            HttpURLConnection con = null;

            try {

                URL url = new URL(serverURL + "/" + phpFile);

                byte[] postDataBytes = uri[0].getBytes("UTF-8");

                con = (HttpURLConnection)url.openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

                con.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                con.setRequestProperty("Content-Language", "en-US");

                con.setUseCaches(false);
                con.setDoInput(true);
                con.setDoOutput(true);

                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.write(postDataBytes);
                wr.flush();
                wr.close();

                InputStream is = con.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuilder response = new StringBuilder();

                while((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }

                rd.close();
                resultString = response.toString();
            }

            catch (Exception e) {}
            finally {
                if (con != null) con.disconnect();
            }

            return resultString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.trim().equals("")) return;

            isProgress = false;
            mListener.onNetworkCallback(requestTask, result);
        }
    }

}