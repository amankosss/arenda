package kz.app.arenda.classes;

// КЛАСС ДЛЯ РАБОТЫ С ГЕОЛОКАЦИЕЙ

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class geolocationService implements GoogleApiClient.ConnectionCallbacks {

    sharedSettings mSettings;

    private Listener mListener = null;
    private GoogleApiClient mGoogleApiClient;
    private Context mContext = null;

    public interface Listener {
        void onGetCityAndStreetName(String cityStreet);
    }

    public geolocationService(Listener listener, Context context) {
        mListener = listener;
        mContext = context;
        mSettings = new sharedSettings(context);
        buildGoogleApiClient();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
    }

    public void getMyLocation() {

        if (mGoogleApiClient == null) {
            buildGoogleApiClient();
            mGoogleApiClient.connect();
            return;
        }

        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
            return;
        }

        updateLocation();
    }

    public void updateLocation() {

        mSettings.saveCoordinate(mSettings.SETTING_COORDS_LATITUDE, 0);
        mSettings.saveCoordinate(mSettings.SETTING_COORDS_LONGITUDE, 0);

        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation == null) return;

        mSettings.saveCoordinate(mSettings.SETTING_COORDS_LATITUDE, Float.valueOf(String.valueOf(mLastLocation.getLatitude())));
        mSettings.saveCoordinate(mSettings.SETTING_COORDS_LONGITUDE, Float.valueOf(String.valueOf(mLastLocation.getLongitude())));
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        updateLocation();
    }

    @Override
    public void onConnectionSuspended(int connectionHint) {

    }

    public void getCityAndStreet(double lat, double lng, boolean onlyCity) {
        new getCityAndStreet(lat, lng, onlyCity).execute();
    }

    class getCityAndStreet extends AsyncTask<String, String, String> {

        double lat;
        double lng;

        boolean onlyCity;

        getCityAndStreet(double _lat, double _lng, boolean _onlyCity) {
            lat = _lat;
            lng = _lng;
            onlyCity = _onlyCity;
        }

        @Override
        protected String doInBackground(String... params) {

            String returnStr = "underfined";

            Geocoder geoCoder = new Geocoder(mContext, Locale.getDefault());
            List<Address> list = null;
            try {
                list = geoCoder.getFromLocation(lat, lng, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (list == null || list.size() == 0) return returnStr;

            // если нам нужен только город
            if (onlyCity) {

                if (list.get(0).getLocality() != null) returnStr = list.get(0).getLocality();
                return returnStr;
            }

            // если нам нужен и город и улица
            if (list.get(0).getLocality() != null && list.get(0).getThoroughfare() != null) {

                returnStr = list.get(0).getLocality() + ", " + list.get(0).getThoroughfare();
                returnStr = returnStr.replace("просп.", "проспект").replace("пер.", "переулок").replace("бул.", "бульвар").replace("ул.", "улица").replace("ш.", "шоссе").replace("пр.", "проезд").replace("пл.", "площадь").replace("наб.", "набережная").replace("пос.", "поселок").replace("Unnamed Rd", "неизвестная улица");
            }

            return returnStr;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            mListener.onGetCityAndStreetName(result);
        }
    }

}