package kz.app.arenda;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import kz.app.arenda.classes.messageDialog;
import kz.app.arenda.classes.networkService;
import kz.app.arenda.classes.sharedSettings;

public class activitySettings extends Activity implements messageDialog.Listener, networkService.Listener {

    // подключенные классы для работы с диалоговыми сообщениями, настройками и сетью
    messageDialog msgDialog;
    sharedSettings mSettings;
    networkService mNetwork;

    // надписи, картинки, кнопки и т.п. элементы, которые используются в данном окне (активити)
    TextView view_textWebsite;

    Button view_buttonChangeMode;
    Button view_buttonChangePhone;
    ImageButton view_buttonNotify;
    ImageButton view_button1Star;
    ImageButton view_button2Star;
    ImageButton view_button3Star;
    ImageButton view_button4Star;
    ImageButton view_button5Star;

    // переменные данной активити
    // ...

    // обработчик физической кнопки НАЗАД
    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.splash, R.anim.alpha);
    }

    // событие при показе данного окна
    @Override
    protected void onResume() {
        super.onResume();
        mSettings.saveBoolean(mSettings.IN_BACKGROUND, false);
    }

    // событие при скрытии данного окна
    @Override
    protected void onPause() {
        super.onPause();
        mSettings.saveBoolean(mSettings.IN_BACKGROUND, true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // инициализация классов для работы с диалоговыми сообщениями, настройками и сетью
        msgDialog = new messageDialog(this, activitySettings.this);
        mSettings = new sharedSettings(this);
        mNetwork = new networkService(this, this);

        view_textWebsite = (TextView)findViewById(R.id.label_website);

        // пользователь нажал на вебсайт
        view_textWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                singletonArenda.browseURL(getString(R.string.Link_Website), getApplicationContext());
            }
        });

        view_buttonChangeMode = (Button)findViewById(R.id.btn_changeMode);
        view_buttonChangePhone = (Button)findViewById(R.id.btn_changePhone);

        view_buttonNotify = (ImageButton)findViewById(R.id.btn_notify);

        view_button1Star = (ImageButton)findViewById(R.id.btn_1s_star);
        view_button2Star = (ImageButton)findViewById(R.id.btn_2s_star);
        view_button3Star = (ImageButton)findViewById(R.id.btn_3s_star);
        view_button4Star = (ImageButton)findViewById(R.id.btn_4s_star);
        view_button5Star = (ImageButton)findViewById(R.id.btn_5s_star);

        // окрашиваем рейтинг в нужное кол-во звездочек
        updateStars();

        // если мы в режиме риелтора, то показать кнопку смены номера и выключения Push уведомлений
        int currentMode = mSettings.loadInt(mSettings.SETTING_CURRENT_MODE);
        view_buttonChangePhone.setVisibility(currentMode == 1 ? View.GONE : View.VISIBLE);
        view_buttonNotify.setVisibility(currentMode == 1 ? View.GONE : View.VISIBLE);
        view_buttonNotify.setBackgroundResource(mSettings.loadInt(mSettings.SETTING_DISABLE_PUSH_NOTIFICATIONS) == 0 ? R.drawable.btn_notify : R.drawable.btn_notify_hover);

        // в зависимости от выбранного режима, установить нужный текст для кнопки смены режима
        view_buttonChangeMode.setText(getString(currentMode == 1 ? R.string.Settings_Button_RealtorMode : R.string.Settings_Button_UserMode));
    }

    // прослушиваем нажатие кнопок в диалоговых сообщениях
    @Override
    public void onMessageDialogButtonClicked(String dialogTask, boolean isPositive) {

        if (!isPositive) return;

        // если это смена режима
        if (dialogTask.trim().equals(msgDialog.TASK_CHANGE_MODE)) {

            // меняем значение выбранного режима
            int newMode = mSettings.loadInt(mSettings.SETTING_CURRENT_MODE) == 1 ? 2 : 1;
            mSettings.saveInt(mSettings.SETTING_CURRENT_MODE, newMode);

            // перезапускаем приложение, очищая весь стек
            startActivity(new Intent(activitySettings.this, activitySplashscreen.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            overridePendingTransition(R.anim.splash, R.anim.alpha);

            return;
        }

        // если это выключение Push уведомлений (ставим DISABLED = 1)
        if (dialogTask.trim().equals(msgDialog.TASK_DISABLE_PUSH_NOTIFICATIONS)) {

            view_buttonNotify.setBackgroundResource(R.drawable.btn_notify_hover);
            mSettings.saveInt(mSettings.SETTING_DISABLE_PUSH_NOTIFICATIONS, 1);
        }
    }

    // прослушиваем ответы от сервера
    @Override
    public void onNetworkCallback(String requestTask, String resultString) {
        // в данной актвити мы не читаем ответы.
    }

    // закрытие окна настроек
    public void tap_close(View v) {
        onBackPressed();
    }

    // пользователь включает/отключает Push уведомления
    public void tap_notify(View v) {

        // проверяем наличие интернета
        if (!checkInternet()) return;

        // если Push уведомления в данный момент включены (DISABLED = 0)
        if (mSettings.loadInt(mSettings.SETTING_DISABLE_PUSH_NOTIFICATIONS) == 0) {

            msgDialog.show(msgDialog.TASK_DISABLE_PUSH_NOTIFICATIONS, getString(R.string.Settings_Text_DisablePushTitle),
                    getString(R.string.Settings_Text_DisablePushDescription),
                    getString(R.string.Button_YES), getString(R.string.Button_NO), true);
            return;
        }

        // если же выключены (DISABLED = 1)
        view_buttonNotify.setBackgroundResource(R.drawable.btn_notify);
        mSettings.saveInt(mSettings.SETTING_DISABLE_PUSH_NOTIFICATIONS, 0);
    }

    // пользователь хочет сменить режим (соискатель или риелтор)
    public void tap_changeMode(View v) {

        // проверяем наличие интернета
        if (!checkInternet()) return;

        // проверяем, если ранее не было регистрации в качестве риелтора, то открываем регистрацию
        if (mSettings.loadString(mSettings.SETTING_MY_USERID).trim().length() != 10) {
            startActivity(new Intent(activitySettings.this, activityWelcome.class));
            overridePendingTransition(R.anim.splash, R.anim.alpha);
            return;
        }

        // если регистрация была, то просто переключаем режим
        msgDialog.show(msgDialog.TASK_CHANGE_MODE, getString(R.string.Settings_Text_ChangeModeTitle),
                null,
                getString(R.string.Button_YES), getString(R.string.Button_NO), true);
    }

    // пользователь хочет сменить номер телефона
    public void tap_changePhone(View v) {

        // проверяем наличие интернета
        if (!checkInternet()) return;

        // показываем окно смены (4 шаг окна Welcome)
        startActivity(new Intent(activitySettings.this, activityWelcome.class));
        overridePendingTransition(R.anim.splash, R.anim.alpha);
    }

    // пользователь поставил оценку 1
    public void tap_button1Star(View v) {
        rateApp(1);
    }

    // пользователь поставил оценку 2
    public void tap_button2Star(View v) {
        rateApp(2);
    }

    // пользователь поставил оценку 3
    public void tap_button3Star(View v) {
        rateApp(3);
    }

    // пользователь поставил оценку 4
    public void tap_button4Star(View v) {
        rateApp(4);
    }

    // пользователь поставил оценку 5
    public void tap_button5Star(View v) {
        rateApp(5);
    }

    // пользователь хочет оценить приложение
    private void rateApp(int stars) {

        // если пользователь ставит туже оценку, что и раньше
        if (stars == mSettings.loadInt(mSettings.SETTING_STARS_RATE)) return;

        // проверяем наличие интернета
        if (!checkInternet()) return;

        // сохраняем оценку и обновляем звездочки на экране
        mSettings.saveInt(mSettings.SETTING_STARS_RATE, stars);
        updateStars();

        // если пользователь поставил 4 или 5, то переходим в Google Play
        if (stars > 3) {
            singletonArenda.browseURL(getString(R.string.Link_GooglePlay), getApplicationContext());
            return;
        }

        // иначе показываем окно обратной связи, чтобы узнать почему такая оценка?
        openContactUsDialog(getString(R.string.Settings_Button_ThanksForRate));
    }

    // "окрашиваем" нужное кол-во звездочек, взависимости от поставленной оценки
    private void updateStars() {

        int stars = mSettings.loadInt(mSettings.SETTING_STARS_RATE);

        view_button1Star.setBackgroundResource(stars > 0 ? R.drawable.btn_rate_hover : R.drawable.btn_rate);
        view_button2Star.setBackgroundResource(stars > 1 ? R.drawable.btn_rate_hover : R.drawable.btn_rate);
        view_button3Star.setBackgroundResource(stars > 2 ? R.drawable.btn_rate_hover : R.drawable.btn_rate);
        view_button4Star.setBackgroundResource(stars > 3 ? R.drawable.btn_rate_hover : R.drawable.btn_rate);
        view_button5Star.setBackgroundResource(stars > 4 ? R.drawable.btn_rate_hover : R.drawable.btn_rate);
    }

    // пользователь хочет связаться с нами
    public void tap_contactUs(View v) {

        // проверяем наличие интернета
        if (!checkInternet()) return;

        openContactUsDialog(getString(R.string.Settings_Button_ContactUs));
    }

    // окрыть диалог обратной связи
    private void openContactUsDialog(String title) {

        final EditText feedbackEdit = new EditText(this);
        feedbackEdit.setFilters(new InputFilter[]{new InputFilter.LengthFilter(255)});
        feedbackEdit.setSingleLine(true);

        final AlertDialog.Builder builder = new AlertDialog.Builder(activitySettings.this);

        builder.setTitle(title)
                .setMessage(getString(R.string.Settings_Text_ContactUsDescription))
                .setIcon(R.mipmap.ic_launcher)
                .setView(feedbackEdit);

        builder.setNegativeButton(getString(R.string.Button_Cancel), null);
        builder.setPositiveButton(getString(R.string.Button_Send), null);

        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(final DialogInterface dialog) {

                Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        // 1. получаем текст сообщения обратной связи и заменяем запрещенные символы
                        String message = feedbackEdit.getText().toString();
                        message = message.replace("'", "").replace("\\", "").replace("+", "").replace("*", "").replace("|", "");

                        // 2. проверка на пустой текст
                        if (message.trim().equals("")) return;

                        // 3. проверяем наличие интернета
                        if (!checkInternet()) return;

                        // 4. если все хорошо, то формируем тело с параметрами запроса
                        String params = "action=settings" +
                                "&task=contactus"+
                                "&userid=" + mSettings.loadString(mSettings.SETTING_MY_USERID) +
                                "&phone=" + mSettings.loadString(mSettings.SETTING_MY_NUMBER) +
                                "&id=" + mSettings.loadString(mSettings.SETTING_MY_ID) +
                                "&stars=" + mSettings.loadInt(mSettings.SETTING_STARS_RATE) +
                                "&text=" + message +
                                "&mode=" + mSettings.loadInt(mSettings.SETTING_CURRENT_MODE);

                        // 5. отправляем запрос на сервер
                        mNetwork.postRequest(mNetwork.TASK_CONTACT_US, mNetwork.PHP_FILE_SETTINGS, params, mNetwork.TIMEOUT_DISABLED);
                        dialog.dismiss();
                    }
                });
            }
        });

        alertDialog.show();
    }

    // пользователь нажимает "Мы в Facebook"
    public void tap_facebook(View v) {
        singletonArenda.browseURL(getString(R.string.Link_Facebook), getApplicationContext());
    }

    // пользователь нажимает "Мы в Twitter"
    public void tap_twitter(View v) {
        singletonArenda.browseURL(getString(R.string.Link_Twitter), getApplicationContext());
    }

    // пользователь нажимает "Мы ВКонтакте"
    public void tap_vkontakte(View v) {
        singletonArenda.browseURL(getString(R.string.Link_Vkontakte), getApplicationContext());
    }

    // проверяем наличие интернета
    private boolean checkInternet() {

        if (singletonArenda.checkInternet(getApplicationContext())) return true;

        msgDialog.show(null, getString(R.string.Title_Information),
                       getString(R.string.Message_NoInternetConecttion),
                       getString(R.string.Button_OK), null, true);
        return false;
    }

}