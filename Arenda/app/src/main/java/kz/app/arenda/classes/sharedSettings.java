package kz.app.arenda.classes;

import android.content.Context;
import android.content.SharedPreferences;

// КЛАСС ДЛЯ РАБОТЫ С НАСТРОЙКАМИ

public class sharedSettings {

    public String IN_BACKGROUND = "background";

    public String SETTING_MY_USERID = "authUserID";
    public String SETTING_MY_NUMBER = "authPhone";
    public String SETTING_MY_ID = "authID";
    public String SETTING_CURRENT_MODE = "currentMode";
    public String SETTING_WELCOME_STEP = "welcomeStep";
    public String SETTING_STARS_RATE = "rateApp";
    public String SETTING_DISABLE_PUSH_NOTIFICATIONS = "disablePushNotifications";
    public String SETTING_ALREADY_SEARCHED = "alreadySearched";
    public String SETTING_SELECTED_REGION = "selectedRegion";
    public String SETTING_COORDS_LATITUDE = "savedLatitude";
    public String SETTING_COORDS_LONGITUDE = "savedLongitude";
    public String SETTING_USERS_ACTION_TIME = "usersActionTime";
    public String SETTING_REALTOR_NEWS = "realtorNews";
    public String SETTING_REALTOR_USERS_COLORS_HINT = "realtorUsersColorsHint";
    public String SETTING_CALLS_IS_DISABLED = "callsIsDisabled";
    public String SETTING_REALTOR_TEST_PERIOD_EXPIRED = "realtorTestPeriodExpired";
    public String SETTING_SHOP_OPENS = "shopOpens";

    private SharedPreferences shared;

    public sharedSettings(Context context) {
        shared = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
    }

    public float loadCoordinate(String keyName) {
        return shared.getFloat(keyName, 0);
    }

    public void saveCoordinate(String keyName, float keyValue) {
        shared.edit().putFloat(keyName, keyValue).apply();
    }

    public String loadString(String keyName) {
        return shared.getString(keyName, "");
    }

    public void saveString(String keyName, String keyValue) {
        shared.edit().putString(keyName, keyValue).apply();
    }

    public int loadInt(String keyName) {
        return shared.getInt(keyName, 0);
    }

    public void saveInt(String keyName, int keyValue) {
        shared.edit().putInt(keyName, keyValue).apply();
    }

    public boolean loadBoolean(String keyName) {
        return shared.getBoolean(keyName, false);
    }

    public void saveBoolean(String keyName, boolean keyValue) {
        shared.edit().putBoolean(keyName, keyValue).apply();
    }
}