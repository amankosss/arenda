package kz.app.arenda;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

// ЗДЕСЬ НАСТРОЙКИ ОТОБРАЖЕНИЯ ЯЧЕЙКИ СОИСКАТЕЛЯ В ОКНЕ РИЕЛТОРА (НИЧЕГО НЕ ТРОГАТЬ)

public class activityRealtor_User_Adapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<activityRealtor_User_Item> mUsersArray;

    activityRealtor_User_Adapter (Context context, ArrayList <activityRealtor_User_Item> usersArray) {
        ctx = context;
        mUsersArray = usersArray;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mUsersArray.size();
    }

    @Override
    public Object getItem(int position) {
        return mUsersArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) view = lInflater.inflate(R.layout.model_user, parent, false);

        activityRealtor_User_Item userItem = getUser(position);

        // показать или скрыть надпись "новый"
        TextView textNew = ((TextView) view.findViewById(R.id.userItem_new));
        textNew.setVisibility(userItem.newUser == 1 ? View.VISIBLE : View.GONE);

        boolean longRentTime = userItem.rentTime.trim().equals("MONTH");

        // заголовок: сегодня в 21:00
        TextView textSearchTime = ((TextView) view.findViewById(R.id.userItem_searchTime));
        textSearchTime.setText(singletonArenda.getTime(userItem.searchTime));

        // заголовок: Ищут 1-комнатную квартиру
        TextView textRooms = ((TextView) view.findViewById(R.id.userItem_rooms));
        String rooms = ctx.getString(R.string.Realtor_Text_UserItem_Rooms).replace("%@", singletonArenda.generateRooms(userItem.rooms, ctx));
        textRooms.setText(rooms);

        // заголовок: На длительный срок, в районе:
        TextView textRentTime = ((TextView) view.findViewById(R.id.userItem_rentTime));
        String rentTime = ctx.getString(R.string.Realtor_Text_UserItem_RentTime).replace("%@", longRentTime ? ctx.getString(R.string.Realtor_Text_UserItem_RentTime_Month) : ctx.getString(R.string.Realtor_Text_UserItem_RentTime_Day));
        textRentTime.setText(rentTime);

        // заголовок: Уфа, Проспект октября
        TextView textRegion = ((TextView) view.findViewById(R.id.userItem_region));
        textRegion.setText(userItem.region);

        // заголовок: Не дороже: 14000 руб./мес.
        TextView textFlatCost = ((TextView) view.findViewById(R.id.userItem_flatCost));
        String flatCost = singletonArenda.generateFlatCost(userItem.flatCost, longRentTime ? 0 : 1, ctx);
        textFlatCost.setText(flatCost);

        // заголовок: Риелтору не более 1000 руб.
        TextView textRealtorCost = ((TextView) view.findViewById(R.id.userItem_realtorCost));
        String realtorCost = ctx.getString(R.string.Realtor_Text_UserItem_RealtorCost).replace("%@", String.valueOf(userItem.realtorCost));
        textRealtorCost.setText(realtorCost);

        // не показывать заголовок "Риелтору не более 1000 руб.", если посуточно
        textRealtorCost.setVisibility(longRentTime ? View.VISIBLE : View.GONE);

        // присваиваем к кнопке Позвонить - номер телефона соискателя
        Button buttonCall = ((Button) view.findViewById(R.id.userItem_buttonCall));
        buttonCall.setTag("tel" + userItem.phone);

        // если соискатель отменил поиск жилья
        if (userItem.searching.trim().equals("CANCELED")) {
            buttonCall.setText(ctx.getString(R.string.Realtor_Button_SearchCanceled));
            buttonCall.setBackgroundResource(R.drawable.xml_round_red_disabled);
            buttonCall.setEnabled(false);
        } else {
            // если он ищет в данный момент жилье
            buttonCall.setText(ctx.getString(R.string.Realtor_Button_Call));
            buttonCall.setBackgroundResource(R.drawable.xml_round_green);
            buttonCall.setEnabled(true);
        }

        // показываем кнопку Info, если есть комментарий
        ImageButton buttonInfo = ((ImageButton) view.findViewById(R.id.userItem_buttonInfo));
        buttonInfo.setVisibility(userItem.comment.trim().equals("-") ? View.GONE : View.VISIBLE);
        buttonInfo.setTag("info" + userItem.comment);

        // показать индекс соискателя: #1
        TextView textIndex = ((TextView) view.findViewById(R.id.userItem_index));
        textIndex.setText(String.valueOf(position + 1));

        // окрашиваем цвет ячейки соискателя, если риелтор это сделал
        RelativeLayout backgroudColor = ((RelativeLayout) view.findViewById(R.id.group_userItem));
        if (userItem.color.trim().equals("WHITE")) backgroudColor.setBackgroundResource(R.drawable.xml_modal);
        if (userItem.color.trim().equals("RED")) backgroudColor.setBackgroundResource(R.drawable.xml_modal_red);
        if (userItem.color.trim().equals("GREEN")) backgroudColor.setBackgroundResource(R.drawable.xml_modal_green);
        if (userItem.color.trim().equals("BLUE")) backgroudColor.setBackgroundResource(R.drawable.xml_modal_blue);
        if (userItem.color.trim().equals("YELLOW")) backgroudColor.setBackgroundResource(R.drawable.xml_modal_yellow);

        return view;
    }

    activityRealtor_User_Item getUser(int position) {
        return ((activityRealtor_User_Item) getItem(position));
    }

}