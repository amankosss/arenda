package kz.app.arenda;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.Random;

import kz.app.arenda.classes.geolocationService;
import kz.app.arenda.classes.messageDialog;
import kz.app.arenda.classes.networkService;
import kz.app.arenda.classes.sharedSettings;

public class activityWelcome extends Activity implements messageDialog.Listener, networkService.Listener {

    // подключенные классы для работы с диалоговыми сообщениями, настройками и сетью
    messageDialog msgDialog;
    sharedSettings mSettings;
    networkService mNetwork;
    geolocationService geoService;

    // надписи, картинки, кнопки и т.п. элементы, которые используются в данном окне (активити)
    TextView view_textStepTitle;
    TextView view_textStepDescription;
    TextView view_textRegistrationTitle;
    TextView view_textRegistrationPlus7;
    TextView view_textRegistrationAgreement;
    TextView view_textRegistrationAgreementLink;
    TextView view_textRegistrationResendCodeLink;

    ImageView view_imageStepIcon;
    ImageView view_imageRegistrationGreenLine;

    ImageButton view_buttonClose;
    Button view_buttonNextStep;
    Button view_buttonSelectRealtor;
    Button view_buttonSelectUser;
    Button view_buttonContinueSubmitRegistration;

    EditText view_editRegistrationNumber;
    RelativeLayout view_relativeLayoutRegistration;

    ProgressBar view_loaderUserRegistration;
    ProgressBar view_loaderRegistation;

    // переменные данной активити
    int welcomeStep = 0;                            // какой сейчас шаг приветствия на экране?
    int selectedMode = 0;                           // соискателя или риелтора выбрал пользователь?

    boolean phoneChanging = false;                  // в данный момент регистрация пользователя или смена номера телефона?
    boolean codeSubmitting = false;                 // что сейчас пользователь вводит: номер телефона или код подтверждения
    boolean realtorQuickRegistration = false;       // регистрация в качестве риелтора из окна настроек соискателя

    String getCodePostRequest;

    String authUserID = "";
    String authPhone = "";
    String authID = "";

    // обработчик физической кнопки НАЗАД
    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.splash, R.anim.alpha);
    }

    // пользователь нажимает на X, чтобы закрыть окно (только если это смена номера телефона)
    public void tap_close(View v) {
        onBackPressed();
    }

    // событие при показе данного окна
    @Override
    protected void onResume() {
        super.onResume();
        mSettings.saveBoolean(mSettings.IN_BACKGROUND, false);
    }

    // событие при скрытии данного окна
    @Override
    protected void onPause() {
        super.onPause();
        mSettings.saveBoolean(mSettings.IN_BACKGROUND, true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        // инициализация классов для работы с диалоговыми сообщениями, настройками и сетью
        msgDialog = new messageDialog(this, activityWelcome.this);
        mSettings = new sharedSettings(this);
        mNetwork = new networkService(this, this);
        geoService = new geolocationService(null, this);

        view_textStepTitle = (TextView) findViewById(R.id.label_title);
        view_textStepDescription = (TextView) findViewById(R.id.label_description);
        view_textRegistrationTitle = (TextView) findViewById(R.id.label_registration);
        view_textRegistrationPlus7 = (TextView) findViewById(R.id.label_plus7);
        view_textRegistrationAgreement = (TextView) findViewById(R.id.label_agreementDescription);
        view_textRegistrationAgreementLink = (TextView) findViewById(R.id.label_agreementLink);
        view_textRegistrationResendCodeLink = (TextView) findViewById(R.id.label_resendCodeLink);

        // пользователь нажал на ссылку "Пользовательское соглашение"
        view_textRegistrationAgreementLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                singletonArenda.browseURL(getString(R.string.Link_Website) + "/agreement", getApplicationContext());
            }
        });

        // пользователь нажимает на кнопку "Код не пришел?"
        view_textRegistrationResendCodeLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // проверяем наличие интернета
                if (!singletonArenda.checkInternet(getApplicationContext())) {

                    msgDialog.show(null, getString(R.string.Title_Information),
                            getString(R.string.Message_NoInternetConecttion),
                            getString(R.string.Button_OK), null, true);

                    return;
                }

                msgDialog.show(msgDialog.TASK_RESEND_CODE, "+7" + authPhone,
                        getString(R.string.Welcome_Message_ResendCode),
                        getString(R.string.Button_YES), getString(R.string.Button_NO), true);
            }
        });

        view_imageStepIcon = (ImageView) findViewById(R.id.img_icon_welcome);
        view_imageRegistrationGreenLine = (ImageView) findViewById(R.id.img_bg_greenline);

        view_buttonClose = (ImageButton) findViewById(R.id.btn_close_welcome);
        view_buttonNextStep = (Button) findViewById(R.id.btn_next);
        view_buttonSelectRealtor = (Button) findViewById(R.id.btn_realtor);
        view_buttonSelectUser = (Button) findViewById(R.id.btn_user);
        view_buttonContinueSubmitRegistration = (Button) findViewById(R.id.btn_continue);

        view_loaderUserRegistration = (ProgressBar) findViewById(R.id.loader_welcomeUserRegistration);
        view_loaderRegistation = (ProgressBar) findViewById(R.id.loader_welcomeRegistration);

        view_relativeLayoutRegistration = (RelativeLayout) findViewById(R.id.group_registration);
        view_editRegistrationNumber = (EditText) findViewById(R.id.editText_yourNumber);

        // отслеживаем нажатие и ввод в текстовое поле
        view_editRegistrationNumber.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                // если пользователь ничего не указал, то делаем кнопку неактивной
                view_buttonContinueSubmitRegistration.setEnabled(view_editRegistrationNumber.getText().length() != 0);
                view_buttonContinueSubmitRegistration.setBackgroundResource(view_editRegistrationNumber.getText().length() != 0 ? R.drawable.xml_round_green : R.drawable.xml_round_green_disabled);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        welcomeStep = mSettings.loadInt(mSettings.SETTING_WELCOME_STEP);

        // если окно приветствия открыто из окна настроек приложения
        if (welcomeStep != 0) {

            // значит это будет либо регистрация в качестве риелтора, либо его смена номера
            selectedMode = 2;

            // показываем крестик X для закрытия окна и скрываем кнопку "Далее"
            view_buttonClose.setVisibility(View.VISIBLE);
            view_buttonNextStep.setVisibility(View.GONE);

            // если риелтор хочет сменить номер телефона
            if (mSettings.loadString(mSettings.SETTING_MY_USERID).trim().length() == 10) {

                // меняем процесс регистрации на смену номера телефона
                phoneChanging = true;

                // скрываем надпись и ссылку User Agreement
                view_textRegistrationAgreement.setVisibility(View.GONE);
                view_textRegistrationAgreementLink.setVisibility(View.GONE);

                // меняем заголовок "Регистрация" на "Смена номера телефона"
                view_textRegistrationTitle.setText(getString(R.string.Welcome_Text_ChangePhoneNumber));
                view_editRegistrationNumber.setHint(getString(R.string.Welcome_Text_YourCurrentNumber) + mSettings.loadString(mSettings.SETTING_MY_USERID));
            } else {
                realtorQuickRegistration = true;
            }
        }

        // обновляем шаг на экране
        updateStepInformation();
    }

    // прослушиваем нажатие кнопок в диалоговых сообщениях
    @Override
    public void onMessageDialogButtonClicked(String dialogTask, boolean isPositive) {

        if (!isPositive) return;

        // если мы показывали подсказку разрешить определение местоположения (для Android 6.0)
        if (dialogTask.trim().equals(msgDialog.TASK_NEED_LOCATION_PERMISSION)) {

            // если нет разрешения, то запрашиваем
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, singletonArenda.MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                // если разрешение есть, то получаем координаты
                geoService.getMyLocation();
            }

            return;
        }

        // человек хочет выслать код повторно
        if (dialogTask.trim().equals(msgDialog.TASK_RESEND_CODE)) {

            hideKeyboard();
            mNetwork.postRequest(mNetwork.TASK_NEW_CODE, mNetwork.PHP_FILE_REG, getCodePostRequest, mNetwork.TIMEOUT_DISABLED);
        }

        // если человек ввел номер телефона и нажал ДА
        if (dialogTask.trim().equals(msgDialog.TASK_CHECK_NUMBER)) {

            view_textRegistrationPlus7.setVisibility(View.GONE);
            view_imageRegistrationGreenLine.setVisibility(View.GONE);
            view_buttonContinueSubmitRegistration.setVisibility(View.GONE);
            view_editRegistrationNumber.setVisibility(View.GONE);
            view_textRegistrationAgreementLink.setVisibility(View.GONE);
            view_textRegistrationAgreement.setVisibility(View.GONE);
            view_loaderRegistation.setVisibility(View.VISIBLE);

            hideKeyboard();

            // если это первичная регистрация
            if (!phoneChanging) {
                getCodePostRequest = "action=newuser" +
                        "&task=newcode" +
                        "&phone=" + view_editRegistrationNumber.getText().toString();
            } else {
                // если это изменение телефона, то добавляем к запросу информацию о текущем телефоне и ID
                getCodePostRequest = "action=newuser" +
                        "&task=newcode" +
                        "&phone=" + view_editRegistrationNumber.getText().toString() +
                        "&oldphone=" + mSettings.loadString(mSettings.SETTING_MY_USERID) +
                        "&id=" + mSettings.loadString(mSettings.SETTING_MY_ID);
            }

            // отправляем на сервер запрос получения кода подтверждения
            mNetwork.postRequest(mNetwork.TASK_NEW_CODE, mNetwork.PHP_FILE_REG, getCodePostRequest, mNetwork.TIMEOUT_10_SECONDS);
        }

    }

    // прослушиваем разрешение на геолокацию (только в Android 6.0 и выше)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (requestCode != singletonArenda.MY_PERMISSIONS_REQUEST_LOCATION) return;

        // если пользователь дал разрешение на определение местоположения
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            geoService.getMyLocation();
    }

    // прослушиваем ответы от сервера
    @Override
    public void onNetworkCallback(String requestTask, String resultString) {

        // если задачи не относятся к окну приветствия (проверка на всякий случай)
        if (!requestTask.trim().equals(mNetwork.TASK_NEW_CODE) &&
            !requestTask.trim().equals(mNetwork.TASK_CHECK_CODE) &&
            !requestTask.trim().equals(mNetwork.TASK_REG_TENANT)) return;

        // проверка на таймаут
        if (resultString.trim().equals("TIMEOUT")) {

            if (requestTask.trim().equals(mNetwork.TASK_REG_TENANT)) {
                showModeButtons(true);
            } else {
                returnEnteringPhoneNumber();
            }

            msgDialog.show(null, getString(R.string.Title_NetworkTimeout),
                    getString(R.string.Message_PleaseTryAgain),
                    getString(R.string.Button_OK), null, true);

            return;
        }

        // разбиваем полученный ответ на массив
        String[] resultArray = resultString.split("\\*");

        // если это успешная регистрация соискателя
        if (resultArray[0].trim().equals(mNetwork.ECHO_REG_TENANT_SUCCESS)) {

            // делаем небольшую задержку в 0.5 сек и завершаем процесс регистрации
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    saveAuthInfoAndStart();
                }
            }, 500);
            return;
        }

        // если такой соискатель уже существует
        if (resultArray[0].trim().equals(mNetwork.ECHO_TENANT_EXISTS)) {

            showModeButtons(true);

            msgDialog.show(null, getString(R.string.Title_Information),
                    getString(R.string.Welcome_Message_UserExistsError),
                    getString(R.string.Button_OK), null, true);

            return;
        }

        // если требуется подтвердить номер телефона кодом
        if (resultArray[0].trim().equals(mNetwork.ECHO_CODE_SENT)) {

            // если это была повторная отправка кода
            if (codeSubmitting) return;

            view_textRegistrationResendCodeLink.setVisibility(View.VISIBLE);
            view_loaderRegistation.setVisibility(View.GONE);

            authPhone = view_editRegistrationNumber.getText().toString();

            String regLabel = getString(R.string.Welcome_Text_CodeSentTo) + authPhone;
            view_textRegistrationTitle.setText(regLabel);

            view_imageRegistrationGreenLine.setVisibility(View.VISIBLE);

            view_buttonContinueSubmitRegistration.setText(getString(R.string.Welcome_Button_Submit));
            view_buttonContinueSubmitRegistration.setVisibility(View.VISIBLE);
            view_buttonContinueSubmitRegistration.setEnabled(false);

            view_editRegistrationNumber.setText("");
            view_editRegistrationNumber.setGravity(Gravity.CENTER);
            view_editRegistrationNumber.setHint(getString(R.string.Welcome_Text_EnterCodeHint));
            view_editRegistrationNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
            view_editRegistrationNumber.setVisibility(View.VISIBLE);

            codeSubmitting = true;
            return;
        }

        // если это успешная регистрация риелтора
        if (resultArray[0].trim().equals(mNetwork.ECHO_REG_SUCCESS)) {

            hideKeyboard();
            authUserID = authPhone;
            authID = view_editRegistrationNumber.getText().toString();

            // делаем небольшую задержку в 0.5 сек и завершаем процесс регистрации/смены номера
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    saveAuthInfoAndStart();
                }
            }, 500);
            return;
        }

        // если код подтверждения неверный
        if (resultArray[0].trim().equals(mNetwork.ECHO_ERROR_CODE)) {

            // возвращаем положение элементов на экране в исходное представление
            returnEnteringPhoneNumber();

            msgDialog.show(null, getString(R.string.Title_Information),
                    getString(R.string.Welcome_Message_PleaseEnterCorrectCode),
                    getString(R.string.Button_OK), null, true);

            return;
        }

        // если сервер сообщает о любой из нижеследующих ошибок
        String messageText = "";

        if (resultArray[0].trim().equals(mNetwork.ECHO_WRONG_PHONE))
            messageText = getString(R.string.Welcome_Message_WrongPhone);

        if (resultArray[0].trim().equals(mNetwork.ECHO_ERROR_PHONE))
            messageText = getString(R.string.Welcome_Message_ErrorPhone);

        if (resultArray[0].trim().equals(mNetwork.ECHO_PLEASE_WAIT))
            messageText = getString(R.string.Welcome_Message_PleaseWait1Minute);

        if (resultArray[0].trim().equals(mNetwork.ECHO_DAY_LIMIT))
            messageText = getString(R.string.Welcome_Message_DayLimit);

        if (resultArray[0].trim().equals(mNetwork.ECHO_ALREADY_CHANGED))
            messageText = getString(R.string.Welcome_Message_AlreadyChanged);

        if (messageText.length() != 0) {

            returnEnteringPhoneNumber();
            msgDialog.show(null, getString(R.string.Title_Information),
                    messageText,
                    getString(R.string.Button_OK), null, true);
        }
    }

    // метод выполняется после регистрации, он сохраняет данные о регистрации/смены номера
    private void saveAuthInfoAndStart() {

        // сохраняем наш userID, номер и код (в будущем используется как ID)
        mSettings.saveString(mSettings.SETTING_MY_USERID, authUserID);
        mSettings.saveString(mSettings.SETTING_MY_NUMBER, authPhone);
        mSettings.saveString(mSettings.SETTING_MY_ID, authID);

        // сохраняем текущий шаг приветствия и выбранный режим пользователя
        mSettings.saveInt(mSettings.SETTING_WELCOME_STEP, welcomeStep);
        mSettings.saveInt(mSettings.SETTING_CURRENT_MODE, selectedMode);

        // если это была первичная регистрация, то запускаем нужное окно
        if (!phoneChanging) {
            if (selectedMode == 1)
                startActivity(new Intent(activityWelcome.this, activityMain.class));

            if (selectedMode == 2) {

                if (!realtorQuickRegistration) {
                    startActivity(new Intent(activityWelcome.this, activityRealtor.class));
                } else {
                    startActivity(new Intent(activityWelcome.this, activitySplashscreen.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                }

            }
        }

        // если это была смена телефона, то просто закрываем окно
        onBackPressed();
    }

    // вернуть первоначальное визуальное представление элементов на экране (при регистрации риелтора)
    private void returnEnteringPhoneNumber() {

        // скрываем loading
        view_loaderRegistation.setVisibility(View.GONE);

        // если это ввод номера телефона и при этом не смена телефона, то показываем User Agreement
        if (!codeSubmitting && !phoneChanging) {
            view_textRegistrationAgreement.setVisibility(View.VISIBLE);
            view_textRegistrationAgreementLink.setVisibility(View.VISIBLE);
        }

        // показать надпись "+7" если это ввод телефона
        if (!codeSubmitting) view_textRegistrationPlus7.setVisibility(View.VISIBLE);

        // показать ссылку "Код не пришел" есди это ввод кода
        if (codeSubmitting) view_textRegistrationResendCodeLink.setVisibility(View.VISIBLE);

        // показать кнопку Продолжить/Подтвердить, зеленую полоску и поле ввода
        view_buttonContinueSubmitRegistration.setVisibility(View.VISIBLE);
        view_imageRegistrationGreenLine.setVisibility(View.VISIBLE);
        view_editRegistrationNumber.setVisibility(View.VISIBLE);
    }

    // показать или скрыть кнопки выбора режима и loading (при регистрации соискателя)
    private void showModeButtons(boolean show) {

        view_buttonSelectRealtor.setVisibility(show ? View.VISIBLE : View.GONE);
        view_buttonSelectUser.setVisibility(show ? View.VISIBLE : View.GONE);

        view_loaderUserRegistration.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    // обновить шаг на экране
    private void updateStepInformation() {

        view_textStepTitle.setVisibility(View.GONE);
        view_textStepDescription.setVisibility(View.GONE);
        view_imageStepIcon.setVisibility(View.GONE);

        view_buttonSelectRealtor.setVisibility(View.GONE);
        view_buttonSelectUser.setVisibility(View.GONE);

        if (welcomeStep == 0) {
            view_textStepTitle.setText(getString(R.string.Welcome_Text_Step_1));
            view_textStepDescription.setText(getString(R.string.Welcome_Text_StepDesc_1));
            view_imageStepIcon.setBackgroundResource(R.drawable.icon_welcome_step_1);
        }

        if (welcomeStep == 1) {
            view_textStepTitle.setText(getString(R.string.Welcome_Text_Step_2));
            view_textStepDescription.setText(getString(R.string.Welcome_Text_StepDesc_2));
            view_imageStepIcon.setBackgroundResource(R.drawable.icon_welcome_step_2);
        }

        if (welcomeStep == 2) {
            view_textStepTitle.setText(getString(R.string.Welcome_Text_Step_3));
            view_textStepDescription.setText(getString(R.string.Welcome_Text_StepDesc_3));
            view_imageStepIcon.setBackgroundResource(R.drawable.icon_welcome_step_3);
        }

        if (welcomeStep == 3) {
            view_textStepTitle.setText(getString(R.string.Welcome_Text_Step_4));
            view_textStepDescription.setText(getString(R.string.Welcome_Text_StepDesc_4));
            view_imageStepIcon.setBackgroundResource(R.drawable.icon_welcome_step_4);
        }

        // если это шаг регистрации (ввода номера телефона), то показываем его
        if (welcomeStep == 4) {

            // добавляем эффект анимации, если это не смена номера телефона
            if (!phoneChanging) view_relativeLayoutRegistration.setAnimation(singletonArenda.setFadeInAnimation(true, 500, getApplicationContext()));
            view_relativeLayoutRegistration.setVisibility(View.VISIBLE);

            return;
        }

        // создаем эффект затухания-появления с помощью задержки в 0.5 сек
        new Handler().postDelayed(new Runnable() {
            public void run() {

                view_textStepTitle.setAnimation(singletonArenda.setFadeInAnimation(true, 500, getApplicationContext()));
                view_textStepTitle.setVisibility(View.VISIBLE);

                view_textStepDescription.setAnimation(singletonArenda.setFadeInAnimation(true, 500, getApplicationContext()));
                view_textStepDescription.setVisibility(View.VISIBLE);

                view_imageStepIcon.setAnimation(singletonArenda.setFadeInAnimation(true, 500, getApplicationContext()));
                view_imageStepIcon.setVisibility(View.VISIBLE);

                view_buttonNextStep.setEnabled(true);

                // если пользователь нажал один раз "Далее",
                if (welcomeStep == 1) {
                    // показать подсказку на разрешение местоположения, если это Android 6.0
                    if (Build.VERSION.SDK_INT > 22) {
                        msgDialog.show(msgDialog.TASK_NEED_LOCATION_PERMISSION, getString(R.string.Welcome_Text_Location_Title),
                                getString(R.string.Welcome_Text_Location_Message),
                                getString(R.string.Button_OK), null, false);
                    } else {
                        geoService.getMyLocation();
                    }
                }

            }
        }, 100);

    }

    // 1. Пользователь нажимает "Далее" 3 раза
    public void tap_next(View v) {

        view_buttonNextStep.setEnabled(false);

        welcomeStep++;
        updateStepInformation();

        // при этом если на экране 3 шаг, то нужно показать кнопки "Я риелтор" и "Я ищу жилье"
        if (welcomeStep == 3) {
            view_buttonNextStep.setVisibility(View.GONE);

            // создаем эффект затухания-появления с помощью задержки в 0.3 сек
            new Handler().postDelayed(new Runnable() {
                public void run() {

                    view_buttonSelectRealtor.setAnimation(singletonArenda.setFadeInAnimation(true, 500, getApplicationContext()));
                    view_buttonSelectRealtor.setVisibility(View.VISIBLE);

                    view_buttonSelectUser.setAnimation(singletonArenda.setFadeInAnimation(true, 500, getApplicationContext()));
                    view_buttonSelectUser.setVisibility(View.VISIBLE);
                }
            }, 300);
        }

    }

    // 2. пользователь выбрал режим риелтора
    public void tap_realtor(View v) {

        selectedMode = 2;
        welcomeStep = 4;

        updateStepInformation();
    }

    // 2. пользователь выбрал режим соискателя
    public void tap_user(View v) {

        // проверяем наличие интернета
        if (!singletonArenda.checkInternet(getApplicationContext())) {

            msgDialog.show(null, getString(R.string.Title_Information),
                    getString(R.string.Message_NoInternetConecttion),
                    getString(R.string.Button_OK), null, true);
            return;
        }

        selectedMode = 1;
        welcomeStep = 4;

        showModeButtons(false);

        // формируем userID и codeID
        Random rand = new Random();

        int randPart = rand.nextInt(9999999 - 1000000 + 1) + 1000000;
        int randPart2 = rand.nextInt(9999999 - 1000000 + 1) + 1000000;
        int randPart3 = rand.nextInt(999999 - 100000 + 1) + 100000;

        authUserID = String.valueOf(randPart) + String.valueOf(randPart2) + String.valueOf(randPart3);
        authID = String.valueOf(rand.nextInt(999999 - 100000 + 1) + 100000);

        String params = "action=newuser" +
                "&task=newtenant" +
                "&userid=" + authUserID +
                "&id=" + String.valueOf(authID);

        mNetwork.postRequest(mNetwork.TASK_REG_TENANT, mNetwork.PHP_FILE_REG, params, mNetwork.TIMEOUT_10_SECONDS);
    }

    // 3. пользователь нажимает на кнопку "Продолжить/Подтвердить"
    public void tap_continue(View v) {

        // +++++++++++++++ если в данный момент подтверждение кода +++++++++++++++
        if (codeSubmitting) {

            String regCode = view_editRegistrationNumber.getText().toString();

            // 1. проверяем наличие интернета
            if (!singletonArenda.checkInternet(getApplicationContext())) {

                msgDialog.show(null, getString(R.string.Title_Information),
                        getString(R.string.Message_NoInternetConecttion),
                        getString(R.string.Button_OK), null, true);
                return;
            }

            // 2. Проверяем длину кода на 6 цифр
            if (regCode.length() != 6) {

                msgDialog.show(null, getString(R.string.Title_Information),
                        getString(R.string.Welcome_Message_6DigitsInCode),
                        getString(R.string.Button_OK), null, true);
                return;
            }

            // 3. Если все хорошо, то скрываем все элементы и показываем loading
            view_editRegistrationNumber.setVisibility(View.GONE);
            view_imageRegistrationGreenLine.setVisibility(View.GONE);
            view_buttonContinueSubmitRegistration.setVisibility(View.GONE);
            view_textRegistrationResendCodeLink.setVisibility(View.GONE);

            view_loaderRegistation.setVisibility(View.VISIBLE);

            String params;

            // если это первичная регистрация, то отправляем связку номер - код - режим
            if (!phoneChanging) {
                params = "action=newuser" +
                         "&task=checkcode" +
                         "&userid=" + mSettings.loadString(mSettings.SETTING_MY_USERID) +
                         "&phone=" + authPhone +
                         "&id=" + view_editRegistrationNumber.getText().toString() +
                         "&mode=" + selectedMode;
            } else {
                // если это смена номера, то отправляем связку новый номер - код - старый номер
                params = "action=newuser" +
                        "&task=checkcode" +
                        "&phone=" + authPhone +
                        "&id=" + view_editRegistrationNumber.getText().toString() +
                        "&oldphone=" + mSettings.loadString(mSettings.SETTING_MY_USERID);
            }

            // 4. отправляем запрос на сервер
            mNetwork.postRequest(mNetwork.TASK_CHECK_CODE, mNetwork.PHP_FILE_REG, params, mNetwork.TIMEOUT_10_SECONDS);
            return;
        }

        // +++++++++++++++ если человек еще только вводит номер телефона +++++++++++++++
        String regNumber = view_editRegistrationNumber.getText().toString();

        // 1. проверяем наличие интернета
        if (!singletonArenda.checkInternet(getApplicationContext())) {

            msgDialog.show(null, getString(R.string.Title_Information),
                    getString(R.string.Message_NoInternetConecttion),
                    getString(R.string.Button_OK), null, true);
            return;
        }

        // 2. проверяем длину номера телефона на 10 цифр
        if (regNumber.length() != 10) {

            msgDialog.show(null, getString(R.string.Title_Information),
                    getString(R.string.Welcome_Message_10DigitsInNumber),
                    getString(R.string.Button_OK), null, true);
            return;
        }

        // [проверка убрана] 3. Проверяем, в правильном ли формате указан номер
//        if  (!regNumber.substring(0,1).trim().equals("9")) {
//
//            msgDialog.show(null, getString(R.string.Title_Information),
//                    getString(R.string.Welcome_Message_BadNumberFormat),
//                    getString(R.string.Button_OK), null, true);
//            return;
//        }

        // 4. проверка на совпадение нового телефона с текущим (если это смена)
        if (phoneChanging && mSettings.loadString(mSettings.SETTING_MY_USERID).trim().equals(regNumber)) {

            msgDialog.show(null, getString(R.string.Title_Information),
                    getString(R.string.Welcome_Message_BusyNumber),
                    getString(R.string.Button_OK), null, true);
            return;
        }

        String changeComment = phoneChanging ? "\n\n" + getString(R.string.Welcome_Message_1NumberPerDay) : "";

        // 5. если проверки прошли, просим пользователя проверить номер и нажать ДА
        msgDialog.show(msgDialog.TASK_CHECK_NUMBER, "+7" + regNumber,
                getString(R.string.Welcome_Message_SubmitNumber) + changeComment,
                getString(R.string.Button_YES), getString(R.string.Button_NO), true);
    }

    // скрываем клавиатуру с экрана, когда это нужно
    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view_editRegistrationNumber.getWindowToken(), 0);
    }

}