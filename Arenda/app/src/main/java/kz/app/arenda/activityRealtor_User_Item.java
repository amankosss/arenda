package kz.app.arenda;

public class activityRealtor_User_Item {

    String phone;
    String searching;
    int actionTime;
    int searchTime;
    String region;
    int rooms;
    String rentTime;
    int flatCost;
    int realtorCost;
    String comment;
    String color;
    int newUser;

    activityRealtor_User_Item (String _phone, String _searching, int _actionTime, int _searchTime, String _region, int _rooms,
              String _rentTime, int _flatCost, int _realtorCost, String _comment, String _color, int _newUser) {

        phone = _phone;
        searching = _searching;
        actionTime = _actionTime;
        searchTime = _searchTime;
        region = _region;
        rooms = _rooms;
        rentTime = _rentTime;
        flatCost = _flatCost;
        realtorCost = _realtorCost;
        comment = _comment;
        color = _color;
        newUser = _newUser;
    }

}