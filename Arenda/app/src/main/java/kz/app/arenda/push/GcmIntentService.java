package kz.app.arenda.push;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import kz.app.arenda.R;
import kz.app.arenda.activityMain;
import kz.app.arenda.activityRealtor;
import kz.app.arenda.classes.sharedSettings;

// ЗДЕСЬ НАСТРОЙКИ PUSH УВЕДОМЛЕНИЙ (НИЧЕГО НЕ ТРОГАТЬ)

public class GcmIntentService extends IntentService {

    sharedSettings mSettings;

    public GcmIntentService() {
        super("52526479297");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        String pushMessage = intent.getStringExtra("message");
        if (extras.isEmpty()) return;

        mSettings = new sharedSettings(this);

        // проверяем свернуто ли приложение?
        if (mSettings.loadBoolean(mSettings.IN_BACKGROUND))
            sendNotification(pushMessage, mSettings.loadInt(mSettings.SETTING_CURRENT_MODE));

        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // показываем PUSH уведомление на экране телефоне
    private void sendNotification(String pushMessage, int currentMode) {
        NotificationManager n = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent i = new Intent(this, currentMode == 1 ? activityMain.class : activityRealtor.class);
        i.putExtra("message", pushMessage);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder b = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_push)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                        .setContentTitle("Arenda")
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(pushMessage))
                        .setAutoCancel(true)
                        .setTicker(pushMessage)
                        .setContentText(pushMessage);

        b.setVibrate(new long[]{500, 500, 500, 500, 500});
        b.setSound(sound);

        b.setContentIntent(contentIntent);
        n.notify(1, b.build());
    }

}