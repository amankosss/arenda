package kz.app.arenda.map;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.SupportMapFragment;

public class DraggableMapFragment extends SupportMapFragment {

    private View mOriginalView;
    private DragLayout mDragLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mOriginalView = super.onCreateView(inflater, container, savedInstanceState);

        mDragLayout = new DragLayout(getActivity());
        mDragLayout.addView(mOriginalView);

        return mDragLayout;
    }

    @Override
    public View getView() {
        return mOriginalView;
    }

    public void setOnDragListener(DragLayout.OnDragListener onDragListener) {
        mDragLayout.setOnDragListener(onDragListener);
    }
}