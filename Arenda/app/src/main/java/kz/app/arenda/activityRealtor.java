package kz.app.arenda;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import kz.app.arenda.classes.billingService;
import kz.app.arenda.classes.databaseService;
import kz.app.arenda.classes.gcmService;
import kz.app.arenda.classes.geolocationService;
import kz.app.arenda.classes.messageDialog;
import kz.app.arenda.classes.networkService;
import kz.app.arenda.classes.sharedSettings;

public class activityRealtor extends Activity implements messageDialog.Listener, networkService.Listener, gcmService.Listener, geolocationService.Listener, billingService.Listener {

    // подключенные классы для работы с диалоговыми сообщениями, настройками и сетью
    messageDialog msgDialog;
    sharedSettings mSettings;
    networkService mNetwork;
    gcmService mGoogleCloudMessaging;
    geolocationService geoService;
    billingService mBillingService;

    // надписи, картинки, кнопки и т.п. элементы, которые используются в данном окне (активити)
    TextView view_textUsersOnline;
    TextView view_textYourRegion;
    TextView view_textExtendAccess;
    TextView view_textPayment10DaysHint;
    TextView view_textPayment30DaysHint;

    ProgressBar view_loader;
    ListView view_listUsers;
    RelativeLayout view_relativeUsersEmpty;
    RelativeLayout view_relativePayment;

    Button view_buttonPay1day;
    Button view_buttonPay10days;
    Button view_buttonPay30days;

    ImageView view_imageBackgroundBlack;

    // переменные данной активити
    Timer timerUsers;                              // таймер запроса новых соискателей

    boolean flagErrorPhone;                        // флаг - сообщение о недействительном телефона на экране сейчас
    boolean flagUpdateApp;                         // флаг - сообщение обновитесь на экране сейчас
    boolean flagCheckInternet;                     // флаг - сообщение нет интернета на экране сейчас
    boolean flagNoLocation;                        // флаг - сообщение нет местоположения на экране сейчас

    boolean userInfoUpdated;                       // информация о риелторе обновилась при старте окна
    boolean callsIsDisabled;                       // запрещены ли звонки
    boolean isBanned;                              // заблокирован ли мой номер телефона?

    SQLiteDatabase sqLite;                          // Управление SQL базой

    ArrayList <activityRealtor_User_Item> mUsersArray = new ArrayList <> ();
    activityRealtor_User_Adapter mAdapterUsers;

    // обработчик физической кнопки НАЗАД
    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.splash, R.anim.alpha);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realtor);

        // инициализация классов для работы с диалоговыми сообщениями, настройками и сетью
        msgDialog = new messageDialog(this, activityRealtor.this);
        mSettings = new sharedSettings(this);
        mNetwork = new networkService(this, this);
        mGoogleCloudMessaging = new gcmService(this, this, true);
        geoService = new geolocationService(this, this);
        mBillingService = new billingService(this, activityRealtor.this);

        //todo: временно
        if (mSettings.loadString(mSettings.SETTING_MY_USERID).trim().equals(""))
            mSettings.saveString(mSettings.SETTING_MY_USERID, mSettings.loadString(mSettings.SETTING_MY_NUMBER));

        databaseService dbService = new databaseService(this, "arenda.db", null, 1);
        sqLite = dbService.getWritableDatabase();

        // настраиваем список соискателей
        view_listUsers = (ListView) findViewById(R.id.listview_users);
        mAdapterUsers = new activityRealtor_User_Adapter(this, mUsersArray);
        view_listUsers.setAdapter(mAdapterUsers);
        view_listUsers.setOnItemLongClickListener(onItemLongClickListener);

        view_textUsersOnline = (TextView)findViewById(R.id.label_usersOnline);
        view_textYourRegion = (TextView)findViewById(R.id.label_yourRegion);
        view_textExtendAccess = (TextView)findViewById(R.id.label_extendAccess);
        view_textPayment10DaysHint = (TextView)findViewById(R.id.label_payment10DaysHint);
        view_textPayment30DaysHint = (TextView)findViewById(R.id.label_payment30DaysHint);

        view_loader = (ProgressBar)findViewById(R.id.loader_realtor);
        view_relativeUsersEmpty = (RelativeLayout)findViewById(R.id.group_usersEmpty);
        view_relativePayment = (RelativeLayout)findViewById(R.id.modal_payment);

        view_buttonPay1day = (Button)findViewById(R.id.btn_pay_1day);
        view_buttonPay10days = (Button)findViewById(R.id.btn_pay_10days);
        view_buttonPay30days = (Button)findViewById(R.id.btn_pay_30days);

        view_imageBackgroundBlack = (ImageView)findViewById(R.id.img_bg_black);

        // загружаем список соискателей, сохраненных ранее в базе данных
        loadUsers();
        callsIsDisabled = mSettings.loadBoolean(mSettings.SETTING_CALLS_IS_DISABLED);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mBillingService.destroy();
    }

    // событие при показе данного окна
    @Override
    protected void onResume() {
        super.onResume();
        mSettings.saveBoolean(mSettings.IN_BACKGROUND, false);

        geoService.getMyLocation();

        new Handler().postDelayed(new Runnable() {
            public void run() {
                startTimerAndRequest();
            }
        }, 1000);
    }

    // событие при скрытии данного окна
    @Override
    protected void onPause() {
        super.onPause();
        mSettings.saveBoolean(mSettings.IN_BACKGROUND, true);

        stopTimer();

        // убираем у всех новых ячеек надпись "новый"
        sqLite.execSQL("UPDATE USERS SET NEWUSER = '0' WHERE NEWUSER = '1' ");

        for (int i = 0; i < mUsersArray.size(); i++) {
            if (mUsersArray.get(i).newUser == 1) {
                mUsersArray.get(i).newUser = 0;
            }
        }

        mAdapterUsers.notifyDataSetChanged();
    }

    // получаем Device Token от сервиса Google Cloud Messaging
    @Override
    public void onGetDeviceToken(String tokenString) {
        mNetwork.updateDeviceToken(tokenString);
    }

    // получаем название города, например "Ваш город: Уфа"
    @Override
    public void onGetCityAndStreetName(String cityStreet) {

        if (cityStreet.trim().equals("underfined")) {
            view_textYourRegion.setText(getString(R.string.Realtor_Text_UnderfinedCity));
            return;
        }

        String city = getString(R.string.Realtor_Text_YourCity) + " " + cityStreet;
        view_textYourRegion.setText(city);
    }

    // прослушиваем нажатие кнопок в диалоговых сообщениях
    @Override
    public void onMessageDialogButtonClicked(String dialogTask, boolean isPositive) {

        flagErrorPhone = false;
        flagUpdateApp = false;
        flagCheckInternet = false;
        flagNoLocation = false;

        if (!isPositive) return;

        // если это ссылка на договор аренды
        if (dialogTask.trim().equals(msgDialog.TASK_SHOW_CONTRACT)) {
            singletonArenda.browseURL(getString(R.string.Link_Contract), getApplicationContext());
            return;
        }

        // если пользователь просит помощи настроить геолокацию
        if (dialogTask.trim().equals(msgDialog.TASK_LOCATION_HELP)) {
            singletonArenda.browseURL(getString(R.string.Link_Help), getApplicationContext());
            return;
        }

        // если это обновление приложения
        if (dialogTask.trim().equals(msgDialog.TASK_UPDATE_APP)) {
            singletonArenda.browseURL(getString(R.string.Link_GooglePlay), getApplicationContext());
            return;
        }

        // если это сообщение об истечении тестового периода
        if (dialogTask.trim().equals(msgDialog.TASK_TEST_PERIOD_EXPIRED)) {
            openShop(false);
            return;
        }

        // произошел таймаут покупки товара
        if (dialogTask.trim().equals(msgDialog.TASK_PURCHASE_TIMEOUT)) {
            mBillingService.currentPurchaseTry ++;
            postForPurchase();
            return;
        }

        // если подтвердить покупку не удалось и пользователь требует помощи
        if (dialogTask.trim().equals(msgDialog.TASK_PURCHASE_NOT_VALIDATED_OR_FAILED))
            singletonArenda.browseURL(getString(R.string.Link_Support), getApplicationContext());
    }

    // прослушиваем ответы от сервера
    @Override
    public void onNetworkCallback(String requestTask, String resultString) {

        // если задачи не относятся к окну риелтора (проверка на всякий случай)
        if (!requestTask.trim().equals(mNetwork.TASK_GET_USERS) &&
            !requestTask.trim().equals(mNetwork.TASK_VALIDATING_PURCHASE)) return;

        isBanned = false;

        // разбиваем полученный ответ на массив
        String[] resultArray = resultString.split("\\*");

        // если требуется обновить приложение
        if (resultArray[0].trim().equals(mNetwork.ECHO_UPDATE_APP) && !flagUpdateApp) {

            flagUpdateApp = true;
            stopTimer();

            msgDialog.show(msgDialog.TASK_UPDATE_APP, getString(R.string.Title_NewVersion) + " " + resultArray[1],
                    getString(R.string.Message_PleaseUpdateApp),
                    getString(R.string.Button_Update), getString(R.string.Button_Cancel), true);
            return;
        }

        // если номер телефона недействителен (возможно бан)
        if (resultArray[0].trim().equals(mNetwork.ECHO_ERROR_PHONE)) {

            isBanned = true;
            stopTimer();

            if (flagErrorPhone) return;
            flagErrorPhone = true;

            showMessageYouAreBanned();
            return;
        }

        // если это список соискателей
        if (resultArray[0].trim().equals(mNetwork.ECHO_USERS)) {

            String users = getString(R.string.Realtor_Text_UsersInCity) + " " + mUsersArray.size();
            view_textUsersOnline.setText(users);

            view_textUsersOnline.setVisibility(View.VISIBLE);
            view_textYourRegion.setVisibility(View.VISIBLE);

            view_loader.setVisibility(View.GONE);

            String[] configArray = resultArray[1].split("\\|");

            // показать новость, если она есть
            showNews(configArray[0]);

            // разрешены ли звонки?
            callsIsDisabled = !configArray[1].trim().equals("1");
            mSettings.saveBoolean(mSettings.SETTING_CALLS_IS_DISABLED, callsIsDisabled);

            if (resultArray.length == 2) return;

            // в фоновом потоке добавляем всевозможных соискателей в базу и на экран
            new addUsersToBase(resultArray).execute();
            return;
        }

        // во время покупки произошла ошибка, возможно джейлбрейк
        if (resultArray[0].trim().equals(mNetwork.ECHO_PAYMENT_ERROR)) {

            enableShopButtons(true);

            msgDialog.show(null, getString(R.string.Title_Error),
                    getString(R.string.Realtor_Message_PaymentJailbroken),
                    getString(R.string.Button_OK), null, true);
            return;
        }

        // успешна оплата
        if (resultArray[0].trim().equals(mNetwork.ECHO_PAYMENT_SUCCESS)) {

            view_textExtendAccess.setText(getString(R.string.Realtor_Text_Payment_Status_ThankYou));

            callsIsDisabled = false;
            mSettings.saveBoolean(mSettings.SETTING_CALLS_IS_DISABLED, false);
            return;
        }

        // Если вдруг сработал таймаут покупки
        if (resultString.trim().equals("TIMEOUT") && requestTask.trim().equals(mNetwork.TASK_VALIDATING_PURCHASE)) {

            if (mBillingService.currentPurchaseTry == 3) {
                enableShopButtons(true);
                msgDialog.show(msgDialog.TASK_PURCHASE_NOT_VALIDATED_OR_FAILED, getString(R.string.Title_Information),
                        getString(R.string.Realtor_Message_PaymentNotValidated),
                        getString(R.string.Button_Help), getString(R.string.Button_OK), true);
                return;
            }

            msgDialog.show(msgDialog.TASK_PURCHASE_TIMEOUT, getString(R.string.Title_NetworkTimeout),
                    getString(R.string.Realtor_Message_PaymentNotSaved),
                    getString(R.string.Button_OK), null, true);
        }

    }

    class addUsersToBase extends AsyncTask<String, String, String> {

        String[] echoArray;

        addUsersToBase(String[] _echoArray) {
            echoArray = _echoArray;
        }

        int savedLastActionTime = 0;
        int newUsers = 0;

        @Override
        protected String doInBackground(String... params) {

            String[] subArray;

            for (int i=2; i<echoArray.length; i++) {

                subArray = echoArray[i].split("\\|");
                if (subArray.length < 10) return "";

                String userPhone = subArray[0];
                String userSearching = subArray[1];

                int userActionTime = Integer.parseInt(subArray[2]);
                int userSearchTime = Integer.parseInt(subArray[3]);
                String userRegion = subArray[4];
                int userRooms = Integer.parseInt(subArray[5]);
                String userRentTime = subArray[6];
                int userFlatCost = Integer.parseInt(subArray[7]);
                int userRealtorCost = Integer.parseInt(subArray[8]);
                String userComment = subArray[9];

                // cохраняем каретку
                savedLastActionTime = userActionTime;

                // если это изменение статуса поиска на "Поиск отменен"
                if (userSearching.trim().equals("CANCELED")) {
                    sqLite.execSQL("UPDATE USERS SET SEARCHING = 'CANCELED' WHERE PHONE = '" + userPhone + "' ");

                    for (int ii = 0; ii < mUsersArray.size(); ii++) {
                        if (mUsersArray.get(ii).phone.trim().equals(userPhone)) {
                            mUsersArray.get(ii).searching = "CANCELED";
                            break;
                        }
                    }

                } else {

                    newUsers++;

                    // если же это новый поиск от соискателя,
                    // то вначале удаляем текущую запись в базе
                    sqLite.execSQL("DELETE from USERS where PHONE = '" + userPhone + "' ");

                    // и добавляем новую
                    sqLite.execSQL("INSERT INTO USERS ('PHONE', 'SEARCHING', 'ACTIONTIME', 'SEARCHTIME', 'REGION', 'ROOMS', 'RENTTIME', 'FLATCOST', 'REALTORCOST', 'COMMENT', 'COLOR', 'NEWUSER') " +
                            "VALUES ('" + userPhone + "', '" + userSearching + "', " + userActionTime + ", " + userSearchTime + ", '" + userRegion + "', " + userRooms + ", '" + userRentTime + "', " + userFlatCost + ", " + userRealtorCost + ", '" + userComment + "', 'WHITE', 1) ");

                    for (int ii = 0; ii < mUsersArray.size(); ii++) {
                        if (mUsersArray.get(ii).phone.trim().equals(userPhone)) {
                            mUsersArray.remove(ii);
                            break;
                        }
                    }

                    addUserToList(userPhone, userSearching, userActionTime, userSearchTime, userRegion, userRooms,
                            userRentTime, userFlatCost, userRealtorCost, userComment, "WHITE", 1, false);
                }

            }

            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // если есть новые соискатели, то произвести вибрацию
            if (newUsers > 0) {
                Vibrator mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                mVibrator.vibrate(500);
            }

            // сохраняем каретку и обновляем список соискателей на экране
            if (savedLastActionTime != 0) mSettings.saveInt(mSettings.SETTING_USERS_ACTION_TIME, savedLastActionTime);

            mAdapterUsers.notifyDataSetChanged();
            showEmptyOrUsersList();
        }

    }

    // показать новость для риелторов
    private void showNews(String news) {

        String savedNews = mSettings.loadString(mSettings.SETTING_REALTOR_NEWS);

        // если новостей никаких нет или нет новых
        if (news.trim().equals("-") || news.trim().equals(savedNews.trim())) return;

        mSettings.saveString(mSettings.SETTING_REALTOR_NEWS, news);

        msgDialog.show(null, getString(R.string.Title_News),
                news,
                getString(R.string.Button_OK), null, true);
    }

    // запуск таймера и попытка получить новый список соискателей при запуске окна
    private void startTimerAndRequest() {

        if (timerUsers == null) {
            timerUsers = new Timer();
            timerUsers.schedule(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            requestNewUsersByTimer();
                        }
                    });
                }
            }, 9000, 10000);
        }

        // проверяем наличие интернета
        if (!singletonArenda.checkInternet(getApplicationContext()) && !flagCheckInternet) {

            flagCheckInternet = true;

            view_loader.setVisibility(View.VISIBLE);
            view_textUsersOnline.setVisibility(View.INVISIBLE);
            view_textYourRegion.setVisibility(View.INVISIBLE);

            msgDialog.show(msgDialog.TASK_NO_INTERNET_CONNECTION, getString(R.string.Title_Information),
                    getString(R.string.Message_NoInternetConecttion),
                    getString(R.string.Button_OK), null, true);
            return;
        }

        // если местоположение определить не удается
        if (mSettings.loadCoordinate(mSettings.SETTING_COORDS_LATITUDE) == 0 && !flagNoLocation) {

            flagNoLocation = true;

            view_loader.setVisibility(View.VISIBLE);
            view_textUsersOnline.setVisibility(View.INVISIBLE);
            view_textYourRegion.setVisibility(View.INVISIBLE);

            msgDialog.show(msgDialog.TASK_LOCATION_HELP, getString(R.string.Title_NoCoordinates),
                    getString(R.string.Message_NoCoordinates),
                    getString(R.string.Button_Help), getString(R.string.Button_OK), true);
            return;
        }

        userInfoUpdated = true; // сообщаем, что информацию о риелторе мы сейчас обновим
        mNetwork.updateUserInfo();

        geoService.getCityAndStreet(
                mSettings.loadCoordinate(mSettings.SETTING_COORDS_LATITUDE),
                mSettings.loadCoordinate(mSettings.SETTING_COORDS_LONGITUDE), true);

        requestNewUsers();
    }

    private void requestNewUsersByTimer() {

        // проверяем наличие интернета
        if (!singletonArenda.checkInternet(getApplicationContext()) ||
            mSettings.loadCoordinate(mSettings.SETTING_COORDS_LATITUDE) == 0) {

            view_loader.setVisibility(View.VISIBLE);
            view_textUsersOnline.setVisibility(View.INVISIBLE);
            view_textYourRegion.setVisibility(View.INVISIBLE);

            return;
        }

        view_loader.setVisibility(View.GONE);
        view_textUsersOnline.setVisibility(View.VISIBLE);
        view_textYourRegion.setVisibility(View.VISIBLE);

        // если при показе окна информацию о риелторе обновить не удалось
        if (!userInfoUpdated) {
            userInfoUpdated = true;
            mNetwork.updateUserInfo();
        }

        geoService.getCityAndStreet(
                mSettings.loadCoordinate(mSettings.SETTING_COORDS_LATITUDE),
                mSettings.loadCoordinate(mSettings.SETTING_COORDS_LONGITUDE), true);

        requestNewUsers();
    }

    // сделать запрос новых соискателей
    private void requestNewUsers() {

        String params = "action=loadusers" +
                "&userid=" + mSettings.loadString(mSettings.SETTING_MY_USERID) +
                "&phone=" + mSettings.loadString(mSettings.SETTING_MY_NUMBER) +
                "&id=" + mSettings.loadString(mSettings.SETTING_MY_ID) +
                "&actiontime=" + mSettings.loadInt(mSettings.SETTING_USERS_ACTION_TIME);

        mNetwork.postRequest(mNetwork.TASK_GET_USERS, mNetwork.PHP_FILE_REALTOR, params, mNetwork.TIMEOUT_DISABLED);
    }

    // оставнавливаем таймер запроса списка соискателей
    private void stopTimer() {

        if (timerUsers == null) return;
        timerUsers.cancel();
        timerUsers = null;
    }

    // пользователь хочет открыть окно настроек
    public void tap_settings(View v) {
        startActivity(new Intent(activityRealtor.this, activitySettings.class));
        overridePendingTransition(R.anim.splash, R.anim.alpha);
    }

    // пользователь хочет получить договор для начинающего риелтора
    public void tap_contract(View v) {
        msgDialog.show(msgDialog.TASK_SHOW_CONTRACT, getString(R.string.Realtor_Text_ContractTitle),
                getString(R.string.Realtor_Text_ContractDescription),
                getString(R.string.Button_See), getString(R.string.Button_OK), true);
    }

    // риелтор хочет посмотреть комментарий к поиску жилья
    public void tap_info(View v) {
        msgDialog.show(null, getString(R.string.Title_Information),
                v.getTag().toString().substring(4),
                getString(R.string.Button_OK), null, true);
    }

    // риелтор хочет позвонить соискателю
    public void tap_call(View v) {

        // если подсказка про выделение цветом еще не показывалась
        if (!mSettings.loadBoolean(mSettings.SETTING_REALTOR_USERS_COLORS_HINT)) {
            mSettings.saveBoolean(mSettings.SETTING_REALTOR_USERS_COLORS_HINT, true);

            msgDialog.show(null, getString(R.string.Title_Hint),
                    getString(R.string.Realtor_Message_UsersColorsHint),
                    getString(R.string.Button_OK), null, true);
            return;
        }

        // проверка вдруг мы заблокированы
        if (isBanned) {
            showMessageYouAreBanned();
            return;
        }

        // если звонки запрещены (платные подписки)
        if (callsIsDisabled) {
            openShop(true);
            return;
        }

        String tel = v.getTag().toString().substring(3);

        // +++ TODO: ДЛЯ СБОРА СТАТИСТИКИ
        String params = "action=log" +
                "&realtorphone=" + mSettings.loadString(mSettings.SETTING_MY_USERID) +
                "&userphone=" + tel;

        mNetwork.postRequest("", mNetwork.PHP_FILE_CALLS, params, mNetwork.TIMEOUT_DISABLED);
        // +++ ДЛЯ СБОРА СТАТИСТИКИ

        // если приложение запущено на Android 6.0 и у нас нет разрешения на звонки, то запрашиваем
        if (Build.VERSION.SDK_INT > 22 && checkSelfPermission(android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, singletonArenda.MY_PERMISSIONS_REQUEST_CALLS);
            return;
        }

        // совершаем звонок
        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:+7" + tel)));
    }

    // обновить список соискателей на экране
    private void loadUsers() {

        Cursor cursor = sqLite.rawQuery("select * from USERS order by ACTIONTIME desc", null);

        if (cursor != null) {

            if (cursor.moveToFirst()) {
                do {
                    // циклом перебираем всех соискателей и добавляем на экран
                    addUserToList(
                            cursor.getString(cursor.getColumnIndex("PHONE")),
                            cursor.getString(cursor.getColumnIndex("SEARCHING")),
                            cursor.getInt(cursor.getColumnIndex("ACTIONTIME")),
                            cursor.getInt(cursor.getColumnIndex("SEARCHTIME")),
                            cursor.getString(cursor.getColumnIndex("REGION")),
                            cursor.getInt(cursor.getColumnIndex("ROOMS")),
                            cursor.getString(cursor.getColumnIndex("RENTTIME")),
                            cursor.getInt(cursor.getColumnIndex("FLATCOST")),
                            cursor.getInt(cursor.getColumnIndex("REALTORCOST")),
                            cursor.getString(cursor.getColumnIndex("COMMENT")),
                            cursor.getString(cursor.getColumnIndex("COLOR")),
                            cursor.getInt(cursor.getColumnIndex("NEWUSER")),
                            true
                    );

                } while (cursor.moveToNext());
            }

            mAdapterUsers.notifyDataSetChanged();
            cursor.close();
        }

        showEmptyOrUsersList();
    }

    // метод добавляем соискателя в список
    private void addUserToList(String _phone, String _searching, int _actionTime, int _searchTime, String _region, int _rooms,
                         String _rentTime, int _flatCost, int _realtorCost, String _comment, String _color, int _newUser, boolean toEnd) {

        activityRealtor_User_Item mUser = new activityRealtor_User_Item(_phone, _searching, _actionTime, _searchTime, _region, _rooms, _rentTime, _flatCost, _realtorCost, _comment, _color, _newUser);
        if (toEnd) mUsersArray.add(mUser); else mUsersArray.add(0, mUser);
    }

    // проверка, есть ли соискатели в базе или нужно показать Empty сообщение
    private void showEmptyOrUsersList() {

        String users = getString(R.string.Realtor_Text_UsersInCity) + " " + mUsersArray.size();
        view_textUsersOnline.setText(users);

        if (mUsersArray.size() > 0) {
            view_listUsers.setVisibility(View.VISIBLE);
            view_relativeUsersEmpty.setVisibility(View.GONE);
            return;
        }

        view_listUsers.setVisibility(View.GONE);
        view_relativeUsersEmpty.setVisibility(View.VISIBLE);
    }

    // обрабатываем долгое удержание ячейки (необходимо для смены цвета ячейки соискателя)
    AdapterView.OnItemLongClickListener onItemLongClickListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View v, int pos, long id) {

            final String selectedUser = mUsersArray.get(pos).phone;

            final String[] menuItems = {
                    getResources().getString(R.string.Realtor_Text_Menu_ColorRed),
                    getResources().getString(R.string.Realtor_Text_Menu_ColorGreen),
                    getResources().getString(R.string.Realtor_Text_Menu_ColorBlue),
                    getResources().getString(R.string.Realtor_Text_Menu_ColorYellow),
                    getResources().getString(R.string.Realtor_Text_Menu_ColorWhite),
                    getResources().getString(R.string.Realtor_Text_Menu_RemoveFromList)
            };

            AlertDialog.Builder b = new AlertDialog.Builder(activityRealtor.this);
            b.setTitle(getResources().getString(R.string.Realtor_Text_Menu_ColorsTitle))
                    .setIcon(R.mipmap.ic_launcher)
                    .setCancelable(true);
            b.setItems(menuItems, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {

                    boolean isDeleting = false;
                    String colorName = "WHITE";

                    // если это удаление соискателя из списка
                    if (item == 5) {

                        sqLite.execSQL("DELETE FROM USERS WHERE PHONE = '" + selectedUser + "' ");
                        isDeleting = true;

                    } else {

                        // если это изменение цвета ячейки соискателя
                        if (item == 0) colorName = "RED";
                        if (item == 1) colorName = "GREEN";
                        if (item == 2) colorName = "BLUE";
                        if (item == 3) colorName = "YELLOW";

                        sqLite.execSQL("UPDATE USERS SET COLOR = '" + colorName + "' WHERE PHONE = '" + selectedUser + "' ");
                    }

                    for (int ii = 0; ii < mUsersArray.size(); ii++) {
                        if (mUsersArray.get(ii).phone.trim().equals(selectedUser)) {

                            // если это удаление соискателя из списка
                            if (isDeleting) {
                                mUsersArray.remove(ii);
                            } else {

                                // если это изменение цвета ячейки соискателя
                                mUsersArray.get(ii).color = colorName;
                            }

                            break;
                        }
                    }

                    // обновляем данные на экране
                    mAdapterUsers.notifyDataSetChanged();
                    showEmptyOrUsersList();
                }
            });

            AlertDialog a = b.create();
            a.show();

            return true;
        }
    };

    // показать сообщение, что номер недействителен (возможно забанен)
    private void showMessageYouAreBanned() {
        msgDialog.show(msgDialog.TASK_YOU_ARE_BANNED, "+7" + mSettings.loadString(mSettings.SETTING_MY_USERID),
                getString(R.string.Message_ErrorPhone),
                getString(R.string.Button_OK), null, true);
    }

    // открыть магазин товаров для риелтора
    private void openShop(boolean withHint) {

        // если нужно показать подсказку про окончание тестового периода
        if (withHint && !mSettings.loadBoolean(mSettings.SETTING_REALTOR_TEST_PERIOD_EXPIRED)) {

            mSettings.saveBoolean(mSettings.SETTING_REALTOR_TEST_PERIOD_EXPIRED, true);

            msgDialog.show(msgDialog.TASK_TEST_PERIOD_EXPIRED, getString(R.string.Title_Information),
                    getString(R.string.Realtor_Message_TestPeriodExpired),
                    getString(R.string.Button_OK), null, true);
            return;
        }

        // проверяем были получены товары (инициализирован ли сервис биллинга)
        if (!mBillingService.isStarted()) {
            msgDialog.show(null, getString(R.string.Title_Information),
                    getString(R.string.Realtor_Message_PleaseWaitOrRestartApp),
                    getString(R.string.Button_OK), null, true);
            return;
        }

        if (view_textExtendAccess.getText().toString().trim().equals(getString(R.string.Realtor_Text_Payment_Status_ThankYou)))
            enableShopButtons(true);

        // обновляем цены на кнопках
        refreshPrices();

        // затемняем фон и показываем настройки поиска
        view_imageBackgroundBlack.setVisibility(View.VISIBLE);
        view_relativePayment.setVisibility(View.VISIBLE);

        // параллельно считаем сколько раз мы открывали окно магазина
        int shopOpens = mSettings.loadInt(mSettings.SETTING_SHOP_OPENS);
        shopOpens ++;
        mSettings.saveInt(mSettings.SETTING_SHOP_OPENS, shopOpens);

        // и передаем информацию на сервер об этом
        String params = "action=purchase" +
                "&type=SHOP" +
                "&typedesc=" + shopOpens +
                "&userid=" + mSettings.loadString(mSettings.SETTING_MY_USERID) +
                "&id=" + mSettings.loadString(mSettings.SETTING_MY_ID);

        mNetwork.postRequest("", mNetwork.PHP_FILE_PAYMENT, params, mNetwork.TIMEOUT_DISABLED);
    }

    // обновить цены на кнопках магазина
    private void refreshPrices() {

        int dayPrice = 0;
        int days10Price = 0;
        int days30Price = 0;

        if (mBillingService.skuDetails1day != null) dayPrice = mBillingService.skuDetails1day.getPriceNum();
        if (mBillingService.skuDetails10days != null) days10Price = mBillingService.skuDetails10days.getPriceNum();
        if (mBillingService.skuDetails30days != null) days30Price = mBillingService.skuDetails30days.getPriceNum();

        view_buttonPay1day.setText(getString(R.string.Realtor_Text_Payment_Button_1Day).replace("%d", String.valueOf(dayPrice)));
        view_buttonPay10days.setText(getString(R.string.Realtor_Text_Payment_Button_10Days).replace("%d", String.valueOf(days10Price)));
        view_buttonPay30days.setText(getString(R.string.Realtor_Text_Payment_Button_30Days).replace("%d", String.valueOf(days30Price)));

        view_textPayment10DaysHint.setText(getString(R.string.Realtor_Text_Payment_PriceHint).replace("%d", String.valueOf(dayPrice * 10 - days10Price)));
        view_textPayment30DaysHint.setText(getString(R.string.Realtor_Text_Payment_PriceHint).replace("%d", String.valueOf(dayPrice * 30 - days30Price)));
    }

    private void enableShopButtons(boolean yes) {

        view_buttonPay1day.setEnabled(yes);
        view_buttonPay10days.setEnabled(yes);
        view_buttonPay30days.setEnabled(yes);

        view_buttonPay1day.setBackgroundResource(yes ? R.drawable.xml_round_green : R.drawable.xml_round_green_disabled);
        view_buttonPay10days.setBackgroundResource(yes ? R.drawable.xml_round_green : R.drawable.xml_round_green_disabled);
        view_buttonPay30days.setBackgroundResource(yes ? R.drawable.xml_round_green : R.drawable.xml_round_green_disabled);

        view_textExtendAccess.setText(getString(yes ? R.string.Realtor_Text_Payment_Status_ExtendAccess : R.string.Realtor_Text_Payment_Status_PleaseWait));
    }

    // закрыть окно магазина
    public void tap_closePayment(View v) {
        view_imageBackgroundBlack.setVisibility(View.GONE);
        view_relativePayment.setVisibility(View.GONE);
    }

    public void tap_pay_1day(View v) {
        startPurchase(mBillingService.PRODUCT_1DAY);
    }

    public void tap_pay_10days(View v) {
        startPurchase(mBillingService.PRODUCT_10DAYS);
    }

    public void tap_pay_30days(View v) {
        startPurchase(mBillingService.PRODUCT_30DAYS);
    }

    private void startPurchase(String sku) {

        // проверяем наличие интернета
        if (!singletonArenda.checkInternet(getApplicationContext())) {

            flagCheckInternet = true;

            msgDialog.show(null, getString(R.string.Title_Information),
                    getString(R.string.Message_NoInternetConecttion),
                    getString(R.string.Button_OK), null, true);
            return;
        }

        enableShopButtons(false);
        mBillingService.purchaseProduct(sku);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == mBillingService.ON_ACTIVITY_RESULT_PURCHASE)
            mBillingService.handlePurchaseIntent(requestCode, resultCode, data);
    }

    // получаем ответы от биллинг сервиса
    @Override
    public void onBillingCallback(String resultString, String resultDescription) {

        // Если при покупке произошла ошибка
        if (resultString.trim().equals(mBillingService.STATUS_FAILED)) {

            enableShopButtons(true);

            // и передаем информацию на сервер об этом
            String params = "action=purchase" +
                    "&type=" + (resultDescription.trim().equals(mBillingService.STATUS_CANCELED) ? mBillingService.STATUS_CANCELED : mBillingService.STATUS_FAILED) +
                    "&typedesc=" + (resultDescription.trim().equals(mBillingService.STATUS_CANCELED) ? "" : resultDescription) +
                    "&product=" + mBillingService.currentPurchaseProductID  +
                    "&userid=" + mSettings.loadString(mSettings.SETTING_MY_USERID) +
                    "&id=" + mSettings.loadString(mSettings.SETTING_MY_ID);

            mNetwork.postRequest("", mNetwork.PHP_FILE_PAYMENT, params, mNetwork.TIMEOUT_DISABLED);

            if (!resultDescription.trim().equals(mBillingService.STATUS_CANCELED)) {
                msgDialog.show(msgDialog.TASK_PURCHASE_NOT_VALIDATED_OR_FAILED, getString(R.string.Title_Information),
                        resultDescription,
                        getString(R.string.Button_Help), getString(R.string.Button_OK), true);
            }

            return;
        }

        // Если покупка успешно выполнена
        if (resultString.trim().equals(mBillingService.STATUS_SUCCESS)) {
            mBillingService.currentPurchaseTry = 1;
            postForPurchase();
        }
    }

    // проверка купленной только что покупки на сервере + дальнейшее сохранение информации об этом
    private void postForPurchase() {

        view_textExtendAccess.setText(getString(R.string.Realtor_Text_Payment_Status_Checking));

        // выполняем запрос на подтверждение покупки
        String params = "action=purchase" +
                "&type=BUY" +
                "&typedesc=" + mBillingService.currentPurchaseTry +
                "&product=" + mBillingService.currentPurchaseProductID  +
                "&userid=" + mSettings.loadString(mSettings.SETTING_MY_USERID) +
                "&id=" + mSettings.loadString(mSettings.SETTING_MY_ID) +
                "&token=" + mBillingService.currentPurchaseToken;

        mNetwork.postRequest(mNetwork.TASK_VALIDATING_PURCHASE, mNetwork.PHP_FILE_PAYMENT, params, mNetwork.TIMEOUT_12_SECONDS);
    }

}