package kz.app.arenda.classes;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import kz.app.arenda.R;

// КЛАСС ДЛЯ ПОКАЗА ДИАЛОГОВОГО СООБЩЕНИЯ

public class messageDialog {

    public String TASK_CHECK_NUMBER = "Check Number";
    public String TASK_RESEND_CODE = "Resend Code";
    public String TASK_NEED_LOCATION_PERMISSION = "Location";
    public String TASK_CHANGE_MODE = "Change Mode";
    public String TASK_DISABLE_PUSH_NOTIFICATIONS = "Disable Push Notifications";
    public String TASK_SHOW_CONTRACT = "Contract";
    public String TASK_SUBMIT_SEARCH = "Submit Search";
    public String TASK_UPDATE_APP = "Update App";
    public String TASK_LOCATION_HELP = "Location Help";
    public String TASK_NO_INTERNET_CONNECTION = "No Internet Connection";
    public String TASK_YOU_ARE_BANNED = "You Are Banned";
    public String TASK_TEST_PERIOD_EXPIRED = "Test Period Expired";
    public String TASK_PURCHASE_TIMEOUT = "Purchase Timeout";
    public String TASK_PURCHASE_NOT_VALIDATED_OR_FAILED = "Purchase Not Validated Or Failed";

    private Listener mListener = null;
    private Activity mActivity = null;

    public interface Listener {
        void onMessageDialogButtonClicked(String dialogTask, boolean isPositive);
    }

    public messageDialog(Listener listener, Activity activity) {
        mListener = listener;
        mActivity = activity;
    }

    public void testShow(String msg) {
        show(null, null, msg, "OK", null, true);
    }

    public void show(final String dialogTask, String title, String message, String positiveButton, String negativeButton, boolean canCancel) {

        AlertDialog alertDialog = new AlertDialog.Builder(mActivity).create();
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(canCancel);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, positiveButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (dialogTask != null) mListener.onMessageDialogButtonClicked(dialogTask, true);
            }
        });

        if (negativeButton != null) {
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, negativeButton, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (dialogTask != null) mListener.onMessageDialogButtonClicked(dialogTask, false);
                }
            });
        }

        if(!mActivity.isFinishing()) alertDialog.show();
    }
}