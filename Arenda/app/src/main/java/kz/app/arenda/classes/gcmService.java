package kz.app.arenda.classes;

import android.content.Context;
import android.os.AsyncTask;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import java.io.IOException;
import kz.app.arenda.R;

// КЛАСС ДЛЯ ПОЛУЧЕНИЯ DEVICE TOKEN

public class gcmService {

    private Listener mListener = null;
    private Context mContext = null;
    private GoogleCloudMessaging gcm;

    public interface Listener {
        void onGetDeviceToken(String tokenString);
    }

    public gcmService(Listener listener, Context context, boolean getToken) {
        mListener = listener;
        mContext = context;
        gcm = GoogleCloudMessaging.getInstance(context);

        if (getToken) getDeviceToken();
    }

    public void getDeviceToken() {
        new getDeviceToken().execute();
    }

    class getDeviceToken extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            String tokenString = "";

            try {
                if (gcm == null) gcm = GoogleCloudMessaging.getInstance(mContext);
                tokenString = gcm.register(mContext.getString(R.string.gcm_id));
            } catch (IOException ex) {}

            return tokenString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.trim().equals("")) return;
            mListener.onGetDeviceToken(result);
        }
    }

}