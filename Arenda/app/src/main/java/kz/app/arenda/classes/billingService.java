package kz.app.arenda.classes;

import android.app.Activity;
import android.content.Intent;

import java.util.ArrayList;

import kz.app.arenda.R;
import kz.app.arenda.billing.IabHelper;
import kz.app.arenda.billing.IabResult;
import kz.app.arenda.billing.Inventory;
import kz.app.arenda.billing.Purchase;
import kz.app.arenda.billing.SkuDetails;

// КЛАСС ДЛЯ РАБОТЫ С ОПЛАТОЙ

public class billingService {

    public int ON_ACTIVITY_RESULT_PURCHASE = 1000;
    public int currentPurchaseTry = 0;
    public String currentPurchaseProductID = "";
    public String currentPurchaseToken = "";
    public boolean currentPurchaseIsCanceled;

    public String PRODUCT_1DAY = "kz.app.arenda.1day";
    public String PRODUCT_10DAYS = "kz.app.arenda.10days";
    public String PRODUCT_30DAYS = "kz.app.arenda.30days";

    public String STATUS_FAILED = "FAILED";
    public String STATUS_CANCELED = "CANCELED";
    public String STATUS_SUCCESS = "SUCCESS";

    public SkuDetails skuDetails1day = null;
    public SkuDetails skuDetails10days = null;
    public SkuDetails skuDetails30days = null;

    private IabHelper mHelper = null;
    private Listener mListener = null;
    private Activity mActivity = null;

    private boolean isStarted = false;

    public interface Listener {
        void onBillingCallback(String resultString, String resultDescription);
    }

    // метод проверяет запущен ли сервис биллинга
    public boolean isStarted() {
        if (mHelper != null && isStarted) return true;
        return false;
    }

    // метод уничтожает сервис биллинга
    public void destroy() {
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

    // конструктор и инициализация здесь
    public billingService(Listener listener, Activity activity) {

        mListener = listener;
        mActivity = activity;

        mHelper = new IabHelper(activity.getApplicationContext(), activity.getApplicationContext().getString(R.string.rsa_key));
        mHelper.enableDebugLogging(false);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {

                if (result.isFailure()) return;

                ArrayList <String> skuList = new ArrayList <> ();
                skuList.add(PRODUCT_1DAY);
                skuList.add(PRODUCT_10DAYS);
                skuList.add(PRODUCT_30DAYS);

                mHelper.queryInventoryAsync(true, skuList, new IabHelper.QueryInventoryFinishedListener() {
                    public void onQueryInventoryFinished(IabResult result, Inventory inventory) {

                        // если же произошла какая-то ошибка, то выдаем ее описание
                        if (result.isFailure()) return;

                        isStarted = true;

                        skuDetails1day = inventory.getSkuDetails(PRODUCT_1DAY);
                        skuDetails10days = inventory.getSkuDetails(PRODUCT_10DAYS);
                        skuDetails30days = inventory.getSkuDetails(PRODUCT_30DAYS);
                    }
                });
            }
        });
    }

    // 1. пользователь хочет купить товар
    public void purchaseProduct(String sku) {

        currentPurchaseProductID = sku;

        if (currentPurchaseProductID.trim().equals(PRODUCT_1DAY) && skuDetails1day == null) {
            mListener.onBillingCallback(STATUS_FAILED, mActivity.getApplicationContext().getString(R.string.Realtor_Message_PaymentProductIsNotExists));
            return;
        }

        if (currentPurchaseProductID.trim().equals(PRODUCT_10DAYS) && skuDetails10days == null) {
            mListener.onBillingCallback(STATUS_FAILED, mActivity.getApplicationContext().getString(R.string.Realtor_Message_PaymentProductIsNotExists));
            return;
        }

        if (currentPurchaseProductID.trim().equals(PRODUCT_30DAYS) && skuDetails30days == null) {
            mListener.onBillingCallback(STATUS_FAILED, mActivity.getApplicationContext().getString(R.string.Realtor_Message_PaymentProductIsNotExists));
            return;
        }

        // Вдруг сервис биллинг был уничтожен
        if (!isStarted()) {
            mListener.onBillingCallback(STATUS_FAILED, mActivity.getApplicationContext().getString(R.string.Realtor_Message_PaymentBillingError));
            return;
        }

        // 2. пытаемся вначале потратить товар (вдруг он был куплен ранее)
        mHelper.queryInventoryAsync(new IabHelper.QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result, Inventory inventory) {

                // Вдруг сервис биллинг был уничтожен
                if (!isStarted()) {
                    mListener.onBillingCallback(STATUS_FAILED, mActivity.getApplicationContext().getString(R.string.Realtor_Message_PaymentBillingError));
                    return;
                }

                // если же произошла какая-то ошибка, то выдаем ее описание
                if (result.isFailure()) {
                    mListener.onBillingCallback(STATUS_FAILED, result.getMessage());
                    return;
                }

                // проверяем куплен ли выбранный товар
                Purchase selectedPurchase = inventory.getPurchase(currentPurchaseProductID);

                // если да, то тратим его
                if (selectedPurchase != null) {
                    mHelper.consumeAsync(selectedPurchase, mConsumeFinishedListener);
                    return;
                }

                // если товар еще не куплен, то покупаем
                mHelper.launchPurchaseFlow(mActivity, currentPurchaseProductID, ON_ACTIVITY_RESULT_PURCHASE, mPurchaseFinishedListener, "");
            }
        });
    }

    // 2. Тратим купленный товар здесь
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {

            // если же произошла какая-то ошибка, то выдаем ее описание
            if (result.isFailure()) {
                mListener.onBillingCallback(STATUS_FAILED, result.getMessage());
                return;
            }

            // сервер Google прислал ответ, что товар успешно активирован (потрачен)
            currentPurchaseToken = purchase.getToken();
            mListener.onBillingCallback(STATUS_SUCCESS, "");
        }
    };

    // 3. Покупаем товар
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {

            // если же произошла какая-то ошибка / или отмена покупки, то выдаем ее описание
            if (result.isFailure()) {
                mListener.onBillingCallback(STATUS_FAILED, currentPurchaseIsCanceled ? STATUS_CANCELED : result.getMessage());
                return;
            }

            // Вдруг сервис биллинг был уничтожен
            if (!isStarted()) {
                mListener.onBillingCallback(STATUS_FAILED, mActivity.getApplicationContext().getString(R.string.Realtor_Message_PaymentBillingError));
                return;
            }

            // если покупка прошла успешно, то тратим ее
            mHelper.consumeAsync(purchase, mConsumeFinishedListener);
        }
    };

    // метод вызывается из активити, которое вызывает окно покупки
    public void handlePurchaseIntent(int requestCode, int resultCode, Intent data) {

        // Вдруг сервис биллинг был уничтожен
        if (!isStarted()) {
            mListener.onBillingCallback(STATUS_FAILED, mActivity.getApplicationContext().getString(R.string.Realtor_Message_PaymentBillingError));
            return;
        }

        currentPurchaseIsCanceled = resultCode == 0;
        mHelper.handleActivityResult(requestCode, resultCode, data);
    }
}