package kz.app.arenda;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import kz.app.arenda.classes.gcmService;
import kz.app.arenda.classes.geolocationService;
import kz.app.arenda.classes.messageDialog;
import kz.app.arenda.classes.networkService;
import kz.app.arenda.classes.sharedSettings;
import kz.app.arenda.map.DraggableMapFragment;
import kz.app.arenda.map.DragLayout;



public class activityMain extends FragmentActivity implements OnMapReadyCallback, messageDialog.Listener, networkService.Listener, gcmService.Listener, geolocationService.Listener {

    // подключенные классы для работы с диалоговыми сообщениями, настройками и сетью
    messageDialog msgDialog;
    sharedSettings mSettings;
    networkService mNetwork;
    gcmService mGoogleCloudMessaging;
    geolocationService geoService;

    // надписи, картинки, кнопки и т.п. элементы, которые используются в данном окне (активити)
    TextView view_textSelectedRegion;
    TextView view_textSearchHint;
    TextView view_textFlatCost;
    TextView view_textRealtorCost;

    ImageView view_imageBackgroundBlack;
    ImageView view_imageIconRedRegion;

    RelativeLayout view_relativeLayoutSearch;
    RelativeLayout view_relativeLayoutSearchSettings;

    SeekBar view_seekbarFlatCost;
    SeekBar view_seekbarRealtorCost;

    EditText view_editSearchYourNumber;
    EditText view_editSearchComment;

    Button view_buttonStartCancelSearch;
    ProgressBar view_loaderSearcher;

    // переменные данной активити
    int searchSettingsRooms = 1;
    int searchSettingsRentTime = 0;

    double searchSettingsRegionLatitude;
    double searchSettingsRegionLongitude;

    String pushBodyParams;

    private GoogleMap mMap;
    private LatLng myPos;
    private LatLng redPointerCoordinates;
    private boolean mapFirstShow = true;

    // обработчик физической кнопки НАЗАД
    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.splash, R.anim.alpha);
    }

    // событие при показе данного окна
    @Override
    protected void onResume() {
        super.onResume();
        mSettings.saveBoolean(mSettings.IN_BACKGROUND, false);

        geoService.getMyLocation();

        // обновлении информации о пользователь на сервере
        mNetwork.updateUserInfo();
    }

    // событие при скрытии данного окна
    @Override
    protected void onPause() {
        super.onPause();
        mSettings.saveBoolean(mSettings.IN_BACKGROUND, true);
    }

    // событие при создании данного окна
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // инициализация классов для работы с диалоговыми сообщениями, настройками и сетью
        msgDialog = new messageDialog(this, activityMain.this);
        mSettings = new sharedSettings(this);
        mNetwork = new networkService(this, this);
        mGoogleCloudMessaging = new gcmService(this, this, true);
        geoService = new geolocationService(this, this);

        //todo: временно
        if (mSettings.loadString(mSettings.SETTING_MY_USERID).trim().equals(""))
            mSettings.saveString(mSettings.SETTING_MY_USERID, mSettings.loadString(mSettings.SETTING_MY_NUMBER));

        view_textSelectedRegion = (TextView)findViewById(R.id.label_selectedRegion);
        view_textSearchHint = (TextView)findViewById(R.id.label_searchHint);
        view_textFlatCost = (TextView)findViewById(R.id.label_flatCost);
        view_textRealtorCost = (TextView)findViewById(R.id.label_realtorCost);

        view_imageBackgroundBlack = (ImageView)findViewById(R.id.img_bg_black);
        view_imageIconRedRegion = (ImageView)findViewById(R.id.img_icon_region);

        view_relativeLayoutSearch = (RelativeLayout)findViewById(R.id.modal_search);
        view_relativeLayoutSearchSettings = (RelativeLayout)findViewById(R.id.modal_searchSettings);


        view_editSearchYourNumber = (EditText)findViewById(R.id.editText_mainYourNumber);
        view_editSearchComment = (EditText)findViewById(R.id.editText_yourComment);

        view_loaderSearcher = (ProgressBar)findViewById(R.id.loader_mainSearcher);
        view_buttonStartCancelSearch = (Button)findViewById(R.id.btn_search);

        // проверяем, нет ли поиска жилья в данный момент
        String selectedRegion = mSettings.loadString(mSettings.SETTING_SELECTED_REGION);
        if (selectedRegion.length() > 0) {
            showRedCancelButton(true);
            view_textSelectedRegion.setText(selectedRegion);
        }

        // Пользователь двигает ползунок "Не дороже: 14000 руб./мес."
        view_seekbarFlatCost = (SeekBar)findViewById(R.id.seekbar_flatCost);
        view_seekbarFlatCost.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                refreshMaxCost();
            }

            public void onStartTrackingTouch(SeekBar seekBar) {}

            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        // Пользователь двигает ползунок "% риелтору не более"
        view_seekbarRealtorCost = (SeekBar)findViewById(R.id.seekbar_realtorCost);
        view_seekbarRealtorCost.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                refreshMaxCost();
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        // отлавливаем движение карты
        DraggableMapFragment mapFragment = (DraggableMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.setOnDragListener(new DragLayout.OnDragListener() {
            @Override
            public void onDrag(MotionEvent motionEvent) {

                if (mMap == null) return;

                // скрываем лишние элементы с экрана когда двигаем карту
                if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {

                    // если в данный момент на экране кнопка "Отменить поиск"
                    if (view_imageIconRedRegion.getVisibility() == View.GONE) return;

                    searchSettingsRegionLatitude = 0.0000;
                    searchSettingsRegionLongitude = 0.0000;

                    view_textSelectedRegion.setVisibility(View.GONE);
                    view_relativeLayoutSearch.setVisibility(View.GONE);
                }

            }
        });

        mapFragment.getMapAsync(this);
    }

    // получаем Device Token от сервиса Google Cloud Messaging
    @Override
    public void onGetDeviceToken(String tokenString) {
        mNetwork.updateDeviceToken(tokenString);
    }

    // получаем название города и улицы по заданным координатам
    @Override
    public void onGetCityAndStreetName(String cityStreet) {

        if (cityStreet.trim().equals("underfined")) {
            view_textSelectedRegion.setText(getString(R.string.Main_Text_SelectYourRegion));
            view_relativeLayoutSearch.setVisibility(View.GONE);
            return;
        }

        view_textSelectedRegion.setText(cityStreet);

        searchSettingsRegionLatitude = redPointerCoordinates.latitude;
        searchSettingsRegionLongitude = redPointerCoordinates.longitude;

        view_relativeLayoutSearch.setAnimation(singletonArenda.setFadeInAnimation(true, 300, getApplicationContext()));
        view_relativeLayoutSearch.setVisibility(View.VISIBLE);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);

        mMap.setMyLocationEnabled(true);

        // показываем элементы обратно когда пользователь остановил карту
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

                // если в данный момент на экране кнопка "Отменить поиск"
                if (view_imageIconRedRegion.getVisibility() == View.GONE) return;

                // если пользователь не двигал карту, а просто нажал
                if (view_textSelectedRegion.getVisibility() == View.VISIBLE) return;

                // проверяем наличие интернета
                if (!checkInternet()) return;

                view_textSelectedRegion.setVisibility(View.VISIBLE);
                view_textSelectedRegion.setText("...");

                // получаем заголовок региона поиска жилья, например "Уфа, Проспект октября"
                getSelectedRegion();

            }
        });

        // получаем наши актуальные координаты
        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location loc) {

                myPos = new LatLng(loc.getLatitude(), loc.getLongitude());

                // если это первый показ карты, то двигаемся к нашему местоположению
                if (mapFirstShow) {
                    mapFirstShow = false;

                    if (view_imageIconRedRegion.getVisibility() == View.VISIBLE) {
                        view_textSelectedRegion.setVisibility(View.GONE);
                        view_relativeLayoutSearch.setVisibility(View.GONE);
                    }

                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myPos, 14.0f));
                }

            }
        });
    }

    // получаем заголовок региона поиска жилья, например "Уфа, Проспект октября"
    private void getSelectedRegion() {

        // 1. вначале получаем координаты X и Y под красным указателем (домик в центре экрана)
        int redPointerLeft = view_imageIconRedRegion.getLeft() + (view_imageIconRedRegion.getWidth() / 2);
        int redPointerTop = view_imageIconRedRegion.getTop() + view_imageIconRedRegion.getHeight();

        // 2. исходя из X и Y получаем широту и долготу под указателем
        Projection mapProjection = mMap.getProjection();
        redPointerCoordinates = mapProjection.fromScreenLocation(new Point(redPointerLeft, redPointerTop));

        // 3. пытаемся определить город и улицу под указателем
        geoService.getCityAndStreet(redPointerCoordinates.latitude, redPointerCoordinates.longitude, false);
    }

    // формируем строку параметров для сервера во время потдверждения/отмены поиска жилья
    private String getSubmittedParameters(String task, String reasonMessage, boolean onlyAuth) {

        String authPhone = mSettings.loadString(mSettings.SETTING_MY_NUMBER);
        String authID = mSettings.loadString(mSettings.SETTING_MY_ID);

        // если нам нужно передать только данные авторизации (для отмены поиска жилья)
        if (onlyAuth) return "action=arenda" +
                             "&task=" + task +
                             "&userid=" + mSettings.loadString(mSettings.SETTING_MY_USERID) +
                             "&phone=" + authPhone +
                             "&id=" + authID +
                             "&reason=" + reasonMessage;


        String selectedRegion = view_textSelectedRegion.getText().toString();

        int selectedFlatMaxCost = view_seekbarFlatCost.getProgress();
        selectedFlatMaxCost*= searchSettingsRentTime == 0 ? 1000 : 100;

        int selectedRealtorMaxCost = 0;
        if (searchSettingsRentTime == 0) {
            selectedRealtorMaxCost = view_seekbarRealtorCost.getProgress();
            selectedRealtorMaxCost*= 500;
        }

        String enteredComment = view_editSearchComment.getText().toString();
        enteredComment = enteredComment.replace("'", "").replace("\\", "").replace("+", "").replace("*", "").replace("|", "").replace(":", "").replace("\"", "").replace("/", "");

        // также формируем строку запроса на отправку Push уведомлений
        pushBodyParams = "action=arenda" +
                "&task=pushes" +
                "&userid=" + mSettings.loadString(mSettings.SETTING_MY_USERID) +
                "&phone=" + authPhone +
                "&id=" + authID +
                "&rooms=" + searchSettingsRooms +
                "&renttime=" + searchSettingsRentTime +
                "&cost=" + selectedFlatMaxCost +
                "&regionlat=" + searchSettingsRegionLatitude +
                "&regionlng=" + searchSettingsRegionLongitude;

        return "action=arenda" +
                "&task=" + task +
                "&userid=" + mSettings.loadString(mSettings.SETTING_MY_USERID) +
                "&phone=" + authPhone +
                "&id=" + authID +
                "&region=" + selectedRegion +
                "&rooms=" + searchSettingsRooms +
                "&renttime=" + searchSettingsRentTime +
                "&cost=" + selectedFlatMaxCost +
                "&realtor=" + selectedRealtorMaxCost +
                "&comment=" + enteredComment +
                "&regionlat=" + searchSettingsRegionLatitude +
                "&regionlng=" + searchSettingsRegionLongitude;
    }

    // прослушиваем нажатие кнопок в диалоговых сообщениях
    @Override
    public void onMessageDialogButtonClicked(String dialogTask, boolean isPositive) {

        if (!isPositive) return;

        // если это подтверждение поиска жилья
        if (dialogTask.trim().equals(msgDialog.TASK_SUBMIT_SEARCH)) {

            mSettings.saveBoolean(mSettings.SETTING_ALREADY_SEARCHED, true);
            showLoaderForSubmitting(true);

            String params = getSubmittedParameters("submit", "", false);
            mNetwork.postRequest(mNetwork.TASK_SUBMIT_SEARCH, mNetwork.PHP_FILE_USER, params, mNetwork.TIMEOUT_10_SECONDS);
            return;
        }

        // если пользователь просит помощи настроить геолокацию
        if (dialogTask.trim().equals(msgDialog.TASK_LOCATION_HELP)) {
            singletonArenda.browseURL(getString(R.string.Link_Help), getApplicationContext());
            return;
        }

        // если это обновление приложения
        if (dialogTask.trim().equals(msgDialog.TASK_UPDATE_APP))
            singletonArenda.browseURL(getString(R.string.Link_GooglePlay), getApplicationContext());
    }

    // прослушиваем ответы от сервера
    @Override
    public void onNetworkCallback(String requestTask, String resultString) {

        // если задачи не относятся к окну соискателя (проверка на всякий случай)
        if (!requestTask.trim().equals(mNetwork.TASK_SUBMIT_SEARCH) &&
            !requestTask.trim().equals(mNetwork.TASK_CANCEL_SEARCH)) return;

        // проверка на таймаут
        if (resultString.trim().equals("TIMEOUT")) {

            stopTask(requestTask);

            msgDialog.show(null, getString(R.string.Title_NetworkTimeout),
                    getString(R.string.Message_PleaseTryAgain),
                    getString(R.string.Button_OK), null, true);

            return;
        }

        // разбиваем полученный ответ на массив
        String[] resultArray = resultString.split("\\*");

        // если это успешный ответ о подтверждении поиска жилья
        if (resultArray[0].trim().equals(mNetwork.ECHO_SUBMIT_SUCCESS)) {

            stopTask(requestTask);
            hideSearchSettings();
            showRedCancelButton(true);

            mSettings.saveString(mSettings.SETTING_SELECTED_REGION, view_textSelectedRegion.getText().toString());
            mNetwork.postRequest(mNetwork.TASK_SEND_PUSHES, mNetwork.PHP_FILE_USER, pushBodyParams, mNetwork.TIMEOUT_DISABLED);
            return;
        }

        // если это отмена поиска жилья
        if (resultArray[0].trim().equals(mNetwork.ECHO_CANCEL_SUCCESS)) {

            stopTask(requestTask);
            showRedCancelButton(false);

            mSettings.saveString(mSettings.SETTING_SELECTED_REGION, "");
            return;
        }

        // если требуется обновить приложение
        if (resultArray[0].trim().equals(mNetwork.ECHO_UPDATE_APP)) {

            stopTask(requestTask);

            msgDialog.show(msgDialog.TASK_UPDATE_APP, getString(R.string.Title_NewVersion) + " " + resultArray[1],
                    getString(R.string.Message_PleaseUpdateApp),
                    getString(R.string.Button_Update), getString(R.string.Button_Cancel), true);
            return;
        }

        // если аккаунт недействителен (возможно бан)
        if (resultArray[0].trim().equals(mNetwork.ECHO_ERROR_ACCOUNT)) {

            stopTask(requestTask);

            msgDialog.show(null, getString(R.string.Title_Information),
                    getString(R.string.Message_ErrorAccount),
                    getString(R.string.Button_OK), null, true);
            return;
        }

        // если комментарий соискателя содержит стоп-слова
        if (resultArray[0].trim().equals(mNetwork.ECHO_ERROR_COMMENT)) {

            stopTask(requestTask);

            msgDialog.show(null, getString(R.string.Title_Information),
                    getString(R.string.Main_Message_StopWords),
                    getString(R.string.Button_OK), null, true);
        }
    }

    // показать/скрыть процесс loading для подтверждения поиска жилья
    private void showLoaderForSubmitting(boolean show) {
        view_loaderSearcher.setVisibility(show ? View.VISIBLE : View.GONE);
        view_relativeLayoutSearchSettings.setVisibility(show ? View.GONE : View.VISIBLE);
        mMap.getUiSettings().setScrollGesturesEnabled(show);
    }

    // показать/скрыть процесс loading для отмены поиска жилья
    private void showLoaderForCancelling(boolean show) {
        view_loaderSearcher.setVisibility(show ? View.VISIBLE : View.GONE);
        view_imageBackgroundBlack.setVisibility(show ? View.VISIBLE : View.GONE);
        view_relativeLayoutSearch.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    // остановить текущее действие
    private void stopTask(String taskName) {

        // если это было подтверждение поиска жилья
        if (taskName.trim().equals(mNetwork.TASK_SUBMIT_SEARCH)) {
            showLoaderForSubmitting(false);
            return;
        }

        // если это была отмена поиска жилья
        if (taskName.trim().equals(mNetwork.TASK_CANCEL_SEARCH))
            showLoaderForCancelling(false);
    }

    // показать красную кнопку "Отменить поиск" или зеленую "Начать поиск"
    private void showRedCancelButton(boolean redButton) {

        if (redButton) {
            view_imageIconRedRegion.setVisibility(View.GONE);
            view_relativeLayoutSearch.setVisibility(View.VISIBLE);
            view_textSearchHint.setText(R.string.Main_Text_CancelSearchHint);
            view_buttonStartCancelSearch.setText(R.string.Main_Button_CancelSearch);
            view_buttonStartCancelSearch.setBackgroundResource(R.drawable.xml_round_red);
            return;
        }

        view_imageIconRedRegion.setVisibility(View.VISIBLE);
        view_textSearchHint.setText(R.string.Main_Text_StartSearchHint);
        view_buttonStartCancelSearch.setText(R.string.Main_Button_StartSearch);
        view_buttonStartCancelSearch.setBackgroundResource(R.drawable.xml_round_green);
    }

    // Пользователь указал регион на карте и нажимает "Начать поиск"
    public void tap_startCancelSearch(View v) {

        if (mMap == null) return;

        // проверяем наличие интернета
        if (!checkInternet()) return;

        // [УБРАНА ПРОВЕРКА] проверяем мое местоположение, активно ли?
        // [УБРАНА ПРОВЕРКА] if (!isMyLocationEnabled()) return;

        // ++++++++++ Пользователь нашел квартиру и теперь нажимает "Отменить поиск" ++++++++++
        if (view_imageIconRedRegion.getVisibility() == View.GONE) {
            openCancelDialog();
            return;
        }

        mMap.getUiSettings().setScrollGesturesEnabled(false);

        view_relativeLayoutSearch.setVisibility(View.GONE);
        view_imageIconRedRegion.setVisibility(View.GONE);

        // затемняем фон и показываем настройки поиска
        view_imageBackgroundBlack.setVisibility(View.VISIBLE);
        view_relativeLayoutSearchSettings.setVisibility(View.VISIBLE);

        view_editSearchYourNumber.setText(mSettings.loadString(mSettings.SETTING_MY_NUMBER));
    }

    // окрыть диалог отмены поиска жилья
    private void openCancelDialog() {

        final EditText reasonEdit = new EditText(this);
        reasonEdit.setFilters(new InputFilter[]{new InputFilter.LengthFilter(255)});
        reasonEdit.setSingleLine(true);

        final AlertDialog.Builder builder = new AlertDialog.Builder(activityMain.this);

        builder.setTitle(getString(R.string.Main_Text_CancelSearchTitle))
                .setMessage(getString(R.string.Main_Text_CancelSearchReason))
                .setIcon(R.mipmap.ic_launcher)
                .setView(reasonEdit);

        builder.setNegativeButton(getString(R.string.Button_NO), null);
        builder.setPositiveButton(getString(R.string.Button_YES), null);

        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(final DialogInterface dialog) {

                Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        // 1. получаем текст сообщения обратной связи и заменяем запрещенные символы
                        String reasonMessage = reasonEdit.getText().toString();
                        reasonMessage = reasonMessage.replace("'", "").replace("\\", "").replace("+", "").replace("*", "").replace("|", "");

                        // 2. проверка на пустой текст
                        if (reasonMessage.trim().length() < 3) return;

                        // 3. проверяем наличие интернета
                        if (!checkInternet()) return;

                        showLoaderForCancelling(true);
                        String params = getSubmittedParameters("cancel", reasonMessage, true);
                        mNetwork.postRequest(mNetwork.TASK_CANCEL_SEARCH, mNetwork.PHP_FILE_USER, params, mNetwork.TIMEOUT_10_SECONDS);

                        dialog.dismiss();
                    }
                });
            }
        });

        alertDialog.show();
    }

    // окрашиваем в нужный цвет выбранное кол-во комнат
    private void selectRooms(int rooms) {
        searchSettingsRooms = rooms;

        Button view_button0room = (Button)findViewById(R.id.btn_0_room);
        Button view_button1room = (Button)findViewById(R.id.btn_1_room);
        Button view_button2rooms = (Button)findViewById(R.id.btn_2_rooms);
        Button view_button3rooms = (Button)findViewById(R.id.btn_3_rooms);
        Button view_button4rooms = (Button)findViewById(R.id.btn_4_rooms);

        view_button0room.setBackgroundResource(rooms == 0 ? R.drawable.xml_oval_green : R.drawable.xml_oval_silver);
        view_button1room.setBackgroundResource(rooms == 1 ? R.drawable.xml_oval_green : R.drawable.xml_oval_silver);
        view_button2rooms.setBackgroundResource(rooms == 2 ? R.drawable.xml_oval_green : R.drawable.xml_oval_silver);
        view_button3rooms.setBackgroundResource(rooms == 3 ? R.drawable.xml_oval_green : R.drawable.xml_oval_silver);
        view_button4rooms.setBackgroundResource(rooms == 4 ? R.drawable.xml_oval_green : R.drawable.xml_oval_silver);

        view_button0room.setTextColor(Color.parseColor(rooms == 0 ? "#ffffff" : "#000000"));
        view_button1room.setTextColor(Color.parseColor(rooms == 1 ? "#ffffff" : "#000000"));
        view_button2rooms.setTextColor(Color.parseColor(rooms == 2 ? "#ffffff" : "#000000"));
        view_button3rooms.setTextColor(Color.parseColor(rooms == 3 ? "#ffffff" : "#000000"));
        view_button4rooms.setTextColor(Color.parseColor(rooms == 4 ? "#ffffff" : "#000000"));

        refreshMaxValue();
        refreshMaxCost();
    }

    // Пользователь меняет кол-во необходимых комнат
    public void tap_button0Room(View v) {
        selectRooms(0);
    }

    // Пользователь меняет кол-во необходимых комнат
    public void tap_button1Room(View v) {
        selectRooms(1);
    }

    // Пользователь меняет кол-во необходимых комнат
    public void tap_button2Rooms(View v) {
        selectRooms(2);
    }

    // Пользователь меняет кол-во необходимых комнат
    public void tap_button3Rooms(View v) {
        selectRooms(3);
    }

    // Пользователь меняет кол-во необходимых комнат
    public void tap_button4Rooms(View v) {
        selectRooms(4);
    }

    // окрашиваем в нужный цвет выбранный период проживания
    private void selectRentTime(int rentTime) {
        searchSettingsRentTime = rentTime;

        Button view_buttonRentTimeMonth = (Button)findViewById(R.id.btn_rentTimeMonth);
        Button view_buttonRentTimeDay = (Button)findViewById(R.id.btn_rentTimeDay);

        view_buttonRentTimeMonth.setBackgroundResource(rentTime == 0 ? R.drawable.xml_oval_green : R.drawable.xml_oval_silver);
        view_buttonRentTimeDay.setBackgroundResource(rentTime == 1 ? R.drawable.xml_oval_green : R.drawable.xml_oval_silver);

        view_buttonRentTimeMonth.setTextColor(Color.parseColor(rentTime == 0 ? "#ffffff" : "#000000"));
        view_buttonRentTimeDay.setTextColor(Color.parseColor(rentTime == 1 ? "#ffffff" : "#000000"));

        view_textRealtorCost.setTextColor(Color.parseColor(rentTime == 1 ? "#999999" : "#222222"));
        view_seekbarRealtorCost.setEnabled(rentTime == 0);
        view_seekbarRealtorCost.getThumb().mutate().setAlpha(rentTime == 1 ? 100 : 255);

        refreshMaxValue();
        refreshMaxCost();
    }

    // Пользователь меняет период проживания на "На длительный срок"
    public void tap_buttonRentTimeMonth(View v) {
        selectRentTime(0);
    }

    // Пользователь меняет период проживания на "Посуточно"
    public void tap_buttonRentTimeDay(View v) {
        selectRentTime(1);
    }

    // Устанавливаем максимальную длину ползунка "Не дороже" взависимости от кол-ва комнат
    private void refreshMaxValue() {

        int sliderMaxValue = 20;
        if (searchSettingsRooms == 1) sliderMaxValue = 40;
        if (searchSettingsRooms == 2) sliderMaxValue = 60;
        if (searchSettingsRooms == 3) sliderMaxValue = 80;
        if (searchSettingsRooms == 4) sliderMaxValue = 100;

        // если человек выбрал посуточную аренду
        if (searchSettingsRentTime == 1) sliderMaxValue = 100;

        view_seekbarFlatCost.setMax(sliderMaxValue);
    }

    // Метод обновления надписи "Не дороже: 14000 руб./мес."
    private void refreshMaxCost() {

        int sliderValue = view_seekbarFlatCost.getProgress();

        // если в данный момент пользователь выбирает "На длительный срок"
        if (searchSettingsRentTime == 0) {
            view_seekbarRealtorCost.setMax(sliderValue);
            int sliderRealtorValue = view_seekbarRealtorCost.getProgress();
            sliderRealtorValue*= 500;

            String realtorMaxCostText = getString(R.string.Main_Text_SearchSettings_RealtorMaxCostText) + " " + sliderRealtorValue + " " + getString(R.string.Main_Text_SearchSettings_Rubles);
            view_textRealtorCost.setText(realtorMaxCostText);
        }

        sliderValue*= searchSettingsRentTime == 0 ? 1000 : 100;
        view_textFlatCost.setText(singletonArenda.generateFlatCost(sliderValue, searchSettingsRentTime, getApplicationContext()));
    }

    // Пользователь указал необходимые параметры и условия и нажимает "OK"
    public void tap_searchSettingsOK(View v) {

        // проверяем наличие интернета
        if (!checkInternet()) return;

        // [УБРАНА ПРОВЕРКА] проверяем мое местоположение, активно ли?
        // [УБРАНА ПРОВЕРКА] if (!isMyLocationEnabled()) return;

        // если пользователь указал цену за квартиру = 0 руб.
        if (view_seekbarFlatCost.getProgress() == 0) {
            msgDialog.show(null, getString(R.string.Title_Information),
                    getString(R.string.Main_Message_EnterFlatCost),
                    getString(R.string.Button_OK), null, true);
            return;
        }

        // если пользователь указал комиссию риелтору = 0 руб.
        if (searchSettingsRentTime == 0 && view_seekbarRealtorCost.getProgress() == 0) {
            msgDialog.show(null, getString(R.string.Title_Information),
                    getString(R.string.Main_Message_EnterRealtorCost),
                    getString(R.string.Button_OK), null, true);
            return;
        }

        // проверяем, что пользователь указал комиссию не меньше 10% от аренды
        if (searchSettingsRentTime == 0) {
            double checkFlatCost = view_seekbarFlatCost.getProgress() * 1000;
            double checkRealtorCost = view_seekbarRealtorCost.getProgress() * 500;

            if (checkRealtorCost / checkFlatCost < 0.1) {
                msgDialog.show(null, getString(R.string.Title_Information),
                        getString(R.string.Main_Message_PleaseEnter10Percents),
                        getString(R.string.Button_OK), null, true);
                return;
            }
        }

        String searchMyNumber = view_editSearchYourNumber.getText().toString();

        // проверяем длину номера телефона на 10 цифр
        if (searchMyNumber.length() != 10) {

            msgDialog.show(null, getString(R.string.Title_Information),
                    getString(R.string.Welcome_Message_10DigitsInNumber),
                    getString(R.string.Button_OK), null, true);
            return;
        }

        // [проверка убрана] проверяем, в правильном ли формате указан номер
//        if  (!searchMyNumber.substring(0,1).trim().equals("9")) {
//
//            msgDialog.show(null, getString(R.string.Title_Information),
//                    getString(R.string.Welcome_Message_BadNumberFormat),
//                    getString(R.string.Button_OK), null, true);
//            return;
//        }

        hideKeyboard();
        mSettings.saveString(mSettings.SETTING_MY_NUMBER, searchMyNumber);

        // ниже формируем строку красивого сообщения "Вы хотите найти квартиру ..."
        String messageText = mSettings.loadBoolean(mSettings.SETTING_ALREADY_SEARCHED) ? "" : getString(R.string.Main_Text_AlreadySearchedHint);

        String roomsText = singletonArenda.generateRooms(searchSettingsRooms, getApplicationContext());

        String rentTimeText = searchSettingsRentTime == 0 ? getString(R.string.Main_Text_SearchSettings_RentTime_Month) : getString(R.string.Main_Text_SearchSettings_RentTime_Day);

        String maxPercentText = searchSettingsRentTime == 0 ? " с " + view_textRealtorCost.getText().toString() : ".";

        messageText = messageText + getString(R.string.Main_Text_Message_SearchSettings_YouAreLookingFor) + " " + roomsText + " " + getString(R.string.Main_Text_Message_SearchSettings_InRegion) + " \"" + view_textSelectedRegion.getText().toString() + "\", " + rentTimeText + ". " + view_textFlatCost.getText().toString() + maxPercentText;
        messageText = messageText.replace(":", "");

        messageText = messageText + "\n\n" + getString(R.string.Main_Text_Message_SearchSettings_YourNumber) + mSettings.loadString(mSettings.SETTING_MY_NUMBER);

        // красивая строка сформирована, теперь показываем сообщение
        msgDialog.show(msgDialog.TASK_SUBMIT_SEARCH, getString(R.string.Main_Text_SubmitSearchTitle),
                messageText,
                getString(R.string.Button_YES), getString(R.string.Button_NO), true);
    }

    // Пользователь не хочет указывать параметры поиска и нажимает "Отмена"
    public void tap_searchSettingsCancel(View v) {
        hideKeyboard();
        hideSearchSettings();
        view_relativeLayoutSearch.setVisibility(View.VISIBLE);
        view_imageIconRedRegion.setVisibility(View.VISIBLE);
    }

    // метод скрывает окно настроек поиска (используется при ответе сервера submitSuccess и методом выше)
    private void hideSearchSettings() {
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        view_imageBackgroundBlack.setVisibility(View.GONE);
        view_relativeLayoutSearchSettings.setVisibility(View.GONE);
    }

    // пользователь хочет открыть окно настроек
    public void tap_settings(View v) {

        if (view_imageBackgroundBlack.getVisibility() == View.VISIBLE) return;

        startActivity(new Intent(activityMain.this, activitySettings.class));
        overridePendingTransition(R.anim.splash, R.anim.alpha);
    }

    // пользователь нажимает "Где я?"
    public void tap_location(View v) {
        goToMyLocation();
    }

    // и выполняет метод перехода к текущему местоположению
    private void goToMyLocation() {

        if (mMap == null) return;
        if (view_imageBackgroundBlack.getVisibility() == View.VISIBLE) return;

        // проверяем мое местоположение, активно ли?
        if (!isMyLocationEnabled() || myPos == null) return;

        if (view_imageIconRedRegion.getVisibility() == View.VISIBLE) {
            view_textSelectedRegion.setVisibility(View.GONE);
            view_relativeLayoutSearch.setVisibility(View.GONE);
        }

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myPos, 14.0f));
    }

    // проверяем мое местоположение, активно ли?
    private boolean isMyLocationEnabled() {

        if (mSettings.loadCoordinate(mSettings.SETTING_COORDS_LATITUDE) != 0) return true;

        msgDialog.show(msgDialog.TASK_LOCATION_HELP, getString(R.string.Title_NoCoordinates),
                getString(R.string.Message_NoCoordinates),
                getString(R.string.Button_Help), getString(R.string.Button_OK), true);
        return false;
    }

    // проверяем наличие интернета
    private boolean checkInternet() {

        if (singletonArenda.checkInternet(getApplicationContext())) return true;

        msgDialog.show(null, getString(R.string.Title_Information),
                getString(R.string.Message_NoInternetConecttion),
                getString(R.string.Button_OK), null, true);
        return false;
    }

    // скрываем клавиатуру с экрана, когда это нужно
    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view_editSearchYourNumber.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(view_editSearchComment.getWindowToken(), 0);
    }

}