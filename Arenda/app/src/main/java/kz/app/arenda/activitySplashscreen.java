package kz.app.arenda;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import kz.app.arenda.classes.sharedSettings;

// ЭТО ТОЧКА ВХОЖДЕНИЯ В ПРИЛОЖЕНИЕ

public class activitySplashscreen extends Activity {

    sharedSettings mSettings;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        mSettings = new sharedSettings(this);

        new Handler().postDelayed(new Runnable() {
            public void run() {

                // загружаем из настроек, какое окно нам показать?
                int currentMode = mSettings.loadInt(mSettings.SETTING_CURRENT_MODE);

                // если это первый запуск, то показываем окно регистрации
                if (currentMode == 0) startActivity(new Intent(activitySplashscreen.this, activityWelcome.class));

                if (currentMode == 1) startActivity(new Intent(activitySplashscreen.this, activityMain.class));
                if (currentMode == 2) startActivity(new Intent(activitySplashscreen.this, activityRealtor.class));

                overridePendingTransition(R.anim.splash, R.anim.alpha);
                finish();
            }
        }, 1000);
    }
}