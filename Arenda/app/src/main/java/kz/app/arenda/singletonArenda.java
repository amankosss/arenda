package kz.app.arenda;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import java.text.SimpleDateFormat;
import java.util.Date;


public class singletonArenda {

    public static int MY_PERMISSIONS_REQUEST_LOCATION = 24;
    public static int MY_PERMISSIONS_REQUEST_CALLS = 25;

    public static String generateFlatCost(int cost, int prefix, Context context) {
        String rentTime = prefix == 0 ? context.getString(R.string.Main_Text_SearchSettings_RublesPerMonth) : context.getString(R.string.Main_Text_SearchSettings_RublesPerDay);
        return context.getString(R.string.Main_Text_SearchSettings_MaxCostText) + " " + cost + " " + rentTime;
    }

    public static String generateRooms(int rooms, Context context) {

        String roomsText = context.getString(R.string.Main_Text_SearchSettings_Room_0);

        if (rooms == 1) roomsText = context.getString(R.string.Main_Text_SearchSettings_Room_1);
        if (rooms == 2) roomsText = context.getString(R.string.Main_Text_SearchSettings_Room_2);
        if (rooms == 3) roomsText = context.getString(R.string.Main_Text_SearchSettings_Room_3);
        if (rooms == 4) roomsText = context.getString(R.string.Main_Text_SearchSettings_Room_4);

        return roomsText;
    }

    // получить версию приложения
    public static String getAppVersion() {
        return BuildConfig.VERSION_NAME;
    }

    // получить версию андройда
    public static String getAndroidVersion() {
        return "" + Build.VERSION.RELEASE;
    }

    // функция получения марки и модели телефона
    public static String getDeviceModel() {
        String brand = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(brand)) return model;
        return brand + " " + model;
    }

    // функция проверки наличия интернета
    public static boolean checkInternet(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo net = cm.getActiveNetworkInfo();
        if (net != null && net.isConnected()) return true;
        return false;
    }

    // открыть ссылку по указанному URL
    public static void browseURL(String url, Context context) {
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    // применить эффект анимации
    public static Animation setFadeInAnimation(boolean fadeIn, int duration, Context context) {

        Animation fade = AnimationUtils.loadAnimation(context, fadeIn ? android.R.anim.fade_in : android.R.anim.fade_out);
        fade.setDuration(duration);

        return fade;
    }

    public static int dpToPx(int i) {
        return (int) (i * Resources.getSystem().getDisplayMetrics().density);
    }

    // получение даты из UNIX времени юзера
    public static String getTime(long userUnix){

        String strToday = "сегодня";
        String strYesterday = "вчера";
        String strAt = "в";
        String strDivider = " ";

        String strTimeFormat = "H:mm";
        String strDateFormat = "d MMM";

        // преобразования Unix-времени пользователя в полную дату
        long l = userUnix * 1000;
        Date userDateTime = new java.util.Date(l);

        String resultTime = new SimpleDateFormat(strTimeFormat).format(userDateTime);

        String resultDate = new SimpleDateFormat(strDateFormat).format(userDateTime);

        long unixYesterday = (System.currentTimeMillis() / 1000L) - (60*60*24);
        String todayDate =  new SimpleDateFormat(strDateFormat).format(new Date());
        String yesterdayDate =  new SimpleDateFormat(strDateFormat).format(unixYesterday * 1000);

        resultDate = resultDate.replace(todayDate, strToday);
        resultDate = resultDate.replace(yesterdayDate, strYesterday);

        return resultDate + " " + strAt + strDivider + resultTime;
    }

}