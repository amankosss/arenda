package kz.app.arenda.classes;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

// ГЕНЕРАТОР СТРУКТУРЫ MYSQL БАЗЫ ДЛЯ ПРИЛОЖЕНИЯ (ТРОГАТЬ ОСТОРОЖНО)
// параметр newVersion во всех классах

public class databaseService extends SQLiteOpenHelper implements BaseColumns {

    public databaseService(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table if not exists USERS" +
                " (PHONE text primary key not null," +
                " SEARCHING text not null," +
                " ACTIONTIME integer," +
                " SEARCHTIME integer," +
                " REGION text not null," +
                " ROOMS integer," +
                " RENTTIME text not null," +
                " FLATCOST integer," +
                " REALTORCOST integer," +
                " COMMENT text not null," +
                " COLOR text not null," +
                " NEWUSER integer" +
                ")");

        // при создании таблиц в базе ОБЯЗАТЕЛЬНО указывать параметр IF NOT EXISTS
        // иначе при апгрейде базы данных произойдет ошибка, так как приложение попробует
        // создать таблицу поверх существующей
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // при апгрейде на новую версию базы данных можно удалить существующую таблицу в базе,
        // например, таблицу MSG, просто потому что в ней появились новые колонки и т.д.
        // db.execSQL("DROP TABLE MSG");

        // апргрейд (увеличение значения newVersion) можно делать только, если появились
        // новые таблицы в базе или нужно изменить кол-во колонок в существующих таблицах

        onCreate(db);
    }

}